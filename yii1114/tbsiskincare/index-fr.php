<?php 
date_default_timezone_set('{timezone string, see: http://php.net/manual/en/timezones.php}');
setlocale(LC_ALL, (isset($_GET['language']) ? $_GET['language'] : 'fr_FR').'.utf8');
bindtextdomain('the-body-shop-international', dirname(__FILE__).'/locales');
bind_textdomain_codeset('the-body-shop-international', 'UTF-8');
textdomain('the-body-shop-international');

// change the following paths if necessary
$yii=dirname(__FILE__).'/../framework-1.1.14/yiilite.php';
$config=dirname(__FILE__).'/protected/config/fr.php';

require_once($yii);
Yii::createWebApplication($config)->run();

