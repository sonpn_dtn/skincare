<?php
/**
 * This is the bootstrap file for test application.
 * This file should be removed when the application is deployed for production.
 */

error_reporting(E_ALL & ~E_STRICT);
ini_set('display_errors', '1');

ini_set('auto_detect_line_endings', true);
date_default_timezone_set('Europe/London');
setlocale(LC_ALL, (isset($_GET['language']) ? $_GET['language'] : 'en_GB').'.utf8');
bindtextdomain('the-body-shop-international', dirname(__FILE__).'/locales');
bind_textdomain_codeset('the-body-shop-international', 'UTF-8');
textdomain('the-body-shop-international');

// change the following paths if necessary
$yii=dirname(__FILE__).'/../framework-1.1.14/yii.php';
$config=dirname(__FILE__).'/protected/config/local.php';

// remove the following line when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);

require_once($yii);
Yii::createWebApplication($config)->run();
