	/**
 * App constants
 */
var TYPE_SINGLE_SELECT = 1,
	TYPE_MULTIPLE_SELECT_AT_LEAST = 4;

/**
 * gettext
 */
var GT = new Gettext({'domain' : 'the-body-shop-international'});

/**
 * App properties/methods
 */
var app = {
	activeTab: 0, // This is required because the jQuery UI "active" tab property is buggy

	positionProductArrow: function($product) {
		if($product.length)
			$('.product-arrow').css('left', $('.ui-tabs .ui-tabs-panel li.active').offset().left - $('.content').offset().left + ($product.width() / 2) - ($('.product-arrow').width() / 2.03));
	},
	validateUser: function() {
		setTimeout(function() {
			if($('#site-index #User_name').val() && $('#site-index #User_gender').val() && $('#site-index #User_regime').val())
				$('.button input').removeClass('disabled');
			else
				$('.button input').addClass('disabled');
		}, 1);
	},
	updateActiveResultTab: function() {
		$('.mobile-tab-container').tabs('option', 'active', app.activeTab);

		var $active = $('.mobile-tab-container .ui-tabs-active');

		$('.ui-fake-active').removeClass('ui-fake-active');
		$('.' + $($active[0]).attr('data-tab')).parent().addClass('ui-fake-active');
	},
	updateBasket: function() {
		var bag = [],
			link = POPULATE_BASKET_URL + '?';

		// Add unique product codes to the bag
		$('.product-price input[type=checkbox]:checked').each(function(index, element) {
			if(bag.indexOf(element.value) == -1)
				bag.push(element.value);
		});

		$('.add-to-bag').html(GT.ngettext('add-to-bag', 'add-to-bag-plural', bag.length).replace(/%u/, bag.length));

		if(bag.length) {
			// Build the link
			for(var i in bag)
			{
				var index = parseInt(i) + 1;
				link += 'prdcode' + index + '=' + bag[i] + '&q' + index + '=1&';
			}

			$('.add-to-bag').attr('href', link).removeClass('disabled');
		} else
			$('.add-to-bag').attr('href', 'javascript:;').addClass('disabled');
	}
};

/**
 * DOM Ready
 */
$(function() {
	// Tooltips
	$(document).tooltip({
		position: {
			my: 'center bottom-8',
			at: 'center top',
			using: function(position, feedback) {
				$(this).css(position);
				$('<div>')
				.addClass('arrow')
				.addClass(feedback.vertical)
				.addClass(feedback.horizontal)
				.appendTo(this);
			}
		}
	});

	// Custom validation
	$('#site-index #User_name').keyup(app.validateUser);
	$('#site-index .selection').click(app.validateUser);
	$('#site-index .button input').click(function() {
		if(!$('#site-index #User_name').val())
			$('#site-index #User_name').attr('placeholder', $('#site-index #User_name').attr('data-placeholder'));

		if(!$('#site-index #User_lastName').val())
			$('#site-index #User_lastName').attr('placeholder', $('#site-index #User_lastName').attr('data-placeholder'));

		if(!$('#site-index #User_name').val() || !$('#site-index #User_lastName').val())
			return false;
	});

	// Render selected answers
	$('.hiddenSelection').each(function(index, element) {
		var $this = $(element);

		if($this.val()) {
			var answerIds = $this.val().split(',');

			for(var i in answerIds)
				$('.selection[rel=' + element.id + '][data-val=' + answerIds[i] + ']').addClass('active');
		}
	});

	// Render the progress bar
	if($('.progress').length) {
		var width = ($('.breadcrumbs .col.answered, .breadcrumbs .col.current, .breadcrumbs .col.skipped').length / $('.breadcrumbs .col').length);
		width -= ($('.breadcrumbs .col').width() / $('.breadcrumbs').width()) / 2;

		$('.progress').width((100 * width) + '%');

		// This little hack needs to be run after the bar has rendered
		// It checks to see if there is a current item, if not, it adds current to the last answered item
		if(!$('.breadcrumbs .col.current').length) {
			var answered = $('.breadcrumbs .col.answered'),
				last = $(answered.get(-1));

			last.addClass('current');
		}
	}

	// Handle answer selection
	$('.selection').click(function() {
		var $this = $(this),
			$parent = $this.parents('form'),
			type = $parent.attr('data-type'),
			typeArg = $parent.attr('data-typearg'),
			answerIds = [];

		if(type == TYPE_SINGLE_SELECT) {
			$('.selection[rel=' + $this.attr('rel') + ']').removeClass('active');
			$this.addClass('active');
		} else if(type == TYPE_MULTIPLE_SELECT_AT_LEAST) {
			if($this.attr('data-unique') == '1') {
				$('.selection[rel=' + $this.attr('rel') + ']').removeClass('active');
				$this.addClass('active');
			} else {
				$('.selection[data-unique=1]').removeClass('active');
				$this.toggleClass('active');
			}
		}

		$('.selection[rel=' + $this.attr('rel') + '].active').each(function(index, element) {
			answerIds.push($(element).attr('data-val'));
		});

		$('#' + $this.attr('rel')).val(answerIds.join(','));

		// Validation
		if(answerIds.length)
			$('.button input').removeAttr('disabled');
		else
			$('.button input').attr('disabled', 'disabled');

		// Answer hint
		$('.hint').removeClass('active');
		if($this.attr('data-description') && $this.hasClass('active')) {

			// TODO Is this the best way to handle mobile devices?
			if(screen.width <= 568 && !$('#site-index').length) {
				var $anchor = $this.hasClass('odd') && $this.next('.selection').length ? $this.next('.selection') : $this;
				$anchor.after($('.hint-container'));
			}

			$('.hint-title').text($this.text());
			$('.hint-description').text($this.attr('data-description'));

			$('.hint').addClass('active');
		}
	});

	if($('.selection.active').length)
		$('.button input').removeAttr('disabled');

	// Handle add to basket
	$('#site-result input[type=checkbox]').click(function() {
		var that = this;

		// Check related boxes
		$('input[value=' + $(this).val() + ']').each(function(index, element) {
			element.checked = that.checked;
		});

		app.updateBasket();
	});

	app.updateBasket();

	// Render results tabs
	var tabSelector = '';

	// TODO Is this the best way to handle mobile devices?
	if(screen.width <= 568) {
		tabSelector = '.mobile-tab-container';

		// Hide Drops of Youth on mobile
		$('#product-140').remove();

		// Mobile copy
		$('[data-mobile-copy]').each(function(index, element) {
			$(element).text($(element).attr('data-mobile-copy'));
		});
	} else
		tabSelector = '.tab-container';

	$(tabSelector).tabs({
		activate: function(event, ui) {
			var products = ui.newPanel.find('li'),
				$product = $(products[0]);

			$('.ui-tabs .ui-tabs-panel li').removeClass('active');
			$product.addClass('active');

			$('.product-detail-container').removeClass('active');
			$('.product-detail-container[data-id=' + $product.attr('data-id') + ']').addClass('active');

			app.positionProductArrow($('.ui-tabs .ui-tabs-panel li.active'));
		}
	});

	// Mobile results tab previous/next navigation
	$('.mobile-tab-container .previous, .mobile-tab-container .next').click(function() {
		var $this = $(this);

		if($this.hasClass('previous') && app.activeTab > 0) {
			app.activeTab--;
			app.updateActiveResultTab();
		}
		else if($this.hasClass('next') && app.activeTab < $('.product-detail-container').length) {
			app.activeTab++;
			app.updateActiveResultTab();
		}
	});

	$('.mobile-tab-container').swipe({
		swipeLeft:function(event, direction, distance, duration, fingerCount) {
			if(app.activeTab < $('.product-detail-container').length) {
				app.activeTab++;
				app.updateActiveResultTab();
			}
		},
		swipeRight:function(event, direction, distance, duration, fingerCount) {
			if(app.activeTab > 0) {
				app.activeTab--;
				app.updateActiveResultTab();
			}
		}
	});

	// Mobile results tab navigation
	$('.ui-tabs .ui-tabs-nav li').click(function() {
		$('.ui-fake-active').removeClass('ui-fake-active');
		app.activeTab = $('.mobile-tab-container').tabs('option', 'active');
	});

	// Handle product detail selection
	$('.ui-tabs .ui-tabs-panel li, .drops-of-youth').click(function() {
		var $this = $(this);

		$('.ui-tabs .ui-tabs-panel li, .drops-of-youth').removeClass('active');
		$this.addClass('active');

		$('.product-detail-container').removeClass('active');
		$('.product-detail-container[data-id=' + $this.attr('data-id') + ']').addClass('active');

		if($this.hasClass('drops-of-youth'))
			$('.product-arrow').css('left', 'initial');
		else
			app.positionProductArrow($this);
	});

	// Newsletter signup
	$('#site-result #User_newsletter').click(function() {
		$('.accepted, .unaccepted').toggleClass('active');
	});

	// Share functionality
	$('.facebook').click(function() {
		FB.ui({
			method: 'share',
			href: window.location.origin 
		});
	});

	// Email form
	$('#email-form input[type=submit]').click(function() {
		var $form = $(this).parents('form');

		$.ajax({
			url: window.location.href,
			method: 'post',
			data: {
				'User[email]': $form.find('#User_email').val(),
				'User[newsletter]': $form.find('#User_newsletter')[0].checked ? '1' : '0'
			},
			success: function(data) {
				$form.find('.errorMessage').remove();
				$form.find('.error').removeClass('error');

				$data = $(data);

				if($data.find('.errorMessage').length)
					$form.find('#User_email')
						.addClass('error')
						.after($data.find('.errorMessage')[0]);
				else
					$form.find('.thanks').removeClass('hidden');
			}
		});

		return false;
	});

	// Blog Header
	$(".nav li a").click(function () {
		$(".nav ul.sub-menu").toggleClass("hidden-nav");
	});

	// SiteCatalyst
	$('.add-to-bag').click(function() {
		_satellite.track('addtocart');
	});
});

/**
 * window onload
 *
$(window).load(function() {
	app.positionProductArrow($('.ui-tabs .ui-tabs-panel li.active'));
});

/**
 * Twitter
 */
window.twttr = (function (d,s,id) {
	var t, js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return; js=d.createElement(s); js.id=id;
	js.src="//platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs);
	return window.twttr || (t = { _e: [], ready: function(f){ t._e.push(f); } });
}(document, "script", "twitter-wjs"));

/**
 * Facebook
 */
window.fbAsyncInit = function() {
	FB.init({appId: 1452283208352325, status: true, cookie: true, xfbml: true});
};

(function() {
	var e = document.createElement('script');
	e.async = true;
	e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
	document.getElementById('fb-root').appendChild(e);
}());
