<?php 

error_reporting(E_ALL & ~E_STRICT);
ini_set('display_errors', '1');

ini_set('auto_detect_line_endings', true);
date_default_timezone_set('Europe/Zurich');
setlocale(LC_ALL, (isset($_GET['language']) ? $_GET['language'] : 'sv_SE').'.utf8');
bindtextdomain('the-body-shop-international', dirname(__FILE__).'/locales');
bind_textdomain_codeset('the-body-shop-international', 'UTF-8');
textdomain('the-body-shop-international');

// change the following paths if necessary
$yii=dirname(__FILE__).'/../framework-1.1.14/yiilite.php';
$config=dirname(__FILE__).'/protected/config/se.php';

require_once($yii);
Yii::createWebApplication($config)->run();

