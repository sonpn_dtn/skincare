<!doctype html>
<html lang="en-GB">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <?php if($this->bodyId != 'site-index') echo '<meta name="robots" content="nofollow" />'; ?>
    <meta name="description" content="<?php echo _('meta-description'); ?>" />
    <meta name="keywords" content="<?php echo _('meta-keywords'); ?>" />

    <meta property="og:title" content="<?php echo _('meta-title'); ?>" />
    <meta property="og:site_name" content="<?php echo _('meta-site-name'); ?>" />
    <meta property="og:url" content="<?php echo _('meta-url'); ?>" />
    <meta property="og:description" content="<?php echo _('meta-description'); ?>" />
    <meta property="og:image" content="<?php echo Yii::app()->getBaseUrl(true); ?>/images/logo-fb.jpg" />
    <meta property="fb:app_id" content="1452283208352325" />

    <link rel="gettext" type="application/x-po" href="<?php echo Yii::app()->baseUrl; ?>/locales/<?php echo Yii::app()->language; ?>/LC_MESSAGES/the-body-shop-international.po" />
    <?php if($this->canonical) { ?>
        <link rel="canonical" href="<?php echo Yii::app()->getBaseUrl(true); ?>" />
    <?php } ?>

    <?php if(strpos($_SERVER['SERVER_NAME'], 'skincareonline.thebodyshop.') === 0) { ?>
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/all.css?2" />

    <?php if($this->settings->gaCode) { ?>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', '<?php echo $this->settings->gaCode; ?>', '<?php echo $_SERVER['SERVER_NAME']; ?>');
            ga('send', 'pageview');
        </script>
    <?php } ?>

        <script src="//assets.adobedtm.com/42c88fb73f5fd0aa15bab49ffa24c097a2516e34/satelliteLib-c38c6b3075bbe69766e1a5e7f5a5ba48a4348635.js"></script>
    <?php } else { ?>
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/html5reset.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery-ui-1.10.4.custom.css" />

    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/responsivegridsystem/col.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/responsivegridsystem/2cols.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/responsivegridsystem/3cols.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/responsivegridsystem/4cols.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/responsivegridsystem/5cols.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/responsivegridsystem/6cols.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/responsivegridsystem/7cols.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/responsivegridsystem/8cols.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/responsivegridsystem/9cols.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/responsivegridsystem/10cols.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/responsivegridsystem/11cols.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/responsivegridsystem/12cols.css" />
    <?php } ?>

    <?php if($this->settings->fontFamily) { ?>
        <link href='http://fonts.googleapis.com/css?family=<?php echo $this->settings->fontFamily; ?>:300,400,600,700,300italic' rel='stylesheet' type='text/css'>
    <?php } ?>

    <?php if($this->settings->fontFamilyHeading) { ?>
        <link href='http://fonts.googleapis.com/css?family=<?php echo $this->settings->fontFamilyHeading; ?>:300,400,600,700,300italic' rel='stylesheet' type='text/css'>
    <?php } ?>

    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/settings/css" />

    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/blog.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/blog-mobile.css" />

    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/print.css" />

    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/810.css?1" media="only screen and (max-width: 810px) and (min-width: 561px)" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/568.css?1" media="only screen and (max-width: 568px)" />

    <link rel="SHORTCUT ICON" href="<?php echo Yii::app()->baseUrl; ?>/favicon.ico"/>

    <title><?php echo _('meta-title'); ?> | <?php echo _('meta-site-name'); ?></title>
</head>
<body id="<?php echo $this->bodyId; ?>" class="<?php echo $this->bodyClass; ?>">

<div class="content-container">
    <?php echo $content; ?>
</div>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/gettext.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/retina.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery-ui-1.10.4.custom.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.touchSwipe.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/script.js"></script>
</body>
</html>
