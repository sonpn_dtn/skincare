<div class="content">
	<div class="group result-blurb">
		<div class="col span_1_of_2 skincare-soulmates">
			<h1><?php CString::_e('your-skincare-soulmates'); ?></h1>
			<p><?php CString::_e('your-skincare-soulmates-copy'); ?></p>

			<p class="user-summary"><?php $this->renderPartial('_summary',array('user'=>$user)); ?></p>
			<p class="ready"><?php CString::_e('ready-to-see-your-new-skincare-regime'); ?></p>
		</div>

		<div class="col span_1_of_2 skincare-summary desktop">
			<?php $this->renderPartial('_emailform',array('user'=>$user,'emailSuccess'=>$emailSuccess)); ?>
		</div>
	</div>

	<div class="group product-tabs">
		<div class="col span_1_of_2 tab-container day-night-products">
			<ul>
				<li><a class="tab-day" href="#tab-day"><?php CString::_e('day'); ?></a></li>
				<?php if($resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_NIGHT] || $resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_NIGHT] || $resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_NIGHT]) { ?><li><a class="tab-night" href="#tab-night"><?php CString::_e('night'); ?></a></li><?php } ?>
			</ul>
			<ul id="tab-day" class="group">
				<?php if($resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_DAY]) $this->renderPartial('_product', array('product' => $resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_DAY])); ?>
				<?php if($resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_DAY]) $this->renderPartial('_product', array('product' => $resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_DAY])); ?>
				<?php if($resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_DAY]) $this->renderPartial('_product', array('product' => $resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_DAY])); ?>
			</ul>
			<?php if($resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_NIGHT] || $resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_NIGHT] || $resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_NIGHT]) { ?>
			<ul id="tab-night" class="group">
				<?php if($resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_NIGHT]) $this->renderPartial('_product', array('product' => $resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_NIGHT])); ?>
				<?php if($resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_NIGHT]) $this->renderPartial('_product', array('product' => $resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_NIGHT])); ?>
				<?php if($resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_NIGHT]) $this->renderPartial('_product', array('product' => $resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_NIGHT])); ?>
			</ul>
			<?php } ?>
		</div>

		<div class="col span_1_of_2 tab-container other-products">
			<a class="add-to-bag disabled clone desktop" href="#" target="_blank"><?php echo sprintf(ngettext('add-to-bag', 'add-to-bag-plural', 0), 0); ?></a>

			<ul>
				<li><a class="tab-other" href="#tab-other"><?php CString::_e('all-day-care'); ?></a></li>
			</ul>

			<ul id="tab-other" class="group">
			<?php foreach($results as $product) $this->renderPartial('_product', array('product' => $product)); ?>
			</ul>
		</div>
	</div>

	<img class="product-arrow" src="<?php echo Yii::app()->baseUrl; ?>/images/arrow-mask.png" alt="" />

	<div class="mobile-tab-container">
		<ul class="mobile-tabs">
			<?php if($resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_DAY]) { ?><li><a class="tab-day" href="#product-<?php echo $resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_DAY]->id; ?>"><?php CString::_e('day'); ?></a></li><?php } ?>
			<?php if($resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_DAY]) { ?><li data-tab="tab-day"><a class="nodisplay" href="#product-<?php echo $resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_DAY]->id; ?>"></a></li><?php } ?>
			<?php if($resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_DAY]) { ?><li data-tab="tab-day"><a class="nodisplay" href="#product-<?php echo $resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_DAY]->id; ?>"></a></li><?php } ?>
			
			<?php if($resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_NIGHT]) { ?><li <?php if($nightTabExists) echo 'data-tab="tab-night"'; ?>><a class="<?php echo $nightTabExists ? 'nodisplay' : 'tab-night'; ?>" href="#product-<?php echo $resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_NIGHT]->id; ?>"><?php CString::_e('night'); ?></a></li><?php $nightTabExists = true; } ?>
			<?php if($resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_NIGHT]) { ?><li <?php if($nightTabExists) echo 'data-tab="tab-night"'; ?>><a class="<?php echo $nightTabExists ? 'nodisplay' : 'tab-night'; ?>" href="#product-<?php echo $resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_NIGHT]->id; ?>"><?php CString::_e('night'); ?></a></li><?php $nightTabExists = true; } ?>
			<?php if($resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_NIGHT]) { ?><li <?php if($nightTabExists) echo 'data-tab="tab-night"'; ?>><a class="<?php echo $nightTabExists ? 'nodisplay' : 'tab-night'; ?>" href="#product-<?php echo $resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_NIGHT]->id; ?>"><?php CString::_e('night'); ?></a></li><?php $nightTabExists = true; } ?>
			
			<?php foreach($results as $index => $product) { if($index) { ?>
			<li data-tab="tab-other"><a class="nodisplay" href="#product-<?php echo $product->id; ?>"></a></li>
			<?php } else { ?>
			<li><a class="tab-other" href="#product-<?php echo $product->id; ?>" data-mobile-copy="<?php CString::_e('all-day-care-mobile'); ?>"><?php CString::_e('all-day-care'); ?></a></li>
			<?php } } ?>
		</ul>

		<?php $this->renderPartial('_productdetail', array('product' => $dropsOfYouth, 'active' => true)); ?>
		<?php if($resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_DAY]) $this->renderPartial('_productdetail', array('product' => $resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_DAY])); ?>
		<?php if($resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_DAY]) $this->renderPartial('_productdetail', array('product' => $resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_DAY])); ?>
		<?php if($resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_DAY]) $this->renderPartial('_productdetail', array('product' => $resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_DAY])); ?>
		<?php if($resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_NIGHT]) $this->renderPartial('_productdetail', array('product' => $resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_NIGHT])); ?>
		<?php if($resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_NIGHT]) $this->renderPartial('_productdetail', array('product' => $resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_NIGHT])); ?>
		<?php if($resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_NIGHT]) $this->renderPartial('_productdetail', array('product' => $resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_NIGHT])); ?>
		<?php foreach($results as $product) $this->renderPartial('_productdetail', array('product' => $product)); ?>

	</div>


	<div class="group">
		<div class="col span_1_of_2">
			<div class="group drops-of-youth active" data-id="<?php echo $dropsOfYouth->id; ?>">
				<div class="col span_1_of_3 fixed">
					<img src="<?php echo $dropsOfYouth->getImageUrl(Product::IMAGE_SIZE_MEDIUM_LARGE); ?>" alt="" />
				</div>

				<div class="col span_2_of_3 fixed">
					<div class="product-name"><?php echo $dropsOfYouth->nameWithoutRange;?></div>
					<p><?php echo $dropsOfYouth->details; ?></p>

					<div class="product-price">
						<div class="checkbox-container">
							<input type="checkbox" id="checkbox-<?php echo $dropsOfYouth->id; ?>" value="<?php echo trim($dropsOfYouth->activeProductVariants[0]->code); ?>" />
							<label for="checkbox-<?php echo $dropsOfYouth->id; ?>"><?php CString::_e('add'); ?></label>
						</div>

						<?php echo money_format("%.2n", $dropsOfYouth->activeProductVariants[0]->price); ?>
						-
						<span class="product-variant"><?php echo $dropsOfYouth->activeProductVariants[0]->name; ?></span>
					</div>
				</div>
			</div>
		</div>

		<div class="col span_1_of_2 bottom-right">
			<div class="promotion" <?php if(CString::_('promotion') == 'promotion') echo 'style="display: none;"'; ?>><?php CString::_e('promotion'); ?></div>
			<a class="add-to-bag disabled" href="#" target="_blank"><?php echo sprintf(ngettext('add-to-bag', 'add-to-bag-plural', 0), 0); ?></a>

			<div class="share">
				<span><?php CString::_e('share-tool'); ?></span>
				<img class="facebook" src="<?php echo Yii::app()->baseUrl; ?>/images/facebook.png" alt="" />
				<a href="https://twitter.com/intent/tweet?text=<?php echo rawurlencode(_('tweet')); ?>">
					<img src="<?php echo Yii::app()->baseUrl; ?>/images/twitter.png" alt="" />
				</a>
			</div>
		</div>
	</div>

	<div class="skincare-summary mobile-block">
		<?php $this->renderPartial('_emailform',array('user'=>$user,'emailSuccess'=>$emailSuccess)); ?>
	</div>

	<div class="group">
		<div class="col span_1_of_2">
			<a class="start-again" href="<?php echo $this->createUrl('site/index',array('User_name'=>$user->name,'User_gender'=>$user->quiz->gender)); ?>">
				<img src="<?php echo Yii::app()->baseUrl; ?>/images/arrow-green-previous.png" alt="" />
				<span><?php CString::_e('start-again'); ?></span>
			</a>
		</div>

		<div class="col span_1_of_2 print-container desktop">
			<a class="print" href="<?php echo $this->createUrl('site/result',array('userId'=>Yii::app()->request->getParam('userId'),'print'=>'true')); ?>" target="_blank">
				<span><?php CString::_e('print-to-purchase-in-store'); ?></span>
				<img src="<?php echo Yii::app()->baseUrl; ?>/images/arrow-green-next.png" alt="" />
			</a>
		</div>
	</div>
</div>
