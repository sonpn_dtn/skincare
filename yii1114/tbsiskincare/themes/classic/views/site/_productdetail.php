<div id="product-<?php echo $product->id; ?>" class="product-detail-container <?php if(!empty($active)) echo 'active'; ?>" data-id="<?php echo $product->id; ?>">
	<div class="group">
		<div class="col span_2_of_8">
			<?php /* if($product->bestSeller) { ?>
			<div class="best-seller"><?php CString::_e('best-seller'); ?></div>
			<?php } */ ?>

			<img src="<?php echo $product->getImageUrl(Product::IMAGE_SIZE_MEDIUM_LARGE); ?>" alt="" />

			<ul class="product-variants-mobile">
			<?php foreach($product->activeProductVariants as $variant) { ?>
				<li>
					<div class="product-variant"><?php echo $variant->name; ?></div>
					<div class="product-price">
						<div class="checkbox-container">
							<input type="checkbox" id="checkbox-<?php echo $variant->id; ?>" value="<?php echo trim($variant->code); ?>" />
							<label for="checkbox-<?php echo $variant->id; ?>"><?php CString::_e('add'); ?></label>
						</div>
						<?php echo money_format("%.2n", $variant->price); ?>
					</div>
				</li>
			<?php } ?>
			</ul>
		</div>

		<div class="col span_3_of_8 product-detail">
			<div class="product-name">
				<span class="product-range"><?php echo $product->productRange->name; ?></span>
				<?php echo $product->nameWithoutRange; ?>
			</div>

			<p><?php echo $product->details; ?></p>
			<?php echo $product->detailBulletsHtml; ?>
		</div>

		<div class="col span_3_of_8 product-variants">
			<ul>
			<?php foreach($product->activeProductVariants as $variant) { ?>
				<li>
					<div class="product-variant"><?php echo $variant->name; ?></div>
					<div class="product-price">
						<input type="checkbox" value="<?php echo trim($variant->code); ?>" />
						<?php echo money_format("%.2n", $variant->price); ?>
					</div>
				</li>
			<?php } ?>
			</ul>
		</div>
	</div>

	<div class="group product-nav">
		<div class="previous">
			<img src="<?php echo Yii::app()->baseUrl; ?>/images/arrow-previous.png" alt="" />
			<span><?php CString::_e('swipe-previous'); ?></span>
		</div>

		<div class="next">
			<span><?php CString::_e('swipe-next'); ?></span>
			<img src="<?php echo Yii::app()->baseUrl; ?>/images/arrow-next.png" alt="" />
		</div>
	</div>
</div>
