<?php

if(!isset($break))
	$break = '<br />';

if(!isset($tag))
	$tag = 'span';

// Summary: list user info
echo sprintf(CString::_('heres-what-you-told-us-today'), $user->name).$break;
echo sprintf(CString::_('youre-a'), $user->quiz->genderName).' ';

// Loop over questions to build summary
foreach($user->quiz->questions as $question)
{
	if($question->summaryText && $question->type == Question::TYPE_SINGLE_SELECT)
	{
		if(!empty($link))
			echo sprintf($question->summaryText,
				CHtml::link(
					Answer::model()->findByQuestionUser($question->id, $user->id)->text,
					array('question/view','id'=>$question->id,'userId'=>Yii::app()->request->getParam('userId'),'summary'=>'true'),
					array('title'=>CString::_('click-to-edit'))
				)
			);
		else
			echo sprintf($question->summaryText, '<'.$tag.'>'.Answer::model()->findByQuestionUser($question->id, $user->id)->text.'</'.$tag.'>');

		echo $break;
	}
	elseif($question->summaryText && $question->type == Question::TYPE_MULTIPLE_SELECT_AT_LEAST)
	{
		$answers = Answer::model()->findAllByQuestionUser($question->id, $user->id);
		$answerArray = array();

		foreach($answers as $answer)
			if(!empty($link))
				$answerArray[] = CHtml::link(
					$answer->summaryText ? $answer->summaryText : $answer->text,
					array('question/view','id'=>$question->id,'userId'=>Yii::app()->request->getParam('userId'),'summary'=>'true'),
					array('title'=>CString::_('click-to-edit'))
				);
			else
				$answerArray[] = '<'.$tag.'>'.($answer->summaryText ? $answer->summaryText : $answer->text).'</'.$tag.'>';

		echo sprintf($question->summaryText, CString::_list($answerArray));
		echo $break;
	}

	// Look for questions with answers that have summary text
	else
	{
		$answer = Answer::model()->findByQuestionUser($question->id, $user->id);

		if($answer && $answer->summaryText)
		{
			if(!empty($link))
				echo str_replace('<a>', '<a href="'.$this->createUrl('question/view',array('id'=>$question->id,'userId'=>Yii::app()->request->getParam('userId'),'summary'=>'true')).'" title="'.CString::_('click-to-edit').'">', $answer->summaryText);
			else
				echo str_replace('a>', $tag.'>', $answer->summaryText);

			echo $break;
		}
	}
}
