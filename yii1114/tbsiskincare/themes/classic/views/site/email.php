<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=Windows-1252">
<title>The Body Shop</title>
<style type="text/css" media="all">
table img {display:block;}
.ReadMsgBody {width: 100%;}
.ExternalClass {width: 100%;}
.ExternalClass * {line-height:100%;}
body {-webkit-text-size-adjust: none;}
@media screen and (max-width: 485px), screen and (max-device-width: 485px) {
img[class=fluid] {
  width:100% !important;
  height: auto !important;
}
table[class=nomob], span[class=nomob], td[class=nomob], img[class=nomob], tr[class=nomob] {
  display:none !important;
}
table[class=email320resize], td[class=email320resize] {
  width:320px !important;
}
table[class=email270resize], td[class=email270resize] {
  width:270px !important;
}
td[class=emailcolsplit]{
  width:270px!important; 
  float:left!important;
}
td[class=emailcolsplitleft]{
  width:270px!important; 
  float:left!important;
  margin-bottom:20px!important;
}
td[class=bottomctapadding]{
	padding:8px 10px 8px 0!important;
}
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
 
  <tr>
    <td align="center">
    <table border="0" cellspacing="0" cellpadding="0">

<tr>
<td style="LINE-HEIGHT: 0px; font-size:0px;" height="20" valign="top" width="1" align="left"></td></tr></table>
      <table border="0" cellspacing="0" cellpadding="0" width="600" class="email320resize">
                          <tr>
                            <td valign="top" width="600" align="left" class="email320resize">
        <a href="#" target="_blank"><img style="DISPLAY: block" border="0" alt="" src="<?php echo Yii::app()->getbaseUrl(true); ?>/images/email/Header.jpg" width="600" height="122" class="fluid"></a></td></tr>
                          <tr>
                            <td bgcolor="#ffffff" valign="top" width="600" align="center" class="email320resize">
                              <table border="0" cellspacing="0" cellpadding="0" width="100%" class="email270resize">

<tr>
<td valign="top" align="center">
<table border="0" cellspacing="0" cellpadding="0">

<tr>
<td style="LINE-HEIGHT: 0px; font-size:0px;" height="20" valign="top" width="1" align="left"></td></tr></table>
<table border="0" cellspacing="0" cellpadding="0">
  
  <tr>
  <td style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #3d3d3d; FONT-SIZE: 11px;" valign="top" width="15" align="left" class="nomob">&nbsp;</td>
  <td style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #3d3d3d; FONT-SIZE: 11px;" valign="middle" width="270" align="left" class="emailcolsplitleft"><table border="0" cellspacing="0" cellpadding="0" width="100%">

<tr>
<td valign="top" align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #555555; FONT-SIZE: 16px; text-transform:uppercase; line-height:18px;"><?php echo strtoupper(str_replace('span', 'strong', CString::_('your-skincare-factsheet'))); ?><br>
</td></tr></table>
<table border="0" cellspacing="0" cellpadding="0">

<tr>
<td valign="top" width="1" height="10" align="left"  style="LINE-HEIGHT: 0px; font-size:0px;"></td></tr></table>
<table border="0" cellspacing="0" cellpadding="0" width="100%">

<tr>
<td valign="top" align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #555555; FONT-SIZE: 12px; text-transform:uppercase; line-height:16px;"><?php echo strtoupper(sprintf(CString::_('hi-name'), '<strong>'.$user->name.'</strong>')); ?>
</td></tr></table>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
  
  <tr>
<td valign="top" align="left"  style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #555555; FONT-SIZE: 12px; line-height:16px;"><?php CString::_e('skin-discovery'); ?>
</td></tr></table></td>
<td class="nomob" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #17355e; FONT-SIZE: 12px" valign="top" width="30" align="center"><img style="DISPLAY: block" border="0" alt="thetrainline.com" src="<?php echo Yii::app()->getbaseUrl(true); ?>/images/email/Divider.jpg" width="2" height="128"></td>
<td style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #17355e; FONT-SIZE: 12px" valign="top" width="270" align="left" class="emailcolsplit">

<table border="0" cellspacing="0" cellpadding="0" width="270">
  
  <tr>
  <td  valign="top" width="90" align="center">
    <?php if(!empty($productRanges[0])) { ?>
      <img style="DISPLAY: block"  border="0" alt="" src="<?php echo $productRanges[0]->getUrl(73, 73); ?>" width="73" height="73">
    <?php } ?>
  </td>
  <td  valign="top" width="90" align="center">
    <?php if(!empty($productRanges[1])) { ?>
      <img style="DISPLAY: block"  border="0" alt="" src="<?php echo $productRanges[1]->getUrl(73, 73); ?>" width="73" height="73">
    <?php } ?>
  </td>
  <td  valign="top" width="90" align="center">
    <?php if(!empty($productRanges[2])) { ?>
      <img style="DISPLAY: block"  border="0" alt="" src="<?php echo $productRanges[2]->getUrl(73, 73); ?>" width="73" height="73">
    <?php } ?>
  </td>
  </tr></table>
  <table border="0" cellspacing="0" cellpadding="0">
      
      <tr>
        <td style="LINE-HEIGHT: 0px; font-size:0px;" height="2" valign="top" width="10" align="left"></td></tr></table>
        <table border="0" cellspacing="0" cellpadding="0" width="270">

<tr>
<td valign="top" width="90" align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #<?php if(!empty($productRanges[0])) echo $productRanges[0]->hexCode ? $productRanges[0]->hexCode : '555555'; ?>; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px;">
<?php if(!empty($productRanges[0])) { ?>
  <strong><?php echo htmlentities(strtoupper($productRanges[0]->name), ENT_QUOTES, 'UTF-8'); ?></strong>
<?php } ?>
</td>
<td valign="top" width="90" align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #<?php if(!empty($productRanges[0])) echo $productRanges[0]->hexCode ? $productRanges[0]->hexCode : '555555'; ?>; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px;">
<?php if(!empty($productRanges[1])) { ?>
  <strong><?php echo htmlentities(strtoupper($productRanges[1]->name), ENT_QUOTES, 'UTF-8'); ?></strong>
<?php } ?>
</td>
<td valign="top" width="90" align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #<?php if(!empty($productRanges[0])) echo $productRanges[0]->hexCode ? $productRanges[0]->hexCode : '555555'; ?>; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px;">
<?php if(!empty($productRanges[2])) { ?>
  <strong><?php echo htmlentities(strtoupper($productRanges[2]->name), ENT_QUOTES, 'UTF-8'); ?></strong>
<?php } ?>
</td>
</tr></table>
<table border="0" cellspacing="0" cellpadding="0" >
      
      <tr>
        <td style="LINE-HEIGHT: 0px; font-size:0px;" height="5" valign="top" width="10" align="left"></td></tr></table>
        <table border="0" cellspacing="0" cellpadding="0" width="270">

<tr>
<td valign="top" width="90" align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #3d3d3d; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px;"><?php if(!empty($productRanges[0])) echo htmlentities($productRanges[0]->description, ENT_QUOTES, 'UTF-8'); ?>
</td>
<td valign="top" width="90" align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #3d3d3d; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px;"><?php if(!empty($productRanges[1])) echo htmlentities($productRanges[1]->description, ENT_QUOTES, 'UTF-8'); ?>
</td>
<td valign="top" width="90" align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #3d3d3d; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px;"><?php if(!empty($productRanges[2])) echo htmlentities($productRanges[2]->description, ENT_QUOTES, 'UTF-8'); ?>
</td>
</tr></table>
<table border="0" cellspacing="0" cellpadding="0">
      
      <tr>
        <td style="LINE-HEIGHT: 0px; font-size:0px;" height="12" valign="top" width="10" align="left"></td></tr></table>


</td>
<td class="nomob" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #17355e; FONT-SIZE: 12px" valign="top" width="15" align="left">&nbsp;</td>  
  </tr></table>
<table border="0" cellspacing="0" cellpadding="0">

<tr>
<td style="LINE-HEIGHT: 0px; font-size:0px;" height="10" valign="top" width="1" align="left"></td></tr></table>
<table border="0" cellspacing="0" cellpadding="0">
  
  <tr>
  <td class="nomob" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #3d3d3d; FONT-SIZE: 11px; text-transform:uppercase;" valign="top" width="15" align="left">&nbsp;</td>
  <td style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #17355e; FONT-SIZE: 12px" valign="top" width="270" align="left" class="emailcolsplitleft">
<table border="0" cellspacing="0" cellpadding="0" width="100%">

<tr>
<td bgcolor="#27bc2b" valign="top" width="5" align="left" >
</td>
<td bgcolor="#27bc2b" valign="top" width="18" align="left" ><img style="DISPLAY: block" border="0" alt="" src="<?php echo Yii::app()->getbaseUrl(true); ?>/images/email/Day.jpg" width="18" height="22"></td>
<td bgcolor="#27bc2b" valign="middle" width="247" align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-SIZE: 15px; text-transform:uppercase; padding:0 0 0 4px;"><strong><?php strtoupper(CString::_e('day')); ?></strong></td>
</tr></table>
<table border="0" cellspacing="0" cellpadding="0">

<tr>
<td valign="top" width="1" height="12" align="left"  style="LINE-HEIGHT: 0px; font-size:0px;"></td></tr></table>

<?php

$this->renderPartial('_productemail',array('products' => array(
  $resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_DAY],
  $resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_DAY],
  $resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_DAY]
)));

?>

</td>
<td class="nomob" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #17355e; FONT-SIZE: 12px" valign="top" width="30" align="center"></td>

<td style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #3d3d3d; FONT-SIZE: 11px;" valign="top" width="270" align="left" class="emailcolsplit">
<table border="0" cellspacing="0" cellpadding="0" width="100%">

<tr>
<td bgcolor="#007532" valign="top" width="5" align="left" >
</td>
<td bgcolor="#007532" valign="top" width="18" align="left" ><img style="DISPLAY: block" border="0" alt="" src="<?php echo Yii::app()->getbaseUrl(true); ?>/images/email/Night.jpg" width="18" height="22"></td>
<td bgcolor="#007532" valign="middle" align="left" width="247" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-SIZE: 15px; text-transform:uppercase; padding:0 0 0 4px;"><strong><?php strtoupper(CString::_e('night')); ?></strong></td>
</tr></table>
<table border="0" cellspacing="0" cellpadding="0">

<tr>
<td valign="top" width="1" height="12" align="left"  style="LINE-HEIGHT: 0px; font-size:0px;"></td></tr></table>


<?php

$products = array(
  $resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_NIGHT],
  $resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_NIGHT],
  $resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_NIGHT]
);

usort($products, array($this, 'emptySort'));

$this->renderPartial('_productemail',array('products' => $products));

?>

</td>
<td class="nomob" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #17355e; FONT-SIZE: 12px" valign="top" width="15" align="left">&nbsp;</td>  
  </tr></table>
  <table border="0" cellspacing="0" cellpadding="0">

<tr>
<td style="LINE-HEIGHT: 0px; font-size:0px;" height="20" valign="top" width="1" align="left"></td></tr></table>
<table border="0" cellspacing="0" cellpadding="0">
  
  <tr>
  <td class="nomob" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #3d3d3d; FONT-SIZE: 11px; text-transform:uppercase;" valign="top" width="15" align="left">&nbsp;</td>
  <td style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #3d3d3d; FONT-SIZE: 11px; " valign="top" width="270" align="left" class="emailcolsplitleft"><table border="0" cellspacing="0" cellpadding="0" width="100%">

<tr>
<td bgcolor="#bae07d" valign="top" width="5" align="left" >
</td>
<td bgcolor="#bae07d" valign="top" width="18" align="left" ><img style="DISPLAY: block" border="0" alt="" src="<?php echo Yii::app()->getbaseUrl(true); ?>/images/email/AllDay.jpg" width="18" height="22"></td>
<td bgcolor="#bae07d" valign="middle" width="247" align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-SIZE: 15px; text-transform:uppercase; padding:0 0 0 4px;"><strong><?php strtoupper(CString::_e('all-day-care')); ?></strong></td>
</tr></table>
<table border="0" cellspacing="0" cellpadding="0">

<tr>
<td valign="top" width="1" height="12" align="left"  style="LINE-HEIGHT: 0px; font-size:0px;"></td></tr></table>

<?php $this->renderPartial('_productemail',array('products' => $results)); ?>

</td>
  
<td class="nomob" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #17355e; FONT-SIZE: 12px" valign="top" width="30" align="center"></td>

<?php $this->renderPartial('_didyouknowemail', array('didYouKnow' => $didYouKnow[0])); ?>

<td class="nomob" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #17355e; FONT-SIZE: 12px" valign="top" width="15" align="left">&nbsp;</td>  
  </tr></table>
  <table border="0" cellspacing="0" cellpadding="0">

<tr>
<td style="LINE-HEIGHT: 0px; font-size:0px;" height="20" valign="top" width="1" align="left"></td></tr></table>
<table border="0" cellspacing="0" cellpadding="0">
  
  <tr>
  <td class="nomob" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #3d3d3d; FONT-SIZE: 11px; text-transform:uppercase;" valign="top" width="15" align="left">&nbsp;</td>
<?php $this->renderPartial('_didyouknowemail', array('didYouKnow' => $didYouKnow[1])); ?>
<td class="nomob" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #17355e; FONT-SIZE: 12px" valign="top" width="30" align="center"></td>

<td style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #3d3d3d; FONT-SIZE: 11px; border:1px solid #cdcbcb;" valign="middle" width="270" align="center" class="emailcolsplit">
<table border="0" cellspacing="0" cellpadding="0">
                          <tbody>
                            <tr>
                              <td valign="top" width="1" height="25" align="center" style="LINE-HEIGHT: 0px; font-size:0px;"></td>
                            </tr>
                          </tbody>
                        </table>
<table border="0" cellspacing="0" cellpadding="0" width="100%">

<tr>
<td valign="middle" align="center">
<a href="#" target="_blank"><img style="DISPLAY: block" border="0" alt="BUY" src="<?php echo $dropsOfYouth->getImageUrl(Product::IMAGE_SIZE_MEDIUM_LARGE); ?>" width="85" height="85"></a></td>
<td valign="middle" align="left"><table border="0" cellspacing="0" cellpadding="0" width="100%">
      <tr>
        <td valign="top" align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #363636; FONT-SIZE: 16px; text-transform:uppercase; line-height:19px;"><strong><?php echo htmlentities(strtoupper($dropsOfYouth->nameWithoutRange), ENT_QUOTES, 'UTF-8'); ?></strong></td>
      </tr>
    </table>
  <table border="0" cellspacing="0" cellpadding="0">

<tr>
<td valign="top" width="1" height="10" align="left"></td></tr></table>
<table border="0" cellspacing="0" cellpadding="0" width="100%">

<tr>
<td valign="top"  align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #666666; FONT-SIZE: 11px; line-height:14px; padding:0 20px 0 0;">
<?php echo htmlentities($dropsOfYouth->details, ENT_QUOTES, 'UTF-8'); ?>
</td></tr></table>
<table border="0" cellspacing="0" cellpadding="0" >
      
      <tr>
        <td style="LINE-HEIGHT: 0px; font-size:0px;" height="10" valign="top" width="1" align="left"></td></tr></table>
        <table border="0" cellspacing="0" cellpadding="0">

<tr>
<td valign="top"  bgcolor="#00bf6c" align="center" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px; padding:2px 10px 2px 10px;"><a href="<?php echo $dropsOfYouth->url; ?>" target="_blank" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px; text-decoration:none;"><?php strtoupper(CString::_e('buy')); ?></a>

</td></tr></table></td>



</tr></table>
<table border="0" cellspacing="0" cellpadding="0">
                          <tbody>
                            <tr>
                              <td valign="top" width="1" height="25" align="center" style="LINE-HEIGHT: 0px; font-size:0px;"></td>
                            </tr>
                          </tbody>
                        </table>

</td>
<td  class="nomob"style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #17355e; FONT-SIZE: 12px" valign="top" width="15" align="left">&nbsp;</td>  
  </tr></table>
  <table border="0" cellspacing="0" cellpadding="0">

<tr>
<td style="LINE-HEIGHT: 0px; font-size:0px;" height="20" valign="top" width="1" align="left"></td></tr></table>


</td></tr></table></td></tr>

<tr>
                            <td valign="top"  align="center">
        <a href="#" target="_blank"><img style="DISPLAY: block" alt="" border="0" src="<?php echo Yii::app()->getbaseUrl(true); ?>/images/email/BottomLine.jpg" class="fluid" width="540" height="6"></a></td></tr>
        <tr>
          <td valign="top"  align="right" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #7fb719; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%">

<tr>
<td valign="top" class="bottomctapadding" align="right" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #7fb719; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px; padding:8px 20px 8px 0;">

<table border="0" cellspacing="0" cellpadding="0">

<tr>
<td valign="top" align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #7fb719; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px; padding:0 3px 0 0;"><a href="<?php echo Yii::app()->getBaseUrl(true); ?>" target="_blank" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #7fb719; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px; text-decoration:underline;">
                            <strong><?php CString::_e('return-to-body-shop-skin-care-tool'); ?></strong>
                          </a>
</td>
<td valign="middle" width="5" align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #43aad4; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px;"><img style="DISPLAY: block" border="0" alt="" src="<?php echo Yii::app()->getbaseUrl(true); ?>/images/email/CTAarrow.jpg" width="5" height="7"></td>

</tr></table></td>
</tr></table>
       
       
       </td></tr>

</table>
<table border="0" cellspacing="0" cellpadding="0">

<tr>
<td style="LINE-HEIGHT: 0px; font-size:0px;" height="20" valign="top" width="1" align="left"></td></tr></table>
</td></tr></table></body></html>