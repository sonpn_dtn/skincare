<div class="group content">
	<div class="col span_1_of_2 content-left">
		<img src="<?php echo Yii::app()->baseUrl; ?>/images/logo.png" alt="" />
		<div class="h1"><?php CString::_e('intro-title'); ?></div>
		<h1><?php CString::_e('h1'); ?></h1>
		<p><?php CString::_e('intro-copy'); ?></p>
	</div>

	<div class="col span_1_of_2 content-right">
		<h2><?php CString::_e('tell-us-a-little-about-yourself'); ?></h2>

		<?php $form=$this->beginWidget('ActiveForm',array(
			'htmlOptions'=>array(
				'data-type'=>Question::TYPE_SINGLE_SELECT,
				'data-typearg'=>null,
				'autocomplete'=>'off',
			),
		)); ?>

			<div class="group text">
				<?php echo $form->label($user,'name',array('class'=>'col span_1_of_13','label'=>CString::_('hi'))); ?>
				<?php echo $form->textField($user,'name',array('class'=>'col span_6_of_13 first-name','placeholder'=>CString::_('first-name'),'data-placeholder'=>CString::_('enter-first-name'))); ?>
				<?php echo $form->textField($user,'lastName',array('class'=>'col span_6_of_13','placeholder'=>CString::_('last-name'),'data-placeholder'=>CString::_('enter-last-name'))); ?>
			</div>

			<div>
				<?php echo $form->label($user,'gender'); ?>
				<?php echo $form->styledButtonList($user,'gender',array_reverse(User::model()->genders(), true),$user->quiz ? $user->quiz->gender : null); ?>
			</div>

			<div>
				<?php echo $form->label($user,'regime'); ?>
				<?php echo $form->styledButtonList($user,'regime',array_reverse(User::model()->regimesObject(), true),$user->quiz ? $user->quiz->regime : null); ?>
			</div>

			<div class="hint-container group">
				<div class="hint">
					<div class="hint-title"></div>
					<div class="hint-description"></div>
				</div>

				&nbsp;
			</div>

			<div class="button">
				<?php echo CHtml::submitButton(CString::_('lets-begin'),array('class'=>'disabled')); ?>
			</div>

		<?php $this->endWidget(); ?>
	</div>
</div>
