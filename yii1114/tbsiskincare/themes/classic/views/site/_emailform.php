<h2><?php CString::_e('your-skincare-summary'); ?></h2>
<p><?php CString::_e('your-skincare-summary-copy'); ?></p>

<?php $form=$this->beginWidget('ActiveForm',array(
	'htmlOptions'=>array(
		'id'=>'email-form',
		'autocomplete'=>'off',
		'novalidate'=>'novalidate',
	),
)); ?>

	<div class="form-content">
		<div class="email">
			<?php echo $form->emailField($user,'email',array('placeholder'=>_('type-in-your-email-address-here'))); ?>
			<?php echo $form->error($user,'email'); ?>
		</div>

		<div class="checkbox">
			<?php echo $form->checkbox($user,'newsletter'); ?>
			<?php echo $form->label($user,'newsletter',array('class'=>'unaccepted'.($user->newsletter ? '' : ' active'))); ?>
			<?php echo $form->label($user,'newsletter',array('label'=>_('newsletter-terms'),'class'=>'accepted'.($user->newsletter ? ' active' : ''))); ?>
		</div>
	</div>

	<div class="group button-container">
		<div class="col span_4_of_5 thanks fixed hidden">
			<span><?php CString::_e('thanks'); ?></span>
			<br />
			<?php CString::_e('you-will-shortly-receive-an-email'); ?>
		</div>

		<div class="col span_1_of_5 button fixed">
			<?php echo CHtml::submitButton(CString::_('enter')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>