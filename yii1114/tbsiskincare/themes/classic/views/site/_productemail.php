<table border="0" cellspacing="0" cellpadding="0" width="270">
      
  <tr>
<td valign="top" width="90"  align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #3d3d3d; FONT-SIZE: 8px; text-transform:uppercase; line-height:12px;"><?php if(!empty($products[0])) echo htmlentities(strtoupper($products[0]->tag->name), ENT_QUOTES, 'UTF-8'); ?></td>
<td valign="top" width="90"  align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #3d3d3d; FONT-SIZE: 8px; text-transform:uppercase; line-height:12px;"><?php if(!empty($products[1])) echo htmlentities(strtoupper($products[1]->tag->name), ENT_QUOTES, 'UTF-8'); ?></td>
<td valign="top" width="90"  align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #3d3d3d; FONT-SIZE: 8px; text-transform:uppercase; line-height:12px;"><?php if(!empty($products[2])) echo htmlentities(strtoupper($products[2]->tag->name), ENT_QUOTES, 'UTF-8'); ?></td>
</tr></table>
<table border="0" cellspacing="0" cellpadding="0" >
      
      <tr>
        <td style="LINE-HEIGHT: 0px; font-size:0px;" height="15" valign="top" width="1" align="left"></td></tr></table>
<table border="0" cellspacing="0" cellpadding="0" width="270">
  
  <tr>
  <td  valign="top" width="90"  align="center">
  	<?php if(!empty($products[0])) { ?>
		<img style="DISPLAY: block"  border="0" alt="" src="<?php echo $products[0]->getImageUrl(Product::IMAGE_SIZE_MEDIUM_LARGE); ?>" width="73" height="73">
	<?php } ?>
  </td>
  <td  valign="top" width="90"  align="center">
  	<?php if(!empty($products[1])) { ?>
		<img style="DISPLAY: block"  border="0" alt="" src="<?php echo $products[1]->getImageUrl(Product::IMAGE_SIZE_MEDIUM_LARGE); ?>" width="73" height="73">
	<?php } ?>
  </td>
  <td  valign="top" width="90"  align="center">
	<?php if(!empty($products[2])) { ?>
		<img style="DISPLAY: block"  border="0" alt="" src="<?php echo $products[2]->getImageUrl(Product::IMAGE_SIZE_MEDIUM_LARGE); ?>" width="73" height="73">
	<?php } ?>
  </td>
  </tr></table>
  <table border="0" cellspacing="0" cellpadding="0">
      
      <tr>
        <td style="LINE-HEIGHT: 0px; font-size:0px;" height="2" valign="top" width="10" align="left"></td></tr></table>
        <table border="0" cellspacing="0" cellpadding="0" width="270">

<tr>
<td valign="top" width="90" align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #<?php if(!empty($products[0])) echo $products[0]->productRange->hexCode ? $products[0]->productRange->hexCode : '3d3d3d'; ?>; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px;">
<?php if(!empty($products[0])) { ?>
	<strong><?php echo htmlentities(strtoupper($products[0]->productRange->name), ENT_QUOTES, 'UTF-8'); ?></strong>
<?php } ?>
</td>
<td valign="top" width="90" align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #<?php if(!empty($products[1])) echo $products[1]->productRange->hexCode ? $products[1]->productRange->hexCode : '3d3d3d'; ?>; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px;">
<?php if(!empty($products[1])) { ?>
	<strong><?php echo htmlentities(strtoupper($products[1]->productRange->name), ENT_QUOTES, 'UTF-8'); ?></strong>
<?php } ?>
</td>
<td valign="top" width="90" align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #<?php if(!empty($products[2])) echo $products[2]->productRange->hexCode ? $products[2]->productRange->hexCode : '3d3d3d'; ?>; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px;">
<?php if(!empty($products[2])) { ?>
	<strong><?php echo htmlentities(strtoupper($products[2]->productRange->name), ENT_QUOTES, 'UTF-8'); ?></strong>
<?php } ?>
</td>
</tr></table>
<table border="0" cellspacing="0" cellpadding="0" >
      
      <tr>
        <td style="LINE-HEIGHT: 0px; font-size:0px;" height="5" valign="top" width="10" align="left"></td></tr></table>
        <table border="0" cellspacing="0" cellpadding="0" width="270">

<tr>
<td valign="top" width="90" align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #3d3d3d; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px;"><?php if(!empty($products[0])) echo htmlentities(strtoupper($products[0]->nameWithoutRange), ENT_QUOTES, 'UTF-8'); ?>
</td>
<td valign="top" width="90" align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #3d3d3d; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px;"><?php if(!empty($products[1])) echo htmlentities(strtoupper($products[1]->nameWithoutRange), ENT_QUOTES, 'UTF-8'); ?>
</td>
<td valign="top" width="90" align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #3d3d3d; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px;"><?php if(!empty($products[2])) echo htmlentities(strtoupper($products[2]->nameWithoutRange), ENT_QUOTES, 'UTF-8'); ?>
</td>
</tr></table>
<table border="0" cellspacing="0" cellpadding="0" >
      
      <tr>
        <td style="LINE-HEIGHT: 0px; font-size:0px;" height="5" valign="top" width="10" align="left"></td></tr></table>
        <table border="0" cellspacing="0" cellpadding="0" width="270">

<tr>
<td valign="top" width="90" align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #3d3d3d; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px;">
<?php if(!empty($products[0])) { ?>
<table border="0" cellspacing="0" cellpadding="0">

<tr>
<td valign="top"  bgcolor="#00bf6c" align="center" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px; padding:2px 10px 2px 10px;">

<a href="<?php echo $products[0]->url; ?>" target="_blank" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px; text-decoration:none;"><?php strtoupper(CString::_e('buy')); ?></a>

</td></tr></table>
<?php } ?>

</td>
<td valign="top" width="90" align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #3d3d3d; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px;">
<?php if(!empty($products[1])) { ?>
<table border="0" cellspacing="0" cellpadding="0">

<tr>
<td valign="top"  bgcolor="#00bf6c" align="center" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px; padding:2px 10px 2px 10px;">

<a href="<?php echo $products[1]->url; ?>" target="_blank" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px; text-decoration:none;"><?php strtoupper(CString::_e('buy')); ?></a>

</td></tr></table>
<?php } ?>
</td>
<td valign="top" width="90" align="left" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #3d3d3d; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px;">
<?php if(!empty($products[2])) { ?>
<table border="0" cellspacing="0" cellpadding="0">

<tr>
<td valign="top"  bgcolor="#00bf6c" align="center" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px; padding:2px 10px 2px 10px;">

<a href="<?php echo $products[2]->url; ?>" target="_blank" style="FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-SIZE: 11px; text-transform:uppercase; line-height:14px; text-decoration:none;"><?php strtoupper(CString::_e('buy')); ?></a>

</td></tr></table>
<?php } ?>
</td>
</tr></table>