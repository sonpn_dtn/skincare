<li class="col span_1_of_3 <?php if(!empty($active)) echo 'active'; ?>" data-id="<?php echo $product->id; ?>">
	<div class="product-tag"><?php echo $product->tag->name; ?></div>
	<img src="<?php echo $product->getImageUrl(Product::IMAGE_SIZE_MEDIUM_LARGE); ?>" alt="" />
	<div class="product-name">
		<span class="product-range" <?php if($product->productRange->hexCode) echo 'style="color:#'.$product->productRange->hexCode.';"'; ?>><?php echo $product->productRange->name; ?></span>
		<?php echo $product->nameWithoutRange; ?>
	</div>
	<div class="product-variant"><?php echo $product->activeProductVariants[0]->name; ?></div>
	<div class="product-price">
		<div class="checkbox-container">
			<input type="checkbox" id="checkbox-<?php echo $product->id; ?>" value="<?php echo trim($product->activeProductVariants[0]->code); ?>" />
			<label for="checkbox-<?php echo $product->id; ?>"><?php CString::_e('add'); ?></label>
		</div>
		<?php echo money_format("%.2n", $product->activeProductVariants[0]->price); ?>
	</div>
</li>