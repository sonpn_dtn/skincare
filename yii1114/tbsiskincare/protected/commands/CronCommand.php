<?php
class CronCommand extends CConsoleCommand
{
	/**
	 * Taken from siteController.php (actionScrape)
	 */
	public function actionRun()
	{
		foreach(Region::model()->findAll() as $region)
			foreach(Product::model()->findAll() as $product)
				$product->updatePriceStock($region->languageCode.'_'.$region->countryCode);
	}
}