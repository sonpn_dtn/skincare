<?php

/**
 * This is the model class for table "tag".
 *
 * The followings are the available columns in table 'tag':
 * @property string $id
 * @property string $name
 * @property integer $status
 * @property string $created
 *
 * The followings are the available model relations:
 * @property Product[] $products
 */
class Tag extends ActiveRecord
{
	public $nonTranslatables = array('id', 'status', 'created');
	public $translatables = array('name');

	// Hard-coded references to database rows (UK defaults)
	public $ID_CLEANSE = 4;
	public $ID_TONE = 2;
	public $ID_MOISTURISE = 1;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tag';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),

			// The following rule is used by search().
            array('name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'products' => array(self::HAS_MANY, 'Product', 'tagId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => _('ID'),
			'name' => _('Name'),
			'status' => _('Status'),
			'created' => _('Created'),
		);
	}

	/**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('name',$this->name,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tag the static model class
	 */
	public static function model($className=__CLASS__)
	{
		$model = parent::model($className);

		$model->ID_CLEANSE = Yii::app()->params['tag']['cleanseId'];
		$model->ID_TONE = Yii::app()->params['tag']['toneId'];
		$model->ID_MOISTURISE = Yii::app()->params['tag']['moisturiseId'];

		return $model;
	}
}
