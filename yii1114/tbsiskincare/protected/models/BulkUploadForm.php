<?php

/**
 * BulkUploadForm class.
 */
class BulkUploadForm extends CFormModel
{
	public $categoryCsv;
	public $categoryCsvFirstLine = 1;
	public $categoryCsvSecondLine;

	public $categoryProductCsv;
	public $categoryProductCsvFirstLine = 1;
	public $categoryProductCsvSecondLine;

	public $productCsv;
	public $productCsvFirstLine = 1;
	public $productCsvSecondLine;

	public $productReferenceCsv;
	public $productReferenceCsvFirstLine = 1;
	public $productReferenceCsvSecondLine;

	public $confirm;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array('confirm', 'required'),
			array('confirm', 'compare', 'compareValue' => 1, 'message' => 'You must confirm "{attribute}"'),

			array('productCsv, productReferenceCsv', 'file', 'allowEmpty' => false, 'types' => 'csv'),
			// array('categoryCsv, categoryProductCsv, productCsv, productReferenceCsv', 'validateCsvStructure'),
			array('categoryCsvFirstLine, categoryCsvSecondLine, categoryProductCsvFirstLine, categoryProductCsvSecondLine, productCsvFirstLine, productCsvSecondLine, productReferenceCsvFirstLine, productReferenceCsvSecondLine', 'safe'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'categoryCsv' => _('Category (CSV)'),
			'categoryCsvFirstLine' => _('First line contains column names'),
			'categoryCsvSecondLine' => _('Second line contains column format'),

			'categoryProductCsv' => 'Category/Product (CSV)',
			'categoryProductCsvFirstLine' => _('First line contains column names'),
			'categoryProductCsvSecondLine' => _('Second line contains column format'),

			'productCsv' => _('Product (CSV)'),
			'productCsvFirstLine' => _('First line contains column names'),
			'productCsvSecondLine' => _('Second line contains column format'),

			'productReferenceCsv' => _('Product Reference (CSV)'),
			'productReferenceCsvFirstLine' => _('First line contains column names'),
			'productReferenceCsvSecondLine' => _('Second line contains column format'),

			'confirm' => _('I understand any existing data will be destroyed on upload'),
		);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName($attribute)
	{
		switch($attribute)
		{
			case 'categoryCsv':
				return 'CatalogSimple_Category';
			case 'categoryProductCsv':
				return 'CatalogSimple_CategoryProduct';
			case 'productCsv':
				return 'CatalogSimple_Product';
			case 'productReferenceCsv':
				return 'CatalogSimple_ProductReference';
		}
	}

	/**
	 * Process CSV upload - this is a generic "test" function to push CSV data in to temporary tables for analysis
	 */
	public function processCsvUpload($attribute, $skipFirstLine, $skipSecondLine)
	{
		$index = 0;

		// Clear out previous upload
		Yii::app()->db->createCommand("TRUNCATE `".$this->tableName($attribute)."`;")->execute();

		// Read upload
		$handle = fopen($this->{$attribute}->tempName, 'r');

		// Build SQL statements for each line/row
		while($data = fgetcsv($handle))
		{
			$index++;

			if(($index == 1 && $skipFirstLine) || ($index == 2 && $skipSecondLine))
				continue;

			$query = "INSERT INTO `".$this->tableName($attribute)."` VALUES (";

			foreach($data as $key => $value)
				$query .= "'".addslashes($value)."',";

			$query = substr($query, 0, strlen($query) - 1);
			$query .= ");";

			Yii::app()->db->createCommand($query)->execute();
		}

		fclose($handle);
	}

	/**
	 * This function reads CSV data and returns an associative array of the data
	 */
	public function convertCsvToAssocArray($attribute)
	{
		$handle = fopen($this->{$attribute}->tempName, 'r');

		$array = array();
		$keys = fgetcsv($handle);
		while($data = fgetcsv($handle))
			$array[] = array_combine($keys, $data);

		fclose($handle);

		return $array;
	}

	/**
	 * Process category CSV upload
	 */
	public function processCategoryCsvUpload()
	{
		// Read upload
		$data = $this->convertCsvToAssocArray('categoryCsv');

		// Build SQL statements for each line/row
		foreach($data as $key => $value)
		{
			if($key == 0 && $this->categoryCsvSecondLine)
				continue;

			$category = new Category;

			if($value['Parent_Code'])
			{
				$parentCategory = Category::model()->findByAttributes(array('code' => $value['Parent_Code']));
				$category->parentId = $parentCategory->id;
			}

			$category->code = $value['Category_Code'];
			$category->name = $value['Category_Name'];
			$category->description = $value['C_DESCRIPTION'];
			$category->status = $value['Category_Online'] == 1 ? Category::STATUS_ACTIVE : Category::STATUS_INACTIVE;

			$category->save();
		}
	}

	/**
	 * Process product CSV upload
	 */
	public function processProductCsvUpload()
	{
		// Read upload
		$data = $this->convertCsvToAssocArray('productCsv');

		// Build SQL statements for each line/row
		foreach($data as $key => $value)
		{
			if($key == 0 && $this->productCsvSecondLine)
				continue;


			$product = new Product;

			$product->code = $value['Product_Code'];
			$product->name = $value['Product_Name'];
			$product->details = $value['P_DETAILS'];
			$product->detailBullets = $value['P_DETAIL_BULLETS'];

			// Create the product range if it does not exist already
			if(!($productRange = ProductRange::model()->findByAttributes(array('name' => $value['PRODUCT_RANGE']))))
			{
				$productRange = new ProductRange;
				$productRange->name = $value['PRODUCT_RANGE'];
				$productRange->save();
			}

			$product->productRangeId = $productRange->id;

			// Create the product tag if it does not exist already
			// Optional, only parse the first product tag
			if($value['Product tag'])
			{
				$productTags = explode('/', $value['Product tag']);
				
				if(!($tag = Tag::model()->findByAttributes(array('name' => $productTags[0]))))
				{
					$tag = new Tag;
					$tag->name = ucfirst(strtolower($productTags[0]));
					$tag->save();
				}

				$product->tagId = $tag->id;
			}

			if($value['PRODUCT_GENDER'] == 'Women' || $value['PRODUCT_GENDER'] == 'Female')
				$product->gender = Product::GENDER_FEMALE;
			elseif($value['PRODUCT_GENDER'] == 'Men' || $value['PRODUCT_GENDER'] == 'Male')
				$product->gender = Product::GENDER_MALE;
			else
				$product->gender = Product::GENDER_NEUTRAL;

			if($value['Day Night'] == 'Day')
				$product->dayNight = Product::DAY_NIGHT_DAY;
			elseif($value['Day Night'] == 'Night')
				$product->dayNight = Product::DAY_NIGHT_NIGHT;
			else
				$product->dayNight = Product::DAY_NIGHT_NEUTRAL;

			$product->status = $value['Product_Online'] == 1 ? Product::STATUS_ACTIVE : Product::STATUS_INACTIVE;

			$product->save();
		}
	}

	/**
	 * Process category/product CSV upload
	 */
	public function processCategoryProductCsvUpload()
	{
		// Read upload
		$data = $this->convertCsvToAssocArray('categoryProductCsv');

		// Build SQL statements for each line/row
		foreach($data as $key => $value)
		{
			if($key == 0 && $this->categoryProductCsvSecondLine)
				continue;

			$category = Category::model()->findByAttributes(array('code' => $value['Category_Code']));
			$product = Product::model()->findByAttributes(array('code' => $value['Product_Code']));

			if($category && $product)
			{
				$categoryProduct = new CategoryProduct;

				$categoryProduct->categoryId = $category->id;
				$categoryProduct->productId = $product->id;

				$categoryProduct->save();
			}
		}
	}

	/**
	 * Process product reference CSV upload
	 */
	public function processProductReferenceCsvUpload()
	{
		// Read upload
		$data = $this->convertCsvToAssocArray('productReferenceCsv');

		// Build SQL statements for each line/row
		foreach($data as $key => $value)
		{
			if($key == 0 && $this->productReferenceCsvSecondLine)
				continue;

			if($product = Product::model()->findByAttributes(array('code' => $value['Product_Code'])))
			{
				$productVariant = new ProductVariant;

				$productVariant->productId = $product->id;
				$productVariant->code = $value['Reference_Code'];
				$productVariant->name = $value['Reference_Name'];
				$productVariant->description = $value['Reference_Desc'];
				$productVariant->ean = $value['EAN_NUMBER'];
				$productVariant->price = 0;
				$productVariant->status = $value['Reference_Online'] == 1 ? ProductVariant::STATUS_ACTIVE : ProductVariant::STATUS_INACTIVE;

				$productVariant->save();
			}
		}
	}

	/**
	 * Validate CSV structure
	 */
	public function validateCsvStructure($attribute, $params)
	{
		if(!$this->hasErrors())
		{
			// Read the first line from the upload
			$handle = fopen($this->{$attribute}->tempName, 'r');
			$data = fgetcsv($handle);
			fclose($handle);

			// Read the columns from the table
			$columns = Yii::app()->db->createCommand("SHOW COLUMNS FROM `".$this->tableName($attribute)."`;")->queryColumn();

			foreach($data as $key => $value)
				if($columns[$key] != $value)
					$this->addError($attribute,'Invalid CSV structure for {attribute}');
		}
	}
}
