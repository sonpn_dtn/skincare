<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property string $id
 * @property string $quizId
 * @property string $name
 * @property string $lastName
 * @property string $cookiemonster
 * @property string $email
 * @property integer $newsletter
 * @property string $cookiemonster
 * @property integer $status
 * @property string $created
 *
 * @property integer $regime
 * @property integer $gender
 *
 * The followings are the available model relations:
 * @property Answer[] $answers
 * @property Quiz $quiz
 */
class User extends CActiveRecord
{
	const REGIME_EXPRESS = 1;
	const REGIME_COMPLETE = 2;

	const GENDER_MALE = 2;
	const GENDER_FEMALE = 3;

	public $regime;
	public $gender;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, lastName, regime, gender', 'required', 'on'=>'insert'),
			array('email', 'required', 'on'=>'update', 'message'=>CString::_('please-enter-an-email-address')),
			array('newsletter, status', 'numerical', 'integerOnly'=>true),
			array('quizId', 'length', 'max'=>10),
			array('name, lastName, email', 'length', 'max'=>255),
			array('cookiemonster','length','max'=>15),
			array('email', 'email', 'message'=>CString::_('the-email-address-you-have-entered-is-invalid')),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'answers' => array(self::MANY_MANY, 'Answer', 'answer_user(userId, answerId)'),
			'quiz' => array(self::BELONGS_TO, 'Quiz', 'quizId'),

			'answerCount' => array(self::STAT, 'Answer', 'answer_user(userId, answerId)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => _('ID'),
			'quizId' => _('Quiz'),
			'name' => CString::_('hi-my-name-is'),
			'lastName' => 'Last Name',
			'email' => _('Email'),
			'newsletter' => CString::_('tick-to-sign-up-to-our-email-newsletter'),
			'status' => _('Status'),
			'created' => _('Created'),

			'regime' => CString::_('i-want-this-to-be'),
			'gender' => CString::_('im-a'),
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return array regime values and names
	 */
	public function regimes()
	{
		return array(
			self::REGIME_EXPRESS => CString::_('express'),
			self::REGIME_COMPLETE => CString::_('complete'),
		);
	}

	/**
	 * @return array dirty fix to return regimes as an object to allow for a name and description
	 */
	public function regimesObject()
	{
		return array(
			(object) array(
				'id' => self::REGIME_EXPRESS,
				'text' => CString::_('express'),
				'description' => CString::_('express-copy'),
			),
			(object) array(
				'id' => self::REGIME_COMPLETE,
				'text' => CString::_('complete'),
				'description' => CString::_('complete-copy'),
			),
		);
	}



	/**
	 * @return array gender values and names
	 */
	public function genders()
	{
		return array(
			self::GENDER_MALE => CString::_('man'),
			self::GENDER_FEMALE => CString::_('woman'),
		);
	}

	public function script($array){
		return Yii::app()->db->createCommand("
				SELECT
				COUNT(DISTINCT `user`.`id`)
				FROM
				`user`
				JOIN `answer_user` ON `user`.`id` = `answer_user`.`userId`
				JOIN `answer` ON `answer_user`.`answerId` = `answer`.`id`
				WHERE
				`user`.`quizId` = ".$array[0]."
				AND (`answer`.`questionId` = ".$array[1]." OR `answer`.`questionId` = ".$array[2].");"
			);
	}

	public function questions($id){

		return Yii::app()->db->createCommand("
			SELECT 
			COUNT(DISTINCT `user`.`id`)
			FROM `user`
			JOIN `answer_user` ON `user`.`id` = `answer_user`.`userId`
			JOIN `answer` ON `answer_user`.`answerId` = `answer`.`id`
			JOIN `question` ON `answer`.`questionId` = `question`.`id`
			WHERE `question`.`id` = ".$id."
		");
	}
}
