<?php

/**
 * This is the model class for table "productVariant".
 *
 * The followings are the available columns in table 'productVariant':
 * @property string $id
 * @property string $productId
 * @property string $code
 * @property string $name
 * @property string $description
 * @property string $ean
 * @property string $price
 * @property string $stock
 * @property integer $status
 * @property string $created
 *
 * @property string $statusName
 *
 * The followings are the available model relations:
 * @property Product $product
 */
class ProductVariant extends ActiveRecord
{
	public $nonTranslatables = array('id', 'productId', 'code', 'ean', 'price', 'stock', 'status', 'created');
	public $translatables = array('name', 'description');

	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 2;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'productVariant';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('productId, code, name, ean', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('productId, stock', 'length', 'max'=>10),
			array('code, name, description, ean', 'length', 'max'=>255),
			array('price', 'length', 'max'=>6),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'product' => array(self::BELONGS_TO, 'Product', 'productId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => _('ID'),
			'productId' => _('Product'),
			'code' => _('Code'),
			'name' => _('Name'),
			'description' => _('Description'),
			'ean' => _('EAN'),
			'price' => _('Price'),
			'stock' => _('Stock'),
			'status' => _('Status'),
			'created' => _('Created'),

			'statusName' => _('Status'),
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductVariant the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return array status values and names
	 */
	public function statuses()
	{
		return array(
			self::STATUS_ACTIVE => _('Active'),
			self::STATUS_INACTIVE => _('Inactive'),
		);
	}
	
	/**
	 * @return string a human-readable name for the status
	 */
	public function getStatusName()
	{
		if($this->status)
		{
			$statuses = $this->statuses();
			return $statuses[$this->status];
		}
	}
}
