<?php

/**
 * This is the model class for table "productRange".
 *
 * The followings are the available columns in table 'productRange':
 * @property string $id
 * @property string $name
 * @property string $description
 * @property string $hexCode
 * @property string $directory
 * @property string $fileName
 * @property string $extension
 * @property integer $status
 * @property string $created
 *
 * The followings are the available model relations:
 * @property Product[] $products
 */
class ProductRange extends ActiveImage
{
	public $file;
	public $nonTranslatables = array('file', 'id', 'hexCode', 'directory', 'fileName', 'extension', 'status', 'created');
	public $translatables = array('name', 'description');

	// Hard-coded references to database rows (UK defaults)
	public $ID_CAMOMILE = 13;

	/**
	 * Initializes this model.
	 * This method is invoked when an AR instance is newly created and has
	 * its {@link scenario} set.
	 * You may override this method to provide code that is needed to initialize the model (e.g. setting
	 * initial property values.)
	 */
	public function init()
	{
		$this->ID_CAMOMILE = Yii::app()->params['range']['camomileId'];
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'productRange';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('name, description, directory, fileName, extension', 'length', 'max'=>255),
			array('hexCode', 'length', 'max'=>6),

			array(
				'file',
				'file',
				'allowEmpty'=>true,
				'types'=>'gif,png,jpg,jpeg',
			),

			// The following rule is used by search().
			array('name, hexCode', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'products' => array(self::HAS_MANY, 'Product', 'productRangeId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => _('ID'),
			'name' => _('Name'),
			'description' => _('Description'),
			'hexCode' => _('Hex Code'),
			'directory' => _('Directory'),
			'fileName' => _('File Name'),
			'Extension' => _('Extension'),
			'status' => _('Status'),
			'created' => _('Created'),

			'file' => _('File'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('name',$this->name,true);
		$criteria->compare('hexCode',$this->hexCode,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductRange the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
