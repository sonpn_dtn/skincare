<?php

/**
 * This is the model class for table "settings".
 *
 * @property string $id
 * @property string $heroProductId
 * @property string $primaryColour
 * @property string $bodySize
 * @property string $appShading
 * @property string $fontFamily
 * @property string $fontFamilyHeading
 * @property string $gaCode
 * @property string $directory
 * @property string $fileName
 * @property string $extension
 */
class Settings extends ActiveImage
{
	public $nonTranslatables = array('id', 'heroProductId', 'primaryColour', 'bodySize', 'appShading', 'fontFamily', 'fontFamilyHeading', 'gaCode', 'directory', 'fileName', 'extension');
	public $translatables = array();

	public $file;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'settings';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('heroProductId, primaryColour, bodySize, appShading, fontFamily, fontFamilyHeading, gaCode, directory, fileName, extension', 'length', 'max'=>255),
			array(
				'file',
				'file',
				'allowEmpty' => true,
				'types' => 'gif,png,jpg,jpeg',
			),
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Settings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
