<?php

/**
 * This is the model class for table "category".
 *
 * The followings are the available columns in table 'category':
 * @property string $id
 * @property string $parentId
 * @property string $code
 * @property string $name
 * @property string $description
 * @property integer $status
 * @property string $created
 *
 * @property string $statusName
 *
 * The followings are the available model relations:
 * @property Category $parent
 * @property Category[] $categories
 * @property Product[] $products
 */
class Category extends CActiveRecord
{
	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 2;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('code, name', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('parentId', 'length', 'max'=>10),
			array('code, name', 'length', 'max'=>255),
			array('description', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parent' => array(self::BELONGS_TO, 'Category', 'parentId'),
			'categories' => array(self::HAS_MANY, 'Category', 'parentId'),
			'products' => array(self::MANY_MANY, 'Product', 'category_product(categoryId, productId)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => _('Category'),
			'parentId' => _('Parent'),
			'code' => _('Code'),
			'name' => _('Name'),
			'description' => _('Description'),
			'status' => _('Status'),
			'created' => _('Created'),

			'statusName' => _('Status'),
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Category the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return array status values and names
	 */
	public function statuses()
	{
		return array(
			self::STATUS_ACTIVE => _('Active'),
			self::STATUS_INACTIVE => _('Inactive'),
		);
	}
	
	/**
	 * @return string a human-readable name for the status
	 */
	public function getStatusName()
	{
		if($this->status)
		{
			$statuses = $this->statuses();
			return $statuses[$this->status];
		}
	}
}
