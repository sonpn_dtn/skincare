<?php

/**
 * This is the model class for table "product".
 *
 * The followings are the available columns in table 'product':
 * @property string $id
 * @property string $productRangeId
 * @property string $tagId
 * @property string $code
 * @property string $name
 * @property string $details
 * @property string $detailBullets
 * @property string $url
 * @property integer $gender
 * @property integer $dayNight
 * @property integer $bestSeller
 * @property integer $status
 * @property string $created
 *
 * @property string $genderName
 * @property string $dayNightName
 * @property string $statusName
 * @property string $nameWithoutRange
 *
 * The followings are the available model relations:
 * @property Category[] $categories
 * @property ProductRange $productRange
 * @property Tag $tag
 * @property ProductVariant[] $productVariants
 * @property Answer[] $answers
 *
 * @property ProductVariant[] $activeProductVariants
 */
class Product extends ActiveRecord
{
	public $nonTranslatables = array('id', 'productRangeId', 'tagId', 'code', 'gender', 'dayNight', 'bestSeller', 'status', 'created');
	public $translatables = array('name', 'details', 'detailBullets', 'url');

	const GENDER_NEUTRAL = 1;
	const GENDER_MALE = 2;
	const GENDER_FEMALE = 3;

	const DAY_NIGHT_NEUTRAL = 1;
	const DAY_NIGHT_DAY = 2;
	const DAY_NIGHT_NIGHT = 3;

	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 2;

	const IMAGE_SIZE_SMALL = '60x60';
	const IMAGE_SIZE_MEDIUM_LARGE = '250X250';
	const IMAGE_SIZE_LARGE = '450X450';

	public $score;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('productRangeId, code, name, gender, dayNight', 'required'),
			array('gender, dayNight, bestSeller, status', 'numerical', 'integerOnly'=>true),
			array('productRangeId, tagId', 'length', 'max'=>10),
			array('code, name', 'length', 'max'=>255),
			array('details, detailBullets, url', 'safe'),

			// The following rule is used by search().
			array('name, gender, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'categories' => array(self::MANY_MANY, 'Category', 'category_product(productId, categoryId)'),
			'productRange' => array(self::BELONGS_TO, 'ProductRange', 'productRangeId'),
			'tag' => array(self::BELONGS_TO, 'Tag', 'tagId'),
			'productVariants' => array(self::HAS_MANY, 'ProductVariant', 'productId'),
			'answers' => array(self::MANY_MANY, 'Answer', 'product_answer(productId, answerId)'),

			'activeProductVariants' => array(self::HAS_MANY, 'ProductVariant', 'productId', 'on'=>'activeProductVariants.price > 0 AND activeProductVariants.status = '.ProductVariant::STATUS_ACTIVE),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => _('ID'),
			'productRangeId' => _('Product Range'),
			'tagId' => _('Tag'),
			'code' => _('Code'),
			'name' => _('Name'),
			'details' => _('Details'),
			'detailBullets' => _('Detail Bullets'),
			'url' => _('URL'),
			'gender' => _('Gender'),
			'dayNight' => _('Day/Night'),
			'bestSeller' => _('Best Seller'),
			'order' => _('Order'),
			'status' => _('Status'),
			'created' => _('Created'),

			'genderName' => _('Gender'),
			'dayNightName' => _('Day/Night'),
			'statusName' => _('Status'),
			'detailBulletsHtml' => _('Detail Bullets'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('code',$this->code,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('gender',$this->gender);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Product the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return array gender values and names
	 */
	public function genders()
	{
		return array(
			self::GENDER_NEUTRAL => _('Neutral'),
			self::GENDER_MALE => _('Male'),
			self::GENDER_FEMALE => _('Female'),
		);
	}
	
	/**
	 * @return string a human-readable name for the gender
	 */
	public function getGenderName()
	{
		if($this->gender)
		{
			$genders = $this->genders();
			return $genders[$this->gender];
		}
	}

	/**
	 * @return array day/night values and names
	 */
	public function dayNightNames()
	{
		return array(
			self::DAY_NIGHT_NEUTRAL => _('Day/Night Neutral'),
			self::DAY_NIGHT_DAY => _('Day'),
			self::DAY_NIGHT_NIGHT => _('Night'),
		);
	}
	
	/**
	 * @return string a human-readable name for the day/night
	 */
	public function getDayNightName()
	{
		if($this->dayNight)
		{
			$dayNightNames = $this->dayNightNames();
			return $dayNightNames[$this->dayNight];
		}
	}

	/**
	 * @return array status values and names
	 */
	public function statuses()
	{
		return array(
			self::STATUS_ACTIVE => _('Active'),
			self::STATUS_INACTIVE => _('Inactive'),
		);
	}
	
	/**
	 * @return string a human-readable name for the status
	 */
	public function getStatusName()
	{
		if($this->status)
		{
			$statuses = $this->statuses();
			return $statuses[$this->status];
		}
	}

	/**
	 * @return string the product name parsed to strip the range
	 */
	public function getNameWithoutRange()
	{
		$range = $this->productRange->name;

		// if(stripos($this->name, $range) === 0)
			return trim(str_ireplace($range, '', $this->name));

		// return $this->name;
	}

	/**
	 * @return array price and stock values for the product variants queried
	 */
	public function getPriceStock($productCodes = array())
	{
		// Determine if we are querying the product instance, or multiple arbitrary products
		if($productCodes)
			$ids = implode(';', $productCodes);
		else
			$ids = $this->code;

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_URL, Yii::app()->params['priceStockUrl'][Yii::app()->language].$ids);
		$contents = curl_exec($curl);

		// Parse the response
		switch(curl_getinfo($curl, CURLINFO_HTTP_CODE)) {
			case 400: // Error
				$return = false;
				break;

			case 200: // Success
			default:
				$return = json_decode($contents);

				if(json_last_error() != JSON_ERROR_NONE)
					$return = false;

				break;
		}

		curl_close($curl);

		return $return;
	}

	/**
	 * Save product and sock values to database
	 */
	public function updatePriceStock($language)
	{
		// Temporarily alter the language during the update
		$userSetLanguage = Yii::app()->language;
		Yii::app()->setLanguage($language);

		// Get the price and stock
		$priceStock = $this->getPriceStock();

		// Update variant stock
		if($priceStock)
		{
			foreach($priceStock as $product)
			{
				foreach($product as $key => $data)
				{
					if($key == 'url')
					{
						$this->url = $data;
						$this->save();
					}
					elseif($key == 'skus')
					{
						foreach($data as $variantCode => $variantInfo)
						{
							if($productVariant = ProductVariant::model()->findByAttributes(array('code' => $variantCode)))
							{
								$productVariant->price = $variantInfo->price;
								$productVariant->stock = $variantInfo->stock;

								$productVariant->save();
							}
						}
					}
				}
			}
		}

		Yii::app()->setLanguage($userSetLanguage);
	}

	/**
	 * @return string a URL to the image
	 */
	public function getImageUrl($size)
	{
		return sprintf(Yii::app()->params['imageUrl'][$size], $this->code);
	}

	/**
	 * @return string pipe-delimited detail bullets as HTML ul/li
	 */
	public function getDetailBulletsHtml()
	{
		if($this->detailBullets)
		{
			$html = str_replace('|', '</li><li>', $this->detailBullets);

			return '<ul><li>'.$html.'</li></ul>';
		}
	}
}
