<?php
return CMap::mergeArray(
        require(dirname(__FILE__).'/main.php'),
        array(
                'sourceLanguage'=>'da_DK',
                'components'=>array(
                        'db'=>array(
                                'connectionString'=>'mysql:host=127.0.0.1;dbname=tbsidk_skincare',
                                'username'=>'tbsidk',
                                'password'=>'Retr0grade!',
                        ),
                        'mailEx' => array(
                                'from' => 'The Body Shop <no-reply@thebodyshop.dk>',
                        ),
                ),
                'params'=>array(
                'imageUrl' => array(
                                '60x60' => 'http://www.thebodyshop.dk/da/images/packshot/products/small/%s_s.jpg',
                                '250X250' => 'http://www.thebodyshop.dk/da/images/packshot/products/med_large/%s_m_l.jpg',
                                '450X450' => 'http://www.thebodyshop.dk/da/images/packshot/products/large/%s_l.jpg',
                        ),
                        'priceStockUrl' => array(
                                'da_DK' => 'http://www.thebodyshop.dk/da/ajax/catalog/skin-care-diagnostics.aspx?ids=',
                        ),
                        'populateBasketUrl' => array(
                                'da_DK' => 'http://www.thebodyshop.dk/da/populate_basket.aspx',
                        ),
                        'tag' => array(
                                'cleanseId' => '{see Step 4}',
                                'toneId' => '{see Step 4}',
                                'moisturiseId' => '{see Step 4}',
                        ),
                        'range' => array(
                                'semomileId' => '{see Step 4}'
                        ),
                 ),
        )
);                           
?> 
