<?php

return CMap::mergeArray(
	require(dirname(__FILE__).'/main.php'),
	array(
		'components'=>array(
			'db'=>array(
				'connectionString'=>'mysql:host=127.0.0.1;dbname=smack_tbsi-skincare',
				'username'=>'smack',
				'password'=>'Retr0grade!',	
				'enableProfiling'=>false,
				'enableParamLogging'=>true,
			),

			'request'=>array(
				'baseUrl'=>'/tbsi-skincare',
			),

			'log'=>array(
				'class'=>'CLogRouter',
				'routes'=>array(
					array(
						'class'=>'CProfileLogRoute',
						'levels'=>'profile',
						'enabled'=>true,
					),
				),
			),
		),
	)
);
