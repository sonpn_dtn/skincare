<?php

return CMap::mergeArray(
	require(dirname(__FILE__).'/main.php'),
	array(
		'modules'=>array(
			'gii'=>array(
				'class'=>'system.gii.GiiModule',
				'password'=>'password',
				'ipFilters'=>array('*','::1'),
			),
		),
	
		'components'=>array(
			'db'=>array(
                'connectionString'=>'mysql:host=192.168.0.1;dbname=dtnv4',
                'username'=>'dtn',
                'password'=>'r8zLQTNBb6wA',
				'enableProfiling'=>false,
				'enableParamLogging'=>true,
			),


			
			'log'=>array(
				'class'=>'CLogRouter',
				'routes'=>array(
					array(
						'class'=>'CProfileLogRoute',
						'levels'=>'profile',
						'enabled'=>true,
					),
				),
			),
		),
	)
);
