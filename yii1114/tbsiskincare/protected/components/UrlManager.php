<?php

class UrlManager extends CUrlManager
{
	/**
	 * Processes the URL rules.
	 */
	protected function processRules()
	{
		if(!isset($_GET['r']))
		{
			$this->setUrlFormat('path');
			$this->showScriptName=false;
			$this->rules=array(
				'<language:[a-z]{2}_[A-Z]{2}>/'=>'site/index',
				'<language:[a-z]{2}_[A-Z]{2}>/<controller:\w+>/<id:[0-9-]+>'=>'<controller>/view',
				'<language:[a-z]{2}_[A-Z]{2}>/<controller:\w+>/<action:\w+>/<id:[0-9-]+>'=>'<controller>/<action>',
				'<language:[a-z]{2}_[A-Z]{2}>/<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			);
		}
		return parent::processRules();
	}

	/**
	 * Constructs a URL.
	 * @param string $route the controller and the action (e.g. article/read)
	 * @param array $params list of GET parameters (name=>value). Both the name and value will be URL-encoded.
	 * If the name is '#', the corresponding value will be treated as an anchor
	 * and will be appended at the end of the URL.
	 * @param string $ampersand the token separating name-value pairs in the URL. Defaults to '&'.
	 * @return string the constructed URL
	 */
	public function createUrl($route,$params=array(),$ampersand='&')
	{
		if(isset($_GET['language']) && !isset($params['language']))
			$params['language']=$_GET['language'];

		if(!isset($params['language']))
			$params['language']=Yii::app()->language;

		return parent::createUrl($route, $params, $ampersand);
	}
}