<?php

class AnswerController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('view','create','update','delete','updateorder'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$productAnswer=new ProductAnswer;

		if(isset($_POST['ProductAnswer']))
		{
			$productAnswer->attributes=$_POST['ProductAnswer'];
			$productAnswer->answerId=$id;

			if($productAnswer->save())
				$productAnswer->unsetAttributes();
		}

		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'productAnswer'=>$productAnswer,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @param integer $questionid the ID of the quiz
	 */
	public function actionCreate($questionid)
	{
		$model=new Answer;
		$questionModel=Question::model()->findByPk($questionid);

		if(isset($_POST['Answer']))
		{
			$model->attributes=$_POST['Answer'];
			$model->order=count($model->question->answers) + 1;

			if(!$model->goToQuestionId)
				$model->goToQuestionId=null;
			
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
			'questionModel'=>$questionModel,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		if(isset($_POST['Answer']))
		{
			$model->attributes=$_POST['Answer'];

			if(!$model->goToQuestionId)
				$model->goToQuestionId=null;

			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model=$this->loadModel($id);
		$questionId=$model->questionId;
		$model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('question/view','id'=>$questionId));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Answer the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Answer::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Update question order
	 */
	public function actionUpdateorder()
	{
		foreach($_POST as $order => $answerId)
		{
			$answer = Answer::model()->findByPk($answerId);
			$answer->order = $order;
			$answer->save();
		}
	}

	/**
	 * Used in the CGridView on the view actions
	 */
	public function renderProductCode($data, $row) { return $data->product->code; }
	public function renderProductName($data, $row) { return $data->product->name; }
}
