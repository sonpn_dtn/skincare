/**	
 * Fonts
 */
@font-face {
	font-family: 'KnockoutRegular';
	src: url('../fonts/win/knockout-htf48-featherweigh.eot');
	src: url('../fonts/win/knockout-htf48-featherweigh.eot?#iefix') format('embedded-opentype'),
		 url('../fonts/win/knockout-htf48-featherweigh.woff') format('woff'),
		 url('../fonts/win/knockout-htf48-featherweigh.ttf') format('truetype'),
		 url('../fonts/win/knockout-htf48-featherweigh.svg#KnockoutRegular') format('svg');
	font-weight: normal;
	font-style: normal;
}

@font-face {
	font-family: 'GillSansMTStd';
	src: url('../fonts/win/gillsansmtstd-light.eot');
	src: url('../fonts/win/gillsansmtstd-light.eot?#iefix') format('embedded-opentype'),
		 url('../fonts/win/gillsansmtstd-light.woff') format('woff'),
		 url('../fonts/win/gillsansmtstd-light.ttf') format('truetype'),
		 url('../fonts/win/gillsansmtstd-light.svg#GillSansMTStdLight') format('svg');
	font-weight: 300;
	font-style: normal;
}

@font-face {
	font-family: 'GillSansMTStd';
	src: url('../fonts/win/gillsansmtstd-lightitalic.eot');
	src: url('../fonts/win/gillsansmtstd-lightitalic.eot?#iefix') format('embedded-opentype'),
		 url('../fonts/win/gillsansmtstd-lightitalic.woff') format('woff'),
		 url('../fonts/win/gillsansmtstd-lightitalic.ttf') format('truetype'),
		 url('../fonts/win/gillsansmtstd-lightitalic.svg#GillSansMTStdLightItalic') format('svg');
	font-weight: 300;
	font-style: italic;
}

@font-face {
	font-family: 'GillSansMTStd';
	src: url('../fonts/win/gillsansmtstd-medium.eot');
	src: url('../fonts/win/gillsansmtstd-medium.eot?#iefix') format('embedded-opentype'),
		 url('../fonts/win/gillsansmtstd-medium.woff') format('woff'),
		 url('../fonts/win/gillsansmtstd-medium.ttf') format('truetype'),
		 url('../fonts/win/gillsansmtstd-medium.svg#GillSansMTStdMedium') format('svg');
	font-weight: normal;
	font-style: normal;
}

@font-face {
	font-family: 'GillSansMTStd';
	src: url('../fonts/win/gillsansmtstd-bold.eot');
	src: url('../fonts/win/gillsansmtstd-bold.eot?#iefix') format('embedded-opentype'),
		 url('../fonts/win/gillsansmtstd-bold.woff') format('woff'),
		 url('../fonts/win/gillsansmtstd-bold.ttf') format('truetype'),
		 url('../fonts/win/gillsansmtstd-bold.svg#GillSansMTStdBold') format('svg');
	font-weight: bold;
	font-style: normal;
}

@font-face {
	font-family: 'GillSansRegular';

	src: url('../fonts/win/gillsans-semibold.eot');
	src: url('../fonts/win/gillsans-semibold.eot?#iefix') format('embedded-opentype'),
		 url('../fonts/win/gillsans-semibold.woff') format('woff'),
		 url('../fonts/win/gillsans-semibold.ttf') format('truetype'),
		 url('../fonts/win/gillsans-semibold.svg#GillSansRegular') format('svg');
	font-weight:600;
	font-style: normal;
}

/**	
 * Core
 */
*,
*:after,
*:before {
	box-sizing: border-box;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-font-smoothing: antialiased;
	font-smoothing: antialiased;
	text-rendering: optimizeLegibility;
}

html,
body {
	font-family: <?php echo $font;?>;
}

.nodisplay,
.mobile-inline,
.mobile-block {
	display: none;
}

.hidden {
	visibility: hidden;
}

.disabled {
	opacity: .5;
}

.content-container {
	color : <?php echo $primaryColour; ?>;
	font-size: <?php echo $bodySize; ?>;
	margin: 0 auto;
	max-width: 1024px;
	overflow-x: hidden;
}

.content-container .content {
	height: 100%;
}

/**
 * Tooltip
 */
.ui-tooltip,
.arrow:after {
	background: <?php echo $primaryColour; ?>;
}

.ui-tooltip {
	padding: 6px 8px 4px;
	color: #009a4e;
	font: 10px <?php echo $fontReg;?>;
	font-weight:600;
	text-transform: uppercase;
	-webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .1);
	box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .1);
}

.arrow {
	width: 70px;
	height: 10px;
	overflow: hidden;
	position: absolute;
	left: 50%;
	margin-left: -35px;
	bottom: -10px;
}

.arrow.top {
	top: -16px;
	bottom: auto;
}

.arrow.left {
	left: 20%;
}

.arrow:after {
	content: "";
	position: absolute;
	left: 20px;
	top: -20px;
	width: 25px;
	height: 20px;
	box-shadow: 6px 5px 9px -9px black;
	-webkit-transform: rotate(45deg);
	-moz-transform: rotate(45deg);
	-ms-transform: rotate(45deg);
	-o-transform: rotate(45deg);
	tranform: rotate(45deg);
}

.arrow.top:after {
	bottom: -20px;
	top: auto;
}

/**
 * Forms
 */
form label {
	font-family: <?php echo $fontReg;?>;
	font-weight:600;
	text-transform: uppercase;
}

form label.error {
	/*color: #f00;*/
}

form input[disabled] {
	opacity: .5;
}

form .errorMessage {
	color: #ff3d3d;
	font-size: .875em;
}

/**
 * Forms: Text Fields
 */
form .text {
	margin-bottom: 15px;
}

form .text label {
	margin-top: 11px;
}

form .text input {
	background-color: transparent;
	border: none;
	border-bottom: 1px solid #fff;
	border-radius: 0;
	-webkit-border-radius: 0;
	color: <?php echo $primaryColour; ?>;
	font: 1.643em <?php echo $fontHead;?>;
	outline: none;
	padding: 0;
	text-transform: uppercase;
	-webkit-appearance: none;
}

/**
 * Forms: Email Fields
 */
form .email input {
	border: 1px solid #d5d5d5;
	color: #424242;
	font: .875em <?php echo $font;?>;
	margin: 15px 0 10px;
	outline: none;
	padding: 10px 21px 7px;
	width: 100%;
	-webkit-appearance: none;
}

form .email input.error {
	border-color: #ff3d3d;
}

/**
 * Forms: Checkboxes
 */
form .checkbox label {
	text-transform: none;
}

form .checkbox input {
	vertical-align: text-bottom;
}

/**
 * Forms: Selections
 */
form .selection-container {
	margin-top: 10px;
}

form .selection {
	background-color: rgba(<?php echo $appShading; ?>, .4);
	cursor: pointer;
	float: left;
	font: .857em <?php echo $fontReg;?>;
	font-weight:600;
	height: 80px;
	margin: 0 0 25px 4%;
	padding: 0 10px;
	text-align: center;
	text-transform: uppercase;
	width: 22%;
}

form .selection .table {
	display: table;
	height: 100%;
	width: 100%;
}

form .selection .table span {
	display: table-cell;
	vertical-align: middle;
}

form .selection:first-child {
	margin-left: 0;
}

form .selection.active {
	border: 2px solid <?php echo $primaryColour; ?>;
	padding: 0 8px;
}

/**
 * Forms: Buttons
 */
form .button {
	text-align: right;
}

form .button a,
form .button input {
	background: transparent url('../images/arrow-next.png') no-repeat right 6px;
	border: none;
	color: <?php echo $primaryColour; ?>;
	cursor: pointer;
	font: 1em <?php echo $fontReg;?>;
	font-weight:600;
	margin: 0 0 10px 0;
	outline: none;
	padding: 0 10px 0 0;
	text-decoration: underline;
	text-transform: uppercase;

	-webkit-appearance: none;
	-webkit-transition: opacity .2s;
	transition: opacity .2s;
}

/**
 * Breadcrumbs
 */
.breadcrumbs {
	bottom: 0;
	background-color: rgba(<?php echo $appShading; ?>, .2);
	padding: 4% 6.5% .5%;
	position: absolute;
	width: 100%;
}

.breadcrumbs .cap-left,
.breadcrumbs .cap-right {
	position: absolute;
	margin-top: -4px;
}

.breadcrumbs .cap-right {
	right: 0;
}

.breadcrumbs .progress-bar {
	background-color: #003612;
	height: 2px;
	position: relative;
}

.breadcrumbs .progress-bar .progress {
	background-color: #fff;
	height: 2px;
	width: 0;
}

.breadcrumbs .col {
	color: #002f16;
	font-size: .714em;
	margin-top: 1px;
	text-transform: uppercase;
	text-align: center;
}

.breadcrumbs .col a {
	display: inline-block;
	padding: 12px 8px;
	text-decoration: none;
}

.breadcrumbs .col.answered,
.breadcrumbs .col.answered a {
	color: <?php echo $primaryColour; ?>;
}

.breadcrumbs .col.skipped {
	opacity: .7;
}

.breadcrumbs .col.current {
	background: url('../images/arrow-current.png') no-repeat center top;
	color: <?php echo $primaryColour; ?>;
}

.breadcrumbs .col.current a {
	color: <?php echo $primaryColour; ?>;
}

/**
 * Site: Index
 */
#site-index .content-container {
	background-image: url(<?php echo $bg;?>);
	height: 568px;
}

#site-index .content-left {
	background-color: rgba(0, 0, 0, .4);
	height: 100%;
	padding: 4% 5%;
}

#site-index .content-left p {
	font-size: 1.143em;
	line-height: 1.25em;
}

#site-index .content-left a {
	color: #fff;
}

#site-index .content-right {
	padding: 4% 5% 0 6%;
}

#site-index .h1 {
	font: 4.143em <?php echo $fontHead;?>;
	margin-top: 20px;
	text-transform: uppercase;
}

#site-index h1 {
	display: none;
}

#site-index h2 {
	font: 2.143em <?php echo $fontHead;?>;
	text-transform: uppercase;
	margin-bottom: 18px;
}

#site-index form .selection {
	float: right;
	margin-bottom: 0;
	width: 36%;
}

#site-index form .selection:first-child {
	margin-left: 10%;
}

#site-index form .selection:last-child {
	margin-left: 0;
}

#site-index form .selection-container {
	margin-bottom: 15px;
	text-align: right;
}

#site-index form .button {
	margin-top: -12px;
	display:block
}

#site-index .hint{
	margin: 0;
}

#site-index .span_6_of_13 {
	width: 45%;
	margin-left: 3%;
}

#site-index .span_1_of_13 {
	width: 4%; 
}
#site-index .group.text .span_1_of_13 {
        width:78%;
        display:block;
        padding-bottom:10px;
	margin-top:-10px;
}
#site-index .group.text .span_6_of_13{
        width:48%;
        margin-left:0%;
}
.first-name {
        margin-right:4%;
}

#site-index .span_1_of_13 {
        width: 4%;
}

/**
 * Question: View / Site: Summary
 */
#question-view .content-container {
	background-image: url(<?php echo $bgShaded;?>);
	min-height: 568px;
	position: relative;
}

#question-view .did-you-know {
	position: absolute;
	right: 0;
}

#question-view .back {
	background: none;
	color: <?php echo $primaryColour; ?>;
	font: .813em <?php echo $fontReg;?>;
	font-weight:600;
	text-decoration: none;
	text-transform: uppercase;
}

#question-view .back span {
	text-decoration: underline;
}

#question-view .back.inactive {
	visibility: hidden;
}

#question-view .content-left,
#site-summary .content-left {
	height: 100%;
	position: relative;
}

#question-view .question-container,
#site-summary .question-container {
	padding: 5.5% 6.5%;
}

#question-view h1,
#site-summary h1 {
	font: 4.143em <?php echo $fontHead;?>;
	margin-bottom: 10px;
	text-transform: uppercase;
}

#question-view .question-container p {
	font-size: 1.143em;
	line-height: 1.188em;
	min-height: 2.25em;
}

#site-summary .question-container p.summary-blurb {
	font-size: 1.143em;
	line-height: 1.188em;
	min-height: 2.25em;
}

#question-view form {
	margin-top: 20px;
}

#question-view .option-3,
#question-view .option-4 {
	padding: 50px 0;
}

#question-view .option-5 .selection:nth-child(3),
#question-view .option-6 .selection:nth-child(4),
#question-view .option-7 .selection:nth-child(5),
#question-view .option-8 .selection:nth-child(5) {
	clear: both;
	margin-left: 0;
}

#question-view .span_3_of_4.hint {
	width: 74%;
}

#question-view .span_1_of_4	.button {
	width: 26%;
}

/**
 * Summary
 */
.user-summary {
	margin: 1em 0;
}

.user-summary strong {
	text-transform: uppercase;
}

.user-summary strong,
.user-summary span,
.user-summary a {
	color: #555;
	font-family: <?php echo $fontReg;?>;
	font-weight:600;
	font-weight: normal;
}

#site-summary .user-summary strong,
#site-summary .user-summary span,
#site-summary .user-summary a {
	color: <?php echo $primaryColour; ?>;
}

#site-summary .content-container {
	background-image: url(<?php echo $bgShaded;?>);
	height: 509px;
}

#site-summary .user-summary {
	font-size: 1.143em;
	line-height: 1.5em;
}

#site-summary .user-summary span {
	/* text-decoration: underline; */
}

#site-summary .ready {
	text-align: right;
}

#site-summary .ready a {
	color: <?php echo $primaryColour; ?>;
	display: inline-block;
	font-family: <?php echo $fontReg;?>;
	font-weight:600;
	margin-top: 30px;
	text-decoration: none;
	text-transform: uppercase;
}

#site-summary .ready a span {
	text-decoration: underline;
}

/**
 * Did You Know
 */
.did-you-know {
	background-color: rgba(<?php echo $appShading; ?>, .4);
	height: 100%;
	padding: 105px 4.5% 0;
	text-align: center;
}

.did-you-know h2 {
	font: 300 1.643em <?php echo $font;?>;
	text-transform: uppercase;
}

.did-you-know h2 em {
	font-style: normal;
	font-weight: normal;
}

.did-you-know.female h2 em {
	color: #59be74;
}

.did-you-know p {
	line-height: 1.25em;
}

/**
 * Hint
 */
.hint {
	background-color: rgba(0, 0, 0, .4);
	border: 2px solid <?php echo $primaryColour; ?>;
	font-size: .929em;
	visibility: hidden;
	margin: 20px 0 40px;
	padding: 15px;
}

.hint.active {
	visibility: visible;
}

.hint-title {
	font-family: <?php echo $fontReg;?>;
	font-weight:600;
	text-transform: uppercase;
	min-height: 1.5em;
	padding-bottom: 5px;
}

.hint-description {
	line-height: 1.188em;
	min-height: 4.6em;
}

#site-index .hint-container {
	margin: 30px 0;
}

#site-index .hint {
	float: right;
	width: 82%;
}

/**
 * Site: Result
 */
#site-result .content {
	color: #3e3e3e;
	padding: 30px 5%;
	position: relative;
}

#site-result .skincare-soulmates h1,
#site-result .skincare-summary h2 {
	font: 2.214em <?php echo $fontHead;?>;
	margin-bottom: 5px;
	text-transform: uppercase;
}

#site-result .skincare-soulmates p,
#site-result .skincare-summary p {
	color: #555;
	line-height: 1.188em;
}

#site-result p.user-summary {
	line-height: 1.25em;
}

#site-result .result-blurb .skincare-soulmates {
	background: url('../images/line.png') no-repeat right center;
	padding-right: 4%;
}

#site-result .product-tabs .day-night-products {
	padding-right: .5%;
}

#site-result .product-tabs .other-products {
	padding-left: .5%;
}

#site-result .skincare-summary {
	padding-left: 2.25%;
}

#site-result .ready {
	font-family: <?php echo $fontReg;?>;
	font-weight:600;
	text-transform: uppercase;
}

#site-result form label {
	color: #666;
	display: none;
	font: .875em <?php echo $font;?>;
	vertical-align: text-top;
	width: 90%;
}

#site-result form label.active {
	display: inline-block;
}

#site-result form .button-container {
	margin-top: 10px;
}

#site-result form .button input {
	background-image: url('../images/arrow-green-next.png');
	color: #27bc2b;
}

#site-result form .thanks {
	color: #7fb719;
}

#site-result form .thanks span {
	font-family: <?php echo $fontReg;?>;
	font-weight:600;
	text-transform: uppercase;
}

#site-result .bottom-right {
	text-align: right;
}

#site-result .promotion {
	color: #666;
	font: .938em <?php echo $fontReg;?>;
	font-weight:600;
	margin-bottom: 10px;
}

#site-result .add-to-bag {
	background: #27bc2b url('../images/arrow-next.png') no-repeat 156px 8px;
	color: <?php echo $primaryColour; ?>;
	display: inline-block;
	font: .875em <?php echo $fontReg;?>;
	font-weight:600;
	min-width: 170px;
	padding: 6px 20px 4px 10px;
	text-align: center;
	text-decoration: none;
	text-transform: uppercase;
}

#site-result .add-to-bag.clone {
	position: absolute;
	right: 0;
}

#site-result .checkbox-container {
	background-color: #27bc2b;
	color: <?php echo $primaryColour; ?>;
	cursor: pointer;
	display: inline-block;
	font-family: <?php echo $fontReg;?>;
	font-weight:600;
	margin: 4px 4px 0 0;
	padding: 4px 8px;
	text-transform: uppercase;
}

#site-result .checkbox-container input {
	margin: 0;
	vertical-align: baseline;
}

#site-result .share {
	font-size: .714em;
	margin-top: 10px;
	text-transform: uppercase;
}

#site-result .share span {
	display: inline-block;
	margin-right: 8px;
}

#site-result .share img {
	cursor: pointer;
	vertical-align: middle;
}

#site-result .start-again,
#site-result .print {
	color: #27bc2b;
	display: inline-block;
	font: .938em <?php echo $fontReg;?>;
	font-weight:600;
	margin-top: 20px;
	text-decoration: none;
	text-transform: uppercase;
}

#site-result .start-again span,
#site-result .print span {
	text-decoration: underline;
}

#site-result .print-container {
	text-align: right;
}

#site-result .printOnly{
	display:none;
}

/**
 * Site: Result: Tabs
 */
.ui-tabs,
.ui-tabs .ui-tabs-nav,
.ui-tabs .ui-tabs-nav li {
	margin: 0;
	padding: 0;
}

.ui-tabs {
	margin-top: 20px;
}

.ui-tabs .ui-tabs-nav li {
	font-size: .893em;
}

.ui-tabs .ui-tabs-nav .ui-tabs-anchor {
	color: <?php echo $primaryColour; ?>;
	margin-top: .25em;
	outline: none;
	padding: 0.8em 1em .5em 2.25em;
	text-transform: uppercase;
}

.ui-tabs .ui-tabs-nav .ui-tabs-anchor.tab-day {
	background: #27bc2b url('../images/icon-day.png') no-repeat 10px center;
}

.ui-tabs .ui-tabs-nav .ui-tabs-anchor.tab-night {
	background: #007532 url('../images/icon-night.png') no-repeat 10px center;
}

.ui-tabs .ui-tabs-nav .ui-tabs-anchor.tab-other {
	background: #bae07d url('../images/icon-other.png') no-repeat 10px center;
	font-family: <?php echo $fontReg;?>;
	font-weight:600;
	/*-webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .1);*/
	/*box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .1);*/
}

.ui-tabs .ui-tabs-nav li.ui-tabs-active,
.ui-tabs .ui-tabs-nav li.ui-fake-active {
	font: 1em <?php echo $fontReg;?>;
	font-weight:600;
}

.ui-tabs .ui-tabs-nav li.ui-tabs-active .ui-tabs-anchor,
.ui-tabs .ui-tabs-nav li.ui-fake-active .ui-tabs-anchor {
	margin-top: 0;
	padding-left: 2em;
}

/**
 * Site: Result: Tab Panels
 */
.ui-tabs .ui-tabs-panel {
	border: 1px solid #e2e2e2;
	font-size: 12px;
	padding: 0;
}

.ui-tabs .ui-tabs-panel#tab-other {
	background-color: #f6fee8;
	-webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .1);
	box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .1);
}

.ui-tabs .ui-tabs-panel li {
	cursor: pointer;
	color: #666;
	padding: 15px 15px 10px;
}

.ui-tabs .ui-tabs-panel li.active,
.drops-of-youth.active {
	-webkit-box-shadow: 0px 0px 16px 0px rgba(39, 188, 43, .3);
	box-shadow: 0px 0px 16px 0px rgba(39, 188, 43, .3);	
}

.ui-tabs .ui-tabs-panel li img {
	width: 100%;
}

.ui-tabs .ui-tabs-panel li .product-tag {
	font-family: <?php echo $fontReg;?>;
	font-weight:600;
	margin-bottom: 20px;
	text-transform: uppercase;
}

.ui-tabs .ui-tabs-panel li .product-name {
	font-size: .938em;
	margin-top: 20px;
	min-height: 3em;
	text-transform: uppercase;
}

.ui-tabs .ui-tabs-panel li .product-name .product-range {
	font-family: <?php echo $fontReg;?>;
	font-weight:600;
}

/**
 * Site: Result: Product Detail
 */
.mobile-tab-container .mobile-tabs,
.mobile-tab-container .product-nav {
	display: none;
}

.product-detail-container {
	background: url('../images/bg.jpg') no-repeat center center;
	background-size: cover;
	color: <?php echo $primaryColour; ?>;
	display: none;
	margin: 10px 0 20px;
	padding: 35px 20px 15px 15px; 
	position: relative;
}

.product-detail-container.active {
	display: block;
}

.product-detail-container .span_2_of_8 {
	padding: 5px 30px 4px 10px;
}

.product-detail-container .span_2_of_8 img {
	width: 100%;
}

.product-detail-container .best-seller {
	background-color: #7fb719;
	border-radius: 30px;
	color: <?php echo $primaryColour; ?>;
	height: 60px;
	font: .913em <?php echo $fontReg;?>;
	font-weight:600;
	line-height: 1.063em;
	margin: 0.7% 0 0 12.5%;
	padding: 18px 5px 0;
	position: absolute;
	text-align: center;
	text-transform: uppercase;
	width: 60px;
}

.product-detail .product-name {
	font-size: 1.786em;
	font-weight: 300;
	margin-top: 5px;
	text-transform: uppercase;
}

.product-detail .product-name .product-range {
	font-weight: normal;
}

.product-detail p {
	margin-top: 5px;
	line-height: 1.188em;
}

.product-detail ul {
	margin: 10px 0 0 15px;
	line-height: 1.25em;
}

.product-arrow {
	position: absolute;
	margin-top: 10px;
	/*left: 110px;*/
	z-index: 1;
}

/**
 * Site: Result: Product Detail: Product Variants
 */
.product-detail-container .product-variants ul {
	bottom: 20px;
	position: absolute;
	right: 25px;
}

.product-detail-container .product-variants ul li {
	display: inline;
	float: right;
	margin-left: 20px;
	list-style-type: none;
	text-align: left;
}

ul li .product-variant {
	font-weight: 300;
	margin-top: 8px;
}

ul li .product-price input {
	margin-left: 0;
	vertical-align: text-bottom;
}

.product-detail-container .product-variants-mobile {
	display: none;
}

/**
 * Site: Result: Product Detail: Product Navigation
 */
.product-detail-container .product-nav {
	padding: 0 10px 10px;
}

.product-detail-container .product-nav .previous,
.product-detail-container .product-nav .next {
	width: 50%;
}

.product-detail-container .product-nav .previous span,
.product-detail-container .product-nav .next span {
	text-decoration: underline;
	text-transform: uppercase;
}

.product-detail-container .product-nav .previous {
	float: left;
}

.product-detail-container .product-nav .next {
	float: right;
	text-align: right;
}

/**
 * Site: Result: Drops of Youth
 */
.drops-of-youth {
	color: #666;
	font-size: .857em;
	max-width: 410px;	
	-webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .1);
	box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .1);
}

.drops-of-youth img {
	padding-top: 10px;
	width: 100%;
}

.drops-of-youth p {
	line-height: 1.188em;
}

.drops-of-youth .span_2_of_3 {
	padding: 25px 35px 15px 0;
}

.drops-of-youth .product-name {
	font-family: <?php echo $fontReg;?>;
	font-weight:600;
	margin-bottom: 10px;
	text-transform: uppercase;
}

.drops-of-youth .product-price {
	margin-top: 10px;
}

#site-result.android .product-price input {
	vertical-align: middle;
}

#site-result .drops-of-youth .product-variant {
	font-weight: 300;
}

/**
 * Retina
 */
@media all and (-webkit-min-device-pixel-ratio: 1.5) {
	form .button a,
	form .button input {
		background-image: url('../images/arrow-next@2x.png');
		background-size: 4px 7px;
	}

	.breadcrumbs .col.current {
		background-image: url('../images/arrow-current@2x.png');
		background-size: 11px 6px;
	}

	#site-result form .button input {
		background-image: url('../images/arrow-green-next@2x.png');
		background-size: 4px 7px;
	}

	#site-result .add-to-bag {
		background-image: url('../images/arrow-next@2x.png');
		background-size: 4px 7px;
	}

	.ui-tabs .ui-tabs-nav .ui-tabs-anchor.tab-day {
		background-image: url('../images/icon-day@2x.png');
		background-size: 14px 14px;
	}

	.ui-tabs .ui-tabs-nav .ui-tabs-anchor.tab-night {
		background-image: url('../images/icon-night@2x.png');
		background-size: 14px 14px;
	}

	.ui-tabs .ui-tabs-nav .ui-tabs-anchor.tab-other {
		background-image: url('../images/icon-other@2x.png');
		background-size: 14px 14px;
	}
}
