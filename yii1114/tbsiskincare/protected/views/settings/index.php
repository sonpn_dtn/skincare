<?php

$this->pageTitle=Yii::app()->name.' - '._('Settings');
$this->breadcrumbs=array(
	_('Settings'),
);
?>

<h1><?php echo _('Settings'); ?></h1>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm',array('htmlOptions'=>array('enctype'=>'multipart/form-data'))); ?>
 
	<div class="row">
		<?php echo CHtml::label(_('Hero Product'), 'hero-product-id'); ?>
		<p class="hint"><?php echo _('The product to be featured on the results page'); ?></p>
		<input type="text" name="term" id="product-search" data-rel="hero-product-id" placeholder="<?php echo _('Search products (type product code or name)'); ?>" size="60" maxlength="255" value="<?php if($heroProduct) echo $heroProduct->code.': '.$heroProduct->name; ?>" />
		<?php echo CHtml::hiddenField('hero-product-id', $heroProduct ? $heroProduct->id : null); ?>
	</div>

	<div class="row">
		<?php echo CHtml::label(_('Primary Colour'), 'primary-colour'); ?>
		<p class="hint"><?php echo _('The colour of text within the app (default: #fff)'); ?></p>
		<input type="text" name="primary-colour" id="primary-colour" placeholder="#fff" size="60" maxlength="255" value="<?php if($settings->primaryColour) echo $settings->primaryColour; ?>" />
	</div>

	<div class="row">
		<?php echo CHtml::label(_('Base Font Size'), 'body-size'); ?>
		<p class="hint"><?php echo _('All font sizes will be calculated from the base size, increase or decrease as required (default: 14px)'); ?></p>
		<input type="text" name="body-size" id="body-size" placeholder="14px" size="60" maxlength="255" value="<?php if($settings->bodySize) echo $settings->bodySize; ?>" />
	</div>

	<div class="row">
  		<?php echo CHtml::label(_('App Shading Colour'), 'app-shading'); ?>
  		<p class="hint"><?php echo _('The app shading colour is used as a background to contain text and increase legibility (default: #000)'); ?></p>
		<input type="text" name="app-shading" id="app-shading" placeholder="#000" size="60" maxlength="255" value="<?php if($settings->appShading) echo CString::rgb2hex($settings->appShading); ?>" />
	</div>

	<div class="row">
		<?php echo CHtml::label(_('Heading Font Family'), 'font-family-heading'); ?>
		<p class="hint">
			<?php echo _('The name of the font family for title heading text from Google Fonts'); ?><br />
			<a href="<?php echo Yii::app()->baseUrl; ?>/images/blueprint/google-fonts.png" target="_blank"><?php echo _('Click here to see a screenshot of how to find your Google Fonts name'); ?></a>
		</p>
		<input type="text" name="font-family-heading" id="font-family-heading" size="60" maxlength="255" value="<?php if($settings->fontFamilyHeading) echo $settings->fontFamilyHeading; ?>" />
	</div>

	<div class="row">
		<?php echo CHtml::label(_('Body Font Family'), 'font-family'); ?>
		<p class="hint">
			<?php echo _('The name of the font family for body text from Google Fonts'); ?><br />
			<a href="<?php echo Yii::app()->baseUrl; ?>/images/blueprint/google-fonts.png" target="_blank"><?php echo _('Click here to see a screenshot of how to find your Google Fonts name'); ?></a>
		</p>
		<input type="text" name="font-family" id="font-family" size="60" maxlength="255" value="<?php if($settings->fontFamily) echo $settings->fontFamily; ?>" />
	</div>

	<div class="row">
		<?php echo CHtml::label(_('Background Image'), 'file'); ?>
		<p class="hint"><?php echo _('The background image displayed throughout the app (upload dimensions must be 1024x680)'); ?></p>
		<?php echo $form->fileField($settings, 'file', array('class' => 'file')); ?>
		<br />
		<img src="<?php echo $settings->fileName ? $settings->getUrl() : Yii::app()->baseUrl.'/images/bg.jpg'; ?>" width="200" alt="" />
	</div>

	<div class="row">
		<?php echo CHtml::label(_('GA Code'), 'ga-code'); ?>
		<input type="text" name="ga-code" id="ga-code" size="60" maxlength="255" value="<?php if($settings->gaCode) echo $settings->gaCode; ?>" />
	</div>

	<div class="row submit">
		<?php echo CHtml::submitButton('Save'); ?>
	</div>
 
<?php  $this->endWidget(); ?>
</div>