<?php
/* @var $this ProductRangeController */
/* @var $model ProductRange */

$this->breadcrumbs=array(
	_('Products')=>array('product/admin'),
	_('Product Ranges'),
);

$this->menu=array(
	array('label'=>_('Create Product Range'), 'url'=>array('create')),
);
?>

<h1><?php echo _('Manage Product Ranges'); ?></h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'product-range-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'name',
		'hexCode',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{update}',
		),
	),
));