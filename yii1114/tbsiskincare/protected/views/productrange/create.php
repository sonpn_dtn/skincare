<?php
/* @var $this ProductRangeController */
/* @var $model ProductRange */

$this->breadcrumbs=array(
	_('Products')=>array('product/admin'),
	_('Product Ranges')=>array('productrange/admin'),
	_('Create Product Range'),
);
?>

<h1><?php echo _('Create Product Range'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>