<?php
/* @var $this TagController */
/* @var $model Tag */

$this->breadcrumbs=array(
	_('Products')=>array('product/admin'),
	_('Tags')=>array('tag/admin'),
	_('Create Tag'),
);
?>

<h1><?php echo _('Create Tag'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>