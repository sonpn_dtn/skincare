<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->breadcrumbs=array(
	_('Categories'),
	$model->name,
	'Update',
);

$this->menu=array(
	array('label'=>_('View Category'), 'url'=>array('view', 'id'=>$model->id)),
);
?>

<h1><?php echo _('Update Category'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>