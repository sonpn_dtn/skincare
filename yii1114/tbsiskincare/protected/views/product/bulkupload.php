<?php
/* @var $this ProductController */
/* @var $model Product */

$this->breadcrumbs=array(
	_('Products')=>array('admin'),
	_('Bulk Upload'),
);

$this->menu=array(
	array('label'=>_('Manage Products'), 'url'=>array('admin')),
);
?>

<h1><?php echo _('Bulk Upload'); ?></h1>

<p><?php echo _('The bulk upload is intended for initial population of products.'); ?></p>

<ul>
	<li><?php echo _('Data must be uploaded in CSV format.'); ?></li>
	<li><?php echo _('The data uploaded will overwrite all information.'); ?></li>
</ul>

<div class="form">

<?php $form = $this->beginWidget('CActiveForm',array('htmlOptions'=>array('enctype'=>'multipart/form-data'))); ?>

	<p class="note"><?php echo _('Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<?php /* <div class="row rememberMe">
		<?php echo $form->labelEx($model,'categoryCsv'); ?>
		<?php echo $form->fileField($model,'categoryCsv'); ?>
		<br />
		<?php echo $form->checkBox($model,'categoryCsvFirstLine',array('onclick'=>'return false;')); ?>
		<?php echo $form->labelEx($model,'categoryCsvFirstLine'); ?>
		<br />
		<?php echo $form->checkBox($model,'categoryCsvSecondLine'); ?>
		<?php echo $form->labelEx($model,'categoryCsvSecondLine'); ?>

		<?php echo $form->error($model,'categoryCsv'); ?>
	</div>

	<div class="row rememberMe">
		<?php echo $form->labelEx($model,'categoryProductCsv'); ?>
		<?php echo $form->fileField($model,'categoryProductCsv'); ?>
		<br />
		<?php echo $form->checkBox($model,'categoryProductCsvFirstLine',array('onclick'=>'return false;')); ?>
		<?php echo $form->labelEx($model,'categoryProductCsvFirstLine'); ?>
		<br />
		<?php echo $form->checkBox($model,'categoryProductCsvSecondLine'); ?>
		<?php echo $form->labelEx($model,'categoryProductCsvSecondLine'); ?>

		<?php echo $form->error($model,'categoryProductCsv'); ?>
	</div> */ ?>

	<div class="row rememberMe">
		<?php echo $form->labelEx($model,'productCsv'); ?>
		<?php echo $form->fileField($model,'productCsv'); ?>
		<br />
		<?php echo $form->checkBox($model,'productCsvFirstLine',array('onclick'=>'return false;')); ?>
		<?php echo $form->labelEx($model,'productCsvFirstLine'); ?>
		<br />
		<?php echo $form->checkBox($model,'productCsvSecondLine'); ?>
		<?php echo $form->labelEx($model,'productCsvSecondLine'); ?>

		<?php echo $form->error($model,'productCsv'); ?>
	</div>

	<div class="row rememberMe">
		<?php echo $form->labelEx($model,'productReferenceCsv'); ?>
		<?php echo $form->fileField($model,'productReferenceCsv'); ?>
		<br />
		<?php echo $form->checkBox($model,'productReferenceCsvFirstLine',array('onclick'=>'return false;')); ?>
		<?php echo $form->labelEx($model,'productReferenceCsvFirstLine'); ?>
		<br />
		<?php echo $form->checkBox($model,'productReferenceCsvSecondLine'); ?>
		<?php echo $form->labelEx($model,'productReferenceCsvSecondLine'); ?>

		<?php echo $form->error($model,'productReferenceCsv'); ?>
	</div>

	<div class="row rememberMe">
		<?php echo $form->checkBox($model,'confirm'); ?>
		<?php echo $form->labelEx($model,'confirm'); ?>
		<?php echo $form->error($model,'confirm'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton(_('Upload')); ?>
	</div>

<?php $this->endWidget(); ?>

</div>