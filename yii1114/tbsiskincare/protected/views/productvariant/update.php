<?php
/* @var $this ProductVariantController */
/* @var $model ProductVariant */

$this->breadcrumbs=array(
	_('Products')=>array('product/admin'),
	$model->product->name=>array('product/view','id'=>$model->product->id),
	$model->name=>array('view','id'=>$model->id),
	_('Update'),
);

$this->menu=array(
	array('label'=>_('View Product Variant'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'<hr style="margin: 0;" />'),
	array('label'=>_('Back to Product'), 'url'=>array('product/view','id'=>$model->product->id)),
);
?>

<h1><?php echo _('Update Product Variant'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>