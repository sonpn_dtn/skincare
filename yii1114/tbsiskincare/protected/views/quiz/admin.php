<?php
/* @var $this QuizController */
/* @var $model Quiz */

$this->breadcrumbs=array(
	_('Quizzes')=>array('admin'),
	_('Manage'),
);

$this->menu=array(
	array('label'=>_('Create Quiz'), 'url'=>array('create')),
	array('label'=>'<hr style="margin: 0;" />'),
	array('label'=>_('Update Localisations'), 'url'=>array('site/loco')),

);

if(Yii::app()->user->hasFlash('message')) { ?>
	<div class="flash-success">
		<?php echo Yii::app()->user->getFlash('message'); ?>
	</div>
<?php } ?>

<h1><?php echo _('Manage Quizzes'); ?></h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'quiz-grid',
	'dataProvider'=>$model->search(),
	'template'=>'{items}',
	'columns'=>array(
		array(
			'name'=>'regime',
			'value'=>'$data->regimeName',
			'filter'=>$model->regimes(),
		),
		array(
			'name'=>'gender',
			'value'=>'$data->genderName',
			'filter'=>$model->genders(),
		),
		array(
			'name'=>'status',
			'value'=>'$data->statusName',
			'filter'=>$model->statuses(),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{update}',
		),
	),
)); ?>
