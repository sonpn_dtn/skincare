<?php
/* @var $this QuizController */
/* @var $model Quiz */

$this->breadcrumbs=array(
	_('Quizzes')=>array('admin'),
	_('Create'),
);

$this->menu=array(
	array('label'=>_('Manage Quizzes'), 'url'=>array('admin')),
);
?>

<h1><?php echo _('Create Quiz'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>