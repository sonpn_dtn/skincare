<?php
/* @var $this QuizController */
/* @var $model Quiz */

$this->breadcrumbs=array(
	_('Quizzes')=>array('admin'),
	$model->regimeName.' | '.$model->genderName,
);

$this->menu=array(
	array('label'=>_('Update Quiz'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>_('Delete Quiz'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('zii','Are you sure you want to delete this item?'))),
	array('label'=>'<hr style="margin: 0;" />'),
	array('label'=>_('Add Question'), 'url'=>array('question/create','quizid'=>$model->id)),
);

?>

<h1><?php echo _('View Quiz'); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'regimeName',
		'genderName',
		'statusName',
		'siteCatalyst_subCategoryKey',
		'siteCatalyst_subCategoryValue',
	),
)); ?>

<h3 style="margin: 1em 0 0;"><?php echo _('Questions'); ?></h3>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'question-grid',
	'dataProvider'=>$questionModel->search(),
	'htmlOptions'=>array('data-updateorderurl'=>$this->createUrl('question/updateorder')),
	'rowHtmlOptionsExpression'=>'array("data-id"=>$data->id)',
	'template'=>'{items}',
	'columns'=>array(
		array(
			'name'=>'type',
			'value'=>'str_replace("...", $data->typeArg, $data->typeName)',
			'filter'=>$questionModel->types(),
		),
		'title',
		'text',
		array(
			'name'=>'status',
			'value'=>'$data->statusName',
			'filter'=>$questionModel->statuses(),
		),
		array(
			'class'=>'CButtonColumn',
			'viewButtonUrl'=>'Yii::app()->urlManager->createUrl("question/view", array("id"=>$data->id))',
			'updateButtonUrl'=>'Yii::app()->urlManager->createUrl("question/update", array("id"=>$data->id))',
			'deleteButtonUrl'=>'Yii::app()->urlManager->createUrl("question/delete", array("id"=>$data->id))',
		),
	),
)); ?>
