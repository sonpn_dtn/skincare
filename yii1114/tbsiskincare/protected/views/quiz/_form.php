<?php
/* @var $this QuizController */
/* @var $model Quiz */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'quiz-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo _('Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'regime'); ?>
		<?php echo $form->dropDownList($model,'regime',array(null=>_('Please select')) + $model->regimes()); ?>
		<?php echo $form->error($model,'regime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gender'); ?>
		<?php echo $form->dropDownList($model,'gender',array(null=>_('Please select')) + $model->genders()); ?>
		<?php echo $form->error($model,'gender'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',$model->statuses()); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'siteCatalyst_subCategoryKey'); ?>
		<?php echo $form->textField($model,'siteCatalyst_subCategoryKey',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'siteCatalyst_subCategoryKey'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'siteCatalyst_subCategoryValue'); ?>
		<?php echo $form->textField($model,'siteCatalyst_subCategoryValue',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'siteCatalyst_subCategoryValue'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? _('Create') : _('Save')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
