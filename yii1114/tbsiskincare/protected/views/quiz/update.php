<?php
/* @var $this QuizController */
/* @var $model Quiz */

$this->breadcrumbs=array(
	_('Quizzes')=>array('admin'),
	$model->regimeName.' | '.$model->genderName=>array('view','id'=>$model->id),
	_('Update'),
);

$this->menu=array(
	array('label'=>_('Manage Quizzes'), 'url'=>array('admin')),
);
?>

<h1><?php echo _('Update Quiz'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>