<?php
/* @var $this ProductanswerController */
/* @var $model Product */

$this->breadcrumbs=array(
	_('Quizzes')=>array('quiz/admin'),
	$model->answer->question->quiz->regimeName.' | '.$model->answer->question->quiz->genderName=>array('quiz/view','id'=>$model->answer->question->quiz->id),
	$model->answer->question->title=>array('question/view','id'=>$model->answer->question->id),
	$model->answer->text=>array('answer/view','id'=>$model->answer->id),
	_('Update Related Product'),
);

$this->menu=array(
	array('label'=>_('Back to Answer'), 'url'=>array('answer/view','id'=>$model->answer->id)),
);
?>

<h1><?php echo _('Update Related Product'); ?></h1>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'product-answer-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo _('Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->label($model,'productId'); ?>
		<input type="text" readonly="readonly" value="<?php echo $model->product->name; ?>" size="60" />
		<?php echo $form->error($model,'productId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'weight'); ?>
		<?php echo $form->textField($model,'weight'); ?>
		<?php echo $form->error($model,'weight'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? _('Create') : _('Save')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->