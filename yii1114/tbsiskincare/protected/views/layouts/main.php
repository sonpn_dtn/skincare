<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/blueprint/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/blueprint/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/blueprint/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/blueprint/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/blueprint/form.css" />

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/redmond/jquery-ui-1.10.4.custom.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/blueprint/custom.css" />

	<link rel="SHORTCUT ICON" href="<?php echo Yii::app()->baseUrl; ?>/favicon.ico"/>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body id="<?php echo $this->bodyId; ?>">

<div class="container" id="page">

	<div id="header" class="clear">
		<div id="logo" class="left"><?php echo CHtml::encode(Yii::app()->name); ?></div>

		<div class="right" style="padding: 20px 20px 0 0;">
		<?php
		foreach($this->regions as $i => $region) {
			if($region->languageCode.'_'.$region->countryCode == Yii::app()->language)
				echo $region->name;
			else {
		?>
		<a href="<?php echo $this->getLanguageSwitchUrl($region->languageCode, $region->countryCode); ?>"><?php echo $region->name; ?></a>
		<?php
			}

			if($i + 1 != count($this->regions)) echo ' | ';
		}
		?>
		</div>
	</div><!-- header -->

	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>_('Products'), 'url'=>array('/product/admin'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>_('Quizzes'), 'url'=>array('/quiz/admin'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>_('Settings'), 'url'=>array('/settings'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>_('Regions'), 'url'=>array('/region/admin'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>_('Help'), 'url'=>array('help/page','view'=>'index'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>_('Analytics'), 'url'=>array('analytic/index'), 'visible'=>!Yii::app()->user->isGuest),

				array('label'=>_('Login'), 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>_('Logout'), 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
	</div><!-- mainmenu -->

	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'homeLink'=>_('Online Skincare Diagnostic Tool'),
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php //echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Powered by <a href="http://smackagency.com" target="_blank">SMACK</a> &copy; <?php echo date('Y'); ?>
	</div><!-- footer -->

</div><!-- page -->

<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/blueprint/jquery-ui-1.10.4.custom.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/blueprint/script.js"></script>
<script type="text/javascript">var BASE_URL='<?php echo Yii::app()->baseUrl; ?>';</script>

</body>
</html>
