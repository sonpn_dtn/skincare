<?php
/* @var $this AnswerController */
/* @var $model Answer */

$this->breadcrumbs=array(
	_('Quizzes')=>array('quiz/admin'),
	$model->question->quiz->regimeName.' | '.$model->question->quiz->genderName=>array('quiz/view','id'=>$model->question->quiz->id),
	$model->question->title=>array('question/view','id'=>$model->question->id),
	$model->text,
);

$this->menu=array(
	array('label'=>_('Update Answer'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>_('Delete Answer'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('zii','Are you sure you want to delete this item?'))),
	array('label'=>'<hr style="margin: 0;" />'),
	array('label'=>_('Back to Question'), 'url'=>array('question/view','id'=>$model->question->id)),
);
?>

<h1><?php echo _('View Answer'); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'text',
		array(
			'name'=>'unique',
			'value'=>$model->unique ? _('Yes') : _('No'),
		),
		'description',
		array(
			'name'=>'goToQuestionId',
			'value'=>$model->goToQuestion ? $model->goToQuestion->title : null,
		),
		array(
			'name'=>'skipToEnd',
			'value'=>$model->skipToEnd ? _('Yes') : _('No'),
		),
		'summaryText',
		'statusName',
	),
)); ?>

<h2 style="margin: 1em 0 0;"><?php echo _('Related Products'); ?></h2>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'product-grid',
	'dataProvider'=>new CArrayDataProvider($model->productAnswers, array('keyField'=>'answerId','pagination'=>array('pageSize'=>999))),
	'template'=>'{items}',
	'columns'=>array(
		array(
			'header'=>_('Code'),
			'value'=>array($this,'renderProductCode'),
		),
		array(
			'header'=>_('Name'),
			'value'=>array($this,'renderProductName'),
		),
		array(
			'header'=>_('Weight'),
			'name'=>'weight'
		),
		array(
			'class'=>'CButtonColumn',
			'viewButtonUrl'=>'Yii::app()->urlManager->createUrl("product/view", array("id"=>$data->productId))',
			'updateButtonUrl'=>'Yii::app()->urlManager->createUrl("productanswer/update",array("productId"=>$data->productId,"answerId"=>$data->answerId))',
			'deleteButtonUrl'=>'Yii::app()->urlManager->createUrl("productanswer/delete",array("productId"=>$data->productId,"answerId"=>$data->answerId))',
		),
	),
)); ?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm'); ?>

	<h3><?php _('Add a Related Product'); ?></h3>

	<p class="note"><?php echo _('Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo $form->errorSummary($productAnswer); ?>

	<div class="row">
		<?php echo $form->labelEx($productAnswer,'productId'); ?>
		<input type="text" name="term" id="product-search" data-rel="ProductAnswer_productId" placeholder="<?php echo _('Search products (type product code or name)'); ?>" size="60" maxlength="255" value="<?php if($productAnswer->productId) echo Yii::app()->request->getParam('term'); ?>" />
		<?php echo $form->hiddenField($productAnswer,'productId'); ?>
		<?php echo $form->error($productAnswer,'productId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($productAnswer,'weight'); ?>
		<?php echo $form->textField($productAnswer,'weight'); ?>
		<?php echo $form->error($productAnswer,'weight'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton(_('Add')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
