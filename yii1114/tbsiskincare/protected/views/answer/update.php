<?php
/* @var $this AnswerController */
/* @var $model Answer */

$this->breadcrumbs=array(
	_('Quizzes')=>array('quiz/admin'),
	$model->question->quiz->regimeName.' | '.$model->question->quiz->genderName=>array('quiz/view','id'=>$model->question->quiz->id),
	$model->question->title=>array('question/view','id'=>$model->question->id),
	_('Update'),
);

$this->menu=array(
	array('label'=>_('Back to Question'), 'url'=>array('question/view','id'=>$model->question->id)),
);
?>

<h1><?php echo _('Update Answer'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'questionModel'=>$model->question)); ?>