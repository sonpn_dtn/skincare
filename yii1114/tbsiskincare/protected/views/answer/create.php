<?php
/* @var $this AnswerController */
/* @var $model Answer */

$this->breadcrumbs=array(
	_('Quizzes')=>array('quiz/admin'),
	$questionModel->quiz->regimeName.' | '.$questionModel->quiz->genderName=>array('quiz/view','id'=>$questionModel->quiz->id),
	$questionModel->title=>array('question/view','id'=>$questionModel->id),
	_('Add Answer'),
);

$this->menu=array(
	array('label'=>_('Back to Question'), 'url'=>array('question/view','id'=>$questionModel->quiz->id)),
);
?>

<h1><?php echo _('Add Answer'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'questionModel'=>$questionModel)); ?>