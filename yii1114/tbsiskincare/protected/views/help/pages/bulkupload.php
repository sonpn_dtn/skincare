<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Bulk Upload')
);

?>

<h1><?php echo _('Bulk Upload'); ?></h1>

<p><?php echo _('When inputting product data in to the CMS for the very first time, an export can be taken from the e-commerce platform for import in to the skin diagnostic tool. This can be done via the upload of 2 CSV files, one for products and one for product variants. The column structure of the CSV files must include the columns listed below, exactly as they are shown.'); ?></p>
<p><strong><?php echo _('Product CSV'); ?></strong></p>
<ul>
	<li>Product_Code</li>
	<li>Product_Online</li>
	<li>Product_Name</li>
	<li>PRODUCT_RANGE</li>
	<li>PRODUCT_GENDER</li>
	<li>Day Night</li>
	<li>P_DETAILS</li>
	<li>P_DETAIL_BULLETS</li>
	<li>Product tag</li>
</ul>

<p><strong><?php echo _('Product Reference CSV'); ?></strong></p>
<ul>
	<li>Product_Code</li>
	<li>Reference_Code</li>
	<li>Reference_Online</li>
	<li>Reference_Name</li>
	<li>Reference_Desc</li>
	<li>EAN_NUMBER</li>
</ul>