<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Product Ranges')=>array('help/page','view'=>'productranges'),
	_('Adding Product Ranges')
);

?>

<h1><?php echo _('Adding Product Ranges'); ?></h1>

<p><?php echo _('To add a new product range, click the "Products" link in the main menu at the top of the screen, then click the "Manage Product Ranges" then "Create Product Range" links in the "Operations" menu to the right of the screen.'); ?></p>
<p><?php echo _('Alternatively, follow the link below.'); ?></p>
<p><a href="<?php echo $this->createUrl('productrange/create'); ?>"><?php echo $this->createAbsoluteUrl('productrange/create'); ?></a></p>

<br />

<ul>
	<li><?php echo _('Complete the form to add a new product range.'); ?></li>
	<li><?php echo _('The "Name" will be displayed on the results page.'); ?></li>
	<li><?php echo _('Enter the colour code as a hexadecimal value. The name will be displayed in this colour.'); ?></li>
</ul>