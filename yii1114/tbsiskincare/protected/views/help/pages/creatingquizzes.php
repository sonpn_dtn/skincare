<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Managing Quizzes')=>array('help/page','view'=>'managingquizzes'),
	_('Adding Quizzes'),
);

?>

<h1><?php echo _('Adding Quizzes'); ?></h1>

<p><?php echo _('To add a new quiz, click the "Quizzes" link in the main menu at the top of the screen, then click the "Create Quiz" link in the "Operations" menu to the right of the screen.'); ?></p>
<p><?php echo _('Alternatively, follow the link below.'); ?></p>
<p><a href="<?php echo $this->createUrl('quiz/create'); ?>"><?php echo $this->createAbsoluteUrl('quiz/create'); ?></a></p>

<br />

<ul>
	<li><?php echo _('Complete the form to add a new quiz.'); ?></li>
	<li><?php echo _('It is important that there are only 4 active quizzes at any one time: Female Express, Female Complete, Male Express and Male Complete'); ?></li>
	<li><?php echo _('You can set a quiz "Status" to "Active" or "Inactive".'); ?></li>
</ul>