<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Related Products')=>array('help/page','view'=>'relatedproducts'),
	_('Deleting Related Products')
);

?>

<h1><?php echo _('Deleting Related Products'); ?></h1>

<p><?php echo sprintf(_('To view related products, first find a related product list (see <a href="%s">Related Products</a>).'), $this->createUrl('help/page',array('view'=>'relatedproducts'))); ?></p>
<p><?php echo sprintf(_('Then click the %s icon next to the related product you want to delete.'), '<img src="'.Yii::app()->baseUrl.'/images/blueprint/delete.png" alt="" />'); ?></p>

<ul>
	<li><?php echo _('You will be prompted to confirm your actions.'); ?></li>
	<li><?php echo _('Deleting is irreversible.'); ?></li>
</ul>