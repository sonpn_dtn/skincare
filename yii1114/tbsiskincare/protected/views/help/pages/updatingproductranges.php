<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Product Ranges')=>array('help/page','view'=>'productranges'),
	_('Updating Product Ranges')
);

?>

<h1><?php echo _('Updating Product Ranges'); ?></h1>

<p><?php echo sprintf(_('To update an existing product range, click the "Products" link in the main menu at the top of the screen, then click the "Manage Product Ranges". Find the product range you wish to update and then click the %s icon.'), '<img src="'.Yii::app()->baseUrl.'/images/blueprint/update.png" alt="" />'); ?></a></p>

<br />

<ul>
	<li><?php echo _('Complete the form to update a product range.'); ?></li>
	<li><?php echo _('The "Name" will be displayed on the results page.'); ?></li>
	<li><?php echo _('Enter the colour code as a hexadecimal value. The name will be displayed in this colour.'); ?></li>
</ul>