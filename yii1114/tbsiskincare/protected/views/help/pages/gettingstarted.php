<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Getting Started')
);

?>

<h1><?php echo _('Getting Started'); ?></h1>

<p><?php echo _('This help section provides information on every screen within the CMS. If you are ever stuck on a specific screen, or you need information on how to perform an action, you should be able to find a relevant article from the "Operations" menu in this help section.'); ?></p>

<p><?php echo _('This Getting Started guide aims to direct you to the key articles within the help section to cover the basic familiarisation of the CMS.'); ?></p>

<ul>
	<li>
		<strong><?php echo _('For populating the CMS for the first time via a CSV export'); ?><br /></strong>
		<em><?php echo _('See'); ?></em> <a href="<?php echo $this->createUrl('help/page',array('view'=>'bulkupload')); ?>"><?php echo _('Bulk Upload'); ?></a>
	</li>
	<li>
		<strong><?php echo _('For information on managing products and product variants'); ?><br /></strong>
		<em><?php echo _('See'); ?></em> <a href="<?php echo $this->createUrl('help/page',array('view'=>'managingproducts')); ?>"><?php echo _('Managing Products'); ?></a>
	</li>
	<li>
		<strong><?php echo _('For information on managing quizzes, questions and answers'); ?><br /></strong>
		<em><?php echo _('See'); ?></em> <a href="<?php echo $this->createUrl('help/page',array('view'=>'managingquizzes')); ?>"><?php echo _('Managing Quizzes'); ?></a>
	</li>
	<li>
		<strong><?php echo _('To better understand how the tool works to select relevant products'); ?><br /></strong>
		<em><?php echo _('See'); ?></em> <a href="<?php echo $this->createUrl('help/page',array('view'=>'relatedproducts')); ?>"><?php echo _('Related Products'); ?></a>
	</li>
	<li>
		<strong><?php echo _('For information on localisation'); ?><br /></strong>
		<em><?php echo _('See'); ?></em> <a href="<?php echo $this->createUrl('help/page',array('view'=>'localisation')); ?>"><?php echo _('Localisation'); ?></a>
	</li>
</ul>