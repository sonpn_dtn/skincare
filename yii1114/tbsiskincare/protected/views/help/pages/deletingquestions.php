<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Questions')=>array('help/page','view'=>'questions'),
	_('Deleting Questions'),
);

?>

<h1><?php echo _('Deleting Questions'); ?></h1>

<p><?php echo sprintf(_('To delete a question, first find a question list (see <a href="%s">Questions</a>).'), $this->createUrl('help/page',array('view'=>'questions'))); ?></p>
<p><?php echo sprintf(_('Then click the %s icon next to the question you want to delete.'), '<img src="'.Yii::app()->baseUrl.'/images/blueprint/delete.png" alt="" />'); ?></p>

<br />

<ul>
	<li><?php echo _('You will be prompted to confirm your actions.'); ?></li>
	<li><?php echo _('Deleting is irreversible.'); ?></li>
	<li><?php echo _('The question (including all answers and related product weights) will be deleted.'); ?></li>
</ul>