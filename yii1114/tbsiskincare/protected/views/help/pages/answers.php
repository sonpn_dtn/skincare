<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Answers')
);

?>

<h1><?php echo _('Answers'); ?></h1>

<p><?php echo sprintf(_('To view answers, first find a question list (see <a href="%s">Questions</a>).'), $this->createUrl('help/page',array('view'=>'questions'))); ?></p>
<p><?php echo sprintf(_('Then click the %s icon next to the question you want view answers to.'), '<img src="'.Yii::app()->baseUrl.'/images/blueprint/view.png" alt="" />'); ?></p>

<p><?php echo _('Answers can be reordered by dragging rows in the grid up and down. Answers will be displayed in the order they are listed.'); ?></p>