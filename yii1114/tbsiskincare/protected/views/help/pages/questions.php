<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Questions')
);

?>

<h1><?php echo _('Questions'); ?></h1>

<p><?php echo sprintf(_('To view questions, find a quiz by clicking the "Quizzes" link in the main menu at the top of the screen and clicking the %s icon next to a quiz.'), '<img src="'.Yii::app()->baseUrl.'/images/blueprint/view.png" alt="" />'); ?></p>

<p><?php echo _('Questions can be reordered by dragging rows in the grid up and down. Questions will be asked in the order they are listed.'); ?></p>