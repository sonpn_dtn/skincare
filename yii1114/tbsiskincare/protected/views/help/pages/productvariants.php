<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Product Variants')
);

?>

<h1><?php echo _('Product Variants'); ?></h1>

<p><?php echo _('Product variants show on the results screen, and allow users to choose different variations of a product to add to their basket. Each product must have at least 1 active variant in stock to be shown as an option on the results page.'); ?></p>
<p><?php echo sprintf(_('To view product variants, first search for a product. For more information, see <a href="%s">Searching Products</a>.'), $this->createUrl('help/page',array('view'=>'searchingproducts'))); ?></p>