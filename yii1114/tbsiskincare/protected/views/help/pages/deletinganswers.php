<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Answers')=>array('help/page','view'=>'answers'),
	_('Deleting Answers')
);

?>

<h1><?php echo _('Deleting Answers'); ?></h1>

<p><?php echo sprintf(_('To delete an answer, first find an answer list (see <a href="%s">Answers</a>).'), $this->createUrl('help/page',array('view'=>'answers'))); ?></p>
<p><?php echo sprintf(_('Then click the %s icon next to the answer you want to delete.'), '<img src="'.Yii::app()->baseUrl.'/images/blueprint/delete.png" alt="" />'); ?></p>

<br />

<ul>
	<li><?php echo _('You will be prompted to confirm your actions.'); ?></li>
	<li><?php echo _('Deleting is irreversible.'); ?></li>
	<li><?php echo _('The answer (including all related products weights) will be deleted.'); ?></li>
</ul>