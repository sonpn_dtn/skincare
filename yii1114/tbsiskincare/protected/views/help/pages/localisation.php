<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Localisation')
);

?>

<h1><?php echo _('Localisation'); ?></h1>

<p><?php echo _('All information in the CMS will be displayed as it is entered. Primarily, this all "dynamically" generated question, answer and product information. Comprehensively, this includes:'); ?></p>

<ul>
	<li><?php echo _('Question titles, text and breadcrumbs'); ?></li>
	<li><?php echo _('"Did You Know?" text'); ?></li>
	<li><?php echo _('Answer text and descriptions'); ?></li>
	<li><?php echo _('Summary text'); ?></li>
	<li><?php echo _('Product titles, details and detail bullets'); ?></li>
	<li><?php echo _('Product variant names'); ?></li>
	<li><em><?php echo _('Prices (local currency is automatically applied)'); ?></em></li>
</ul>

<p><?php echo _('All other information is considered "static" copy, and can be managed via the Loco tool.'); ?></p>
<p><a target="_blank" href="https://localise.biz/dashboard/project/translate/1876">https://localise.biz/dashboard/project/translate/1876</a></p>

<p><?php echo _('When you update copy in Loco, you must tell the skincare tool that the copy has changed. To do this click the "Quizzes" link in the main menu at the top of the screen, then click the "Update Localisations" link in the "Operations" menu to the right of the screen.'); ?></p>

<br />

<p><?php echo _('A full guide to using Loco can be found a the link below.'); ?></p>
<p><a target="_blank" href="http://help.localise.biz/">http://help.localise.biz/</a></p>