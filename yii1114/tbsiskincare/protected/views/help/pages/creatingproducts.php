<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Managing Products')=>array('help/page','view'=>'managingproducts'),
	_('Adding Products')
);

?>

<h1><?php echo _('Adding Products'); ?></h1>

<p><?php echo _('To add a new product, click the "Products" link in the main menu at the top of the screen, then click the "Create Product" link in the "Operations" menu to the right of the screen.'); ?></p>
<p><?php echo _('Alternatively, follow the link below.'); ?></p>
<p><a href="<?php echo $this->createUrl('product/create'); ?>"><?php echo $this->createAbsoluteUrl('product/create'); ?></a></p>

<br />

<ul>
	<li><?php echo _('Complete the form to add a new product.'); ?></li>
	<li><?php echo sprintf(_('The "Product Range" will be shown on the results page to help visually separate products. For more information, see <a href="%s">Product Ranges</a>.'), $this->createUrl('help/page',array('view'=>'productranges'))); ?></li>
	<li><?php echo sprintf(_('The "Product Tag" is used to create a regime on the results page (i.e. cleanse, tone, moisturise). For more information, see <a href="%s">Product Tags</a>.'), $this->createUrl('help/page',array('view'=>'producttags'))); ?></li>
	<li><?php echo _('It is important you enter the "Product Code" accurately as this is used to retrieve image, stock and price information.'); ?></li>
	<li><?php echo _('Product "Name", "Best Seller", "Details" and "Detail Bullets" will be displayed on the results page. Enter a pipe "|" to separate each bullet point.'); ?></li>
	<li><?php echo _('You must enter a "Gender" and a "Day/Night" value. This will help the tool correctly allocate the products it recommends.'); ?></li>
	<li><?php echo _('You can set a product "Status" to "Active" or "Inactive". Inactive products (including all product variants) will not show up on the results page.'); ?></li>
</ul>