<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Product Variants')=>array('help/page','view'=>'productvariants'),
	_('Deleting Product Variants')
);

?>

<h1><?php echo _('Deleting Product Variants'); ?></h1>

<p><?php echo sprintf(_('To delete an existing product variant from a product screen, find the product you wish to delete and then click the %s icon.'), '<img src="'.Yii::app()->baseUrl.'/images/blueprint/delete.png" alt="" />'); ?></a></p>

<br />

<ul>
	<li><?php echo _('You will be prompted to confirm your actions.'); ?></li>
	<li><?php echo _('Deleting is irreversible.'); ?></li>
</ul>