<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Managing Quizzes')
);

?>

<h1><?php echo _('Managing Quizzes'); ?></h1>

<p><?php echo _('Quizzes form the backbone of the skincare diagnostic tool.'); ?></p>

<p>
	<?php echo _('When a user uses the skincare diagnostic tool, they give answers to questions in order to complete the quiz.'); ?><br />
	<?php echo _('Each answer that they give allows the tool to select relevant products, depending on their choices.'); ?><br />
	<?php echo _('When a user completes a quiz, the tool can calculate the most relevant products using the "weighting" system outlined below.'); ?>
</p>

<ul>
	<li><?php echo _('For every quiz, there are questions.'); ?></li>
	<li><?php echo _('For every question, there are answers.'); ?></li>
	<li>
		<?php echo _('For every answer, there are related products.'); ?><br />
		<em><?php echo _('These products are weighted to give certain products a greater relevance depending on the answer given.'); ?></em>
	</li>
</ul>

<p><?php echo _('To manage quizzes, click the "Quizzes" link in the main menu at the top of the screen.'); ?></p>
<p><?php echo _('Alternatively, follow the link below.'); ?></p>
<p><a href="<?php echo $this->createUrl('quiz/admin'); ?>"><?php echo $this->createAbsoluteUrl('quiz/admin'); ?></a></p>

<br />

<p><?php echo _('At all times there must be 4 active quizzes: Female Express, Female Complete, Male Express and Male Complete. These form the options for when the user first starts the quiz.'); ?></p>

<br />

<p><?php echo _('Links to the most important sections when managing quizzes are listed below.'); ?></p>

<ul>
	<li><a href="<?php echo $this->createUrl('help/page',array('view'=>'creatingquizzes')); ?>"><?php echo _('Adding Quizzes'); ?></a></li>
	<li><a href="<?php echo $this->createUrl('help/page',array('view'=>'creatingquestions')); ?>"><?php echo _('Adding Questions'); ?></a></li>
	<li><a href="<?php echo $this->createUrl('help/page',array('view'=>'creatinganswers')); ?>"><?php echo _('Adding Answers'); ?></a></li>
	<li><a href="<?php echo $this->createUrl('help/page',array('view'=>'creatingrelatedproducts')); ?>"><?php echo _('Adding Related Products'); ?></a></li>
</ul>