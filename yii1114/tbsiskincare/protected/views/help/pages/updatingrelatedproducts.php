<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Related Products')=>array('help/page','view'=>'relatedproducts'),
	_('Updating Related Products')
);

?>

<h1><?php echo _('Updating Related Products'); ?></h1>

<p><?php echo sprintf(_('To view related products, first find a related product list (see <a href="%s">Related Products</a>).'), $this->createUrl('help/page',array('view'=>'relatedproducts'))); ?></p>
<p><?php echo sprintf(_('Then click the %s icon next to the related product you want to update.'), '<img src="'.Yii::app()->baseUrl.'/images/blueprint/update.png" alt="" />'); ?></p>

<p><?php echo _('From this screen you can change the "Weight" of a related product and "Save" your updates.'); ?></p>