<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Product Variants')=>array('help/page','view'=>'productvariants'),
	_('Updating Product Variants')
);

?>

<h1><?php echo _('Updating Product Variants'); ?></h1>

<p><?php echo sprintf(_('To update an existing product variant from a product screen, find the variant you wish to update and then click the %s icon.'), '<img src="'.Yii::app()->baseUrl.'/images/blueprint/update.png" alt="" />'); ?></a></p>

<br />

<ul>
	<li><?php echo _('Complete the form to update a product variant.'); ?></li>
	<li><?php echo _('It is important you enter the "Code" accurately as this is used to retrieve stock and price information.'); ?></li>
	<li><?php echo _('The "Name" will be displayed alongside the price on the results screen so the user knows what they are purchasing.'); ?></li>
	<p><?php echo sprintf(_('Price and stock information will be retrieved automatically. For more information, see <a href="%s">Scraping Price & Stock</a>.'), $this->createUrl('help/page',array('view'=>'scrapepricestock'))); ?></p>
	<li><?php echo _('You can set a product variant "Status" to "Active" or "Inactive". Inactive product variants will not show up on the results page.'); ?></li>
</ul>