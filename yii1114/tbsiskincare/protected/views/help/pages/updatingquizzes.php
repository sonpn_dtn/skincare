<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Managing Quizzes')=>array('help/page','view'=>'managingquizzes'),
	_('Updating Quizzes'),
);

?>

<h1><?php echo _('Updating Quizzes'); ?></h1>

<p><?php echo sprintf(_('To update an existing quiz, click the "Quizzes" link in the main menu at the top of the screen. Find the quiz you wish to update and then click the %s icon.'), '<img src="'.Yii::app()->baseUrl.'/images/blueprint/update.png" alt="" />'); ?></a></p>

<br />

<ul>
	<li><?php echo _('Complete the form to add a new quiz.'); ?></li>
	<li><?php echo _('It is important that there are only 4 active quizzes at any one time: Female Express, Female Complete, Male Express and Male Complete'); ?></li>
	<li><?php echo _('You can set a quiz "Status" to "Active" or "Inactive".'); ?></li>
</ul>