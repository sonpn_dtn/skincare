<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Product Tags')
);

?>

<h1><?php echo _('Product Tags'); ?></h1>

<p><?php echo _('To manage product tags, click the "Products" link in the main menu at the top of the screen, then click the "Manage Product Tags" link in the "Operations" menu to the right of the screen.'); ?></p>
<p><?php echo _('Alternatively, follow the link below.'); ?></p>
<p><a href="<?php echo $this->createUrl('tag/admin'); ?>"><?php echo $this->createAbsoluteUrl('tag/admin'); ?></a></p>

<br />

<ul>
	<li><?php echo _('At all times, there must be Cleanse, Tone and Moisturise tags. These are special tags which form the basis to build a regime on the results page.'); ?></li>
	<li><?php echo _('Product tags are managed separately to products. A new product tag must be created before creating the products that go in it. Updating an existing product tag will affect all associated products.'); ?></li>
	<li><?php echo _('Product tags cannot be deleted.'); ?></li>
</ul>