<?php
/* @var $this RegionController */
/* @var $model Region */

$this->breadcrumbs=array(
	_('Regions')=>array('admin'),
	$model->name=>array('view','id'=>$model->id),
	_('Update'),
);

$this->menu=array(
	array('label'=>_('Manage Regions'), 'url'=>array('admin')),
);
?>

<h1><?php echo _('Update Region'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>