# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.37-cll)
# Database: tbsiuk_skincare
# Generation Time: 2014-09-03 17:38:22 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table answer
# ------------------------------------------------------------

LOCK TABLES `answer` WRITE;
/*!40000 ALTER TABLE `answer` DISABLE KEYS */;

INSERT INTO `answer` (`id`, `questionId`, `goToQuestionId`, `skipToEnd`, `unique`, `text`, `summaryText`, `description`, `order`, `status`, `created`)
VALUES
	(1,1,NULL,NULL,NULL,'Under 20','aged <a>under 20</a>','',1,1,'2014-05-19 19:02:55'),
	(2,1,NULL,NULL,NULL,'20-29','aged between <a>20 and 29</a>','',2,1,'2014-05-19 19:03:08'),
	(3,1,NULL,NULL,NULL,'30-39','aged between <a>30 and 39</a>','',3,1,'2014-05-19 19:03:18'),
	(4,1,NULL,NULL,NULL,'40-49','aged between <a>40 and 49</a>','',4,1,'2014-05-19 19:03:25'),
	(5,1,NULL,NULL,NULL,'50+','aged <a>over 50</a>','',5,1,'2014-05-19 19:03:34'),
	(6,2,NULL,0,NULL,'Oily','','Your skin often looks shiny even after cleansing due to excess sebum production. You may notice that you have large pores. Keeping oil under control is your biggest challenge.',1,1,'2014-05-21 16:28:19'),
	(7,2,NULL,NULL,NULL,'Combination',NULL,'Your skin may be oily around your t-zone yet normal or even dry on other areas of your face. Keeping dry areas hydrated whilst tackling oily areas is your biggest challenge.',2,1,'2014-05-21 16:28:34'),
	(8,2,NULL,NULL,NULL,'Normal',NULL,'Your skin generally looks healthy and practically blemish free – you lucky thing!  Maintaining your complexion is your biggest challenge.',3,1,'2014-05-21 16:28:56'),
	(9,2,NULL,NULL,NULL,'Dry',NULL,'Your skin usually looks healthy; it does however feel dry at times. Keeping your skin hydrated is your biggest challenge.',4,1,'2014-05-21 16:29:12'),
	(10,2,NULL,NULL,NULL,'Very dry',NULL,'Your skin often feels dehydrated, making it feel tight and occasionally flaky. Keeping your parched skin hydrated is your biggest challenge.',5,1,'2014-05-21 16:29:26'),
	(11,3,4,NULL,NULL,'Blemishes',NULL,'',1,1,'2014-05-23 08:32:57'),
	(12,3,8,NULL,NULL,'Uneven skin tone',NULL,'',2,2,'2014-05-23 08:33:40'),
	(15,3,8,NULL,NULL,'Dull skin',NULL,'',3,1,'2014-05-23 08:35:44'),
	(16,3,5,NULL,NULL,'Hydration',NULL,'',4,1,'2014-05-23 08:36:09'),
	(17,3,6,NULL,NULL,'Ageing',NULL,'',5,1,'2014-05-23 08:36:28'),
	(18,3,7,NULL,NULL,'Sensitivity',NULL,'',6,1,'2014-05-23 08:36:40'),
	(19,4,8,NULL,NULL,'Just occasionally',NULL,'',NULL,1,'2014-05-23 08:38:48'),
	(20,4,8,NULL,NULL,'Fairly often',NULL,'',NULL,1,'2014-05-23 08:39:05'),
	(21,4,8,NULL,NULL,'All the time',NULL,'',NULL,1,'2014-05-23 08:39:15'),
	(22,5,8,NULL,NULL,'Slightly dry',NULL,'',NULL,1,'2014-05-23 08:39:54'),
	(23,5,8,NULL,NULL,'Quite dry',NULL,'',NULL,1,'2014-05-23 08:40:07'),
	(24,5,8,NULL,NULL,'Very dry',NULL,'',NULL,1,'2014-05-23 08:40:17'),
	(25,6,8,NULL,NULL,'Ageing isn\'t a concern',NULL,'If you’re blessed with a baby-faced complexion your skin will be radiant, firm and line-free. Treasure it!',NULL,1,'2014-05-23 08:40:46'),
	(26,6,8,NULL,NULL,'I\'m thinking ahead',NULL,'Your skin has a youthful radiance that you’d like to maintain, you’d like to prevent rather than cure.',NULL,1,'2014-05-23 08:41:53'),
	(27,6,8,NULL,NULL,'First signs of ageing',NULL,'As time passes you’re starting to see fine lines appearing and your skin is beginning to lack radiance. You’d like to slow time down a little bit.',NULL,1,'2014-05-23 08:42:12'),
	(28,6,8,NULL,NULL,'Advanced signs of ageing',NULL,'Your years of wisdom are beginning to show with some deep lines and loss of skin firmness. You’d like to give your skin a youthful glow.',NULL,1,'2014-05-23 08:42:28'),
	(29,7,8,NULL,NULL,'I\'m not sure',NULL,'A quick test to see if you might have sensitive skin is to brush two fingers across your cheek. If your cheek flushes, feels hot or feels dry then you may have sensitive skin.',NULL,1,'2014-05-23 08:43:11'),
	(30,7,8,NULL,NULL,'Not at all',NULL,'Your skin rarely gets irritated and generally feels comfortable.',NULL,1,'2014-05-23 08:43:28'),
	(31,7,8,NULL,NULL,'Quite sensitive',NULL,'Your skin occasionally gets irritated or has a reaction to things like air conditioning or certain products.',NULL,1,'2014-05-23 08:43:42'),
	(32,7,8,NULL,NULL,'Very sensitive',NULL,'Your skin is frequently irritated, feels tingly. It often has a strong reaction to anything and everything from sunshine, fragrance, pollution – even stress.',NULL,1,'2014-05-23 08:43:53'),
	(33,8,NULL,NULL,NULL,'Fine lines',NULL,'',NULL,1,'2014-05-23 08:44:11'),
	(34,8,NULL,0,NULL,'Deep lines','','',NULL,1,'2014-05-23 08:44:22'),
	(35,8,NULL,NULL,NULL,'Dark circles',NULL,'',NULL,1,'2014-05-23 08:44:30'),
	(36,8,NULL,NULL,NULL,'Puffiness',NULL,'',NULL,1,'2014-05-23 08:44:38'),
	(37,9,NULL,1,NULL,'I am/was a regular smoker','are/were a regular smoker','If you smoke or you used to on a regular basis you already know there can be side effects. In terms of your skin you may see signs of premature ageing and a lack of radiance.',1,1,'2014-05-28 10:39:57'),
	(38,9,NULL,1,NULL,'I don’t drink enough water','don\'t drink enough water','You lose water constantly throughout the day so it’s important to stay hydrated to keep skin cells healthy. If your skin is dehydrated it may feel dry or lack radiance.',2,1,'2014-05-28 10:41:24'),
	(39,9,NULL,1,NULL,'I’m often exposed to the sun','are often exposed to the sun','Whilst exposure to the sun is important for vitamin D production, the sun’s UV rays can accelerate your skin’s natural ageing process. If your skin is often in the sun you may begin to see fine lines and wrinkles appearing. Or even dark spots and hyperpigmentation.',3,1,'2014-05-28 10:46:15'),
	(40,9,10,NULL,NULL,'I wear make-up regularly','wear make-up regularly','Wearing make-up feels great but if you if it’s not removed properly it can clog your pores. If you wear make-up frequently you may find you experience blemishes.',4,1,'2014-05-28 10:49:03'),
	(41,9,NULL,1,NULL,'I’m exposed to heating/air-conditioning','are exposed to heating/air-conditioning','Air conditioning or heating can have a drying effect on skin. You may find that your skin feels dry and experience flaky patches.',5,1,'2014-05-28 10:50:46'),
	(42,9,NULL,1,NULL,'I rarely get enough sleep','rarely get enough sleep','Sleep is your body’s chance to reboot and restore. If you don’t get enough sleep you may find your skin is dull and experience puffy skin or dark circles around your eyes.',6,1,'2014-05-28 10:51:11'),
	(43,9,NULL,1,NULL,'I live in the city','live in the city','The long working hours and pollution that comes with living in the city can play havoc with your skin. If you live or work in a city you may notice fine lines appearing, blemishes or experience sensitivity on your skin.',7,1,'2014-05-28 10:52:18'),
	(44,10,NULL,0,NULL,'Waterproof eye make-up','','',1,1,'2014-05-28 10:52:43'),
	(45,9,NULL,1,NULL,'I often feel stressed','often feel stressed','Stress has an ageing effect on your skin. So if you have a busy lifestyle or often feel under pressure you may notice fine lines beginning to appear.',8,1,'2014-05-28 10:52:45'),
	(46,10,NULL,0,NULL,'Eye make-up','','',2,1,'2014-05-28 10:52:59'),
	(47,10,NULL,0,NULL,'Lip colour','','',3,1,'2014-05-28 10:53:24'),
	(48,10,NULL,0,NULL,'Facial make up','','',4,1,'2014-05-28 10:53:36'),
	(50,11,NULL,0,NULL,'Under 20','aged <a>under 20</a>','',1,1,'2014-05-19 19:02:55'),
	(51,11,NULL,NULL,NULL,'20-29','aged between <a>20 and 29</a>','',2,1,'2014-05-19 19:03:08'),
	(52,11,NULL,NULL,NULL,'30-39','aged between <a>30 and 39</a>','',3,1,'2014-05-19 19:03:18'),
	(53,11,NULL,NULL,NULL,'40-49','aged between <a>40 and 49</a>','',4,1,'2014-05-19 19:03:25'),
	(54,11,NULL,NULL,NULL,'50+','aged <a>over 50</a>','',5,1,'2014-05-19 19:03:34'),
	(55,12,NULL,0,NULL,'Oily','','Your skin often looks shiny even after cleansing due to excess sebum production. You may notice that you have large pores. Keeping oil under control is your biggest challenge.',1,1,'2014-05-21 16:28:19'),
	(56,12,NULL,0,NULL,'Combination','','Your skin may be oily around your t-zone yet normal or even dry on other areas of your face. Keeping dry areas hydrated whilst tackling oily areas is your biggest challenge.',2,1,'2014-05-21 16:28:34'),
	(57,12,NULL,0,NULL,'Normal','','Your skin generally looks healthy and practically blemish free – you lucky thing!  Maintaining your complexion is your biggest challenge.',3,1,'2014-05-21 16:28:56'),
	(58,12,NULL,0,NULL,'Dry','','Your skin usually looks healthy; it does however feel dry at times. Keeping your skin hydrated is your biggest challenge.',4,1,'2014-05-21 16:29:12'),
	(59,12,NULL,0,NULL,'Very dry','','Your skin often feels dehydrated, making it feel tight and occasionally flaky. Keeping your parched skin hydrated is your biggest challenge.',5,1,'2014-05-21 16:29:26'),
	(60,13,14,0,NULL,'Blemishes','','',1,1,'2014-05-23 08:32:57'),
	(61,13,18,0,NULL,'Uneven skin tone',NULL,'',2,2,'2014-05-23 08:33:40'),
	(62,13,18,0,NULL,'Dull skin','','',3,1,'2014-05-23 08:35:44'),
	(63,13,15,0,NULL,'Hydration','','',4,1,'2014-05-23 08:36:09'),
	(64,13,16,NULL,NULL,'Ageing',NULL,'',5,1,'2014-05-23 08:36:28'),
	(65,13,17,NULL,NULL,'Sensitivity',NULL,'',6,1,'2014-05-23 08:36:40'),
	(66,14,18,0,NULL,'Just occasionally','','',NULL,1,'2014-05-23 08:38:48'),
	(67,14,18,0,NULL,'Fairly often','','',NULL,1,'2014-05-23 08:39:05'),
	(68,14,18,0,NULL,'All the time','','',NULL,1,'2014-05-23 08:39:15'),
	(69,15,18,0,NULL,'Slightly dry','','',NULL,1,'2014-05-23 08:39:54'),
	(70,15,18,0,NULL,'Quite dry','','',NULL,1,'2014-05-23 08:40:07'),
	(71,15,18,0,NULL,'Very dry','','',NULL,1,'2014-05-23 08:40:17'),
	(72,16,18,0,NULL,'Ageing isn\'t a concern','','If you’re blessed with a baby-faced complexion your skin will be radiant, firm and line-free. Treasure it!',NULL,1,'2014-05-23 08:40:46'),
	(73,16,18,NULL,NULL,'I\'m thinking ahead',NULL,'Your skin has a youthful radiance that you’d like to maintain, you’d like to prevent rather than cure.',NULL,1,'2014-05-23 08:41:53'),
	(74,16,18,0,NULL,'First signs of ageing','','As time passes you’re starting to see fine lines appearing and your skin is beginning to lack radiance. You’d like to slow time down a little bit.',NULL,1,'2014-05-23 08:42:12'),
	(75,16,18,NULL,NULL,'Advanced signs of ageing',NULL,'Your years of wisdom are beginning to show with some deep lines and loss of skin firmness. You’d like to give your skin a youthful glow.',NULL,1,'2014-05-23 08:42:28'),
	(76,17,18,NULL,NULL,'I\'m not sure',NULL,'A quick test to see if you might have sensitive skin is to brush two fingers across your cheek. If your cheek flushes, feels hot or feels dry then you may have sensitive skin.',NULL,1,'2014-05-23 08:43:11'),
	(77,17,18,NULL,NULL,'Not at all',NULL,'Your skin rarely gets irritated and generally feels comfortable.',NULL,1,'2014-05-23 08:43:28'),
	(78,17,18,NULL,NULL,'Quite sensitive',NULL,'Your skin occasionally gets irritated or has a reaction to things like air conditioning or certain products.',NULL,1,'2014-05-23 08:43:42'),
	(79,17,18,NULL,NULL,'Very sensitive',NULL,'Your skin is frequently irritated, feels tingly. It often has a strong reaction to anything and everything from sunshine, fragrance, pollution – even stress.',NULL,1,'2014-05-23 08:43:53'),
	(80,18,NULL,0,NULL,'Fine lines','','',NULL,1,'2014-05-23 08:44:11'),
	(81,18,NULL,NULL,NULL,'Deep lines',NULL,'',NULL,1,'2014-05-23 08:44:22'),
	(82,18,NULL,0,NULL,'Dark circles',NULL,'',NULL,1,'2014-05-23 08:44:30'),
	(83,18,NULL,0,NULL,'Puffiness','','',NULL,1,'2014-05-23 08:44:38'),
	(84,19,NULL,0,NULL,'I am/was a regular smoker','are/were a regular smoker','If you smoke or you used to on a regular basis you already know there can be side effects. In terms of your skin you may see signs of premature ageing and a lack of radiance.',1,1,'2014-05-28 10:39:57'),
	(85,19,NULL,0,NULL,'I don’t drink enough water','don\'t drink enough water','You lose water constantly throughout the day so it’s important to stay hydrated to keep skin cells healthy. If your skin is dehydrated it may feel dry or lack radiance.',2,1,'2014-05-28 10:41:24'),
	(86,19,NULL,0,NULL,'I’m often exposed to the sun','are often exposed to the sun','Whilst exposure to the sun is important for vitamin D production, the sun’s UV rays can accelerate your skin’s natural ageing process. If your skin is often in the sun you may begin to see fine lines and wrinkles appearing. Or even dark spots and hyperpigmentation.',3,1,'2014-05-28 10:46:15'),
	(87,19,NULL,0,NULL,'I’m exposed to heating/air-conditioning','are exposed to heating/air-conditioning','Air conditioning or heating can have a drying effect on skin. You may find that your skin feels dry and experience flaky patches.',5,1,'2014-05-28 10:50:46'),
	(88,19,NULL,NULL,NULL,'I rarely get enough sleep','rarely get enough sleep','Sleep is your body’s chance to reboot and restore. If you don’t get enough sleep you may find your skin is dull and experience puffy skin or dark circles around your eyes.',6,1,'2014-05-28 10:51:11'),
	(89,19,NULL,0,NULL,'I live in the city','live in the city','The long working hours and pollution that comes with living in the city can play havoc with your skin. If you live or work in a city you may notice fine lines appearing, blemishes or experience sensitivity on your skin.',7,1,'2014-05-28 10:52:18'),
	(90,19,NULL,0,NULL,'I often feel stressed','often feel stressed','Stress has an ageing effect on your skin. So if you have a busy lifestyle or often feel under pressure you may notice fine lines beginning to appear.',8,1,'2014-05-28 10:52:45'),
	(91,13,18,0,0,'Grooming','','',7,1,'2014-05-28 11:52:11'),
	(93,21,NULL,0,NULL,'Under 20','aged <a>under 20</a>','',1,1,'2014-05-30 10:18:14'),
	(94,21,NULL,0,NULL,'20-29','aged between <a>20 and 29</a>','',2,1,'2014-05-30 10:18:29'),
	(95,21,NULL,0,NULL,'30-39','aged between <a>30 and 39</a>','',3,1,'2014-05-30 10:18:42'),
	(96,21,NULL,0,NULL,'40-49','aged between <a>40 and 49</a>','',4,1,'2014-05-30 10:18:49'),
	(97,21,NULL,0,NULL,'50+','aged <a>over 50</a>','',5,1,'2014-05-30 10:19:01'),
	(98,22,NULL,0,NULL,'Oily','','Your skin often looks shiny even after cleansing due to excess sebum production. You may notice that you have large pores. Keeping oil under control is your biggest challenge.',1,1,'2014-05-30 10:20:44'),
	(99,22,NULL,0,NULL,'Combination','','Your skin may be oily around your t-zone yet normal or even dry on other areas of your face. Keeping dry areas hydrated whilst tackling oily areas is your biggest challenge.',2,1,'2014-05-30 10:21:11'),
	(100,22,NULL,0,NULL,'Normal','','Your skin generally looks healthy and practically blemish free – you lucky thing! Maintaining your complexion is your biggest challenge.',3,1,'2014-05-30 10:25:42'),
	(101,22,NULL,0,NULL,'Dry','','Your skin usually looks healthy; it does however feel dry at times. Keeping your skin hydrated is your biggest challenge.',4,1,'2014-05-30 10:26:00'),
	(102,22,NULL,0,NULL,'Very Dry','','Your skin often feels dehydrated, making it feel tight and occasionally flaky. Keeping your parched skin hydrated is your biggest challenge.',5,1,'2014-05-30 10:26:17'),
	(103,23,NULL,0,NULL,'Occasional blemishes','','',1,1,'2014-05-30 10:28:31'),
	(104,23,NULL,0,NULL,'Regular blemishes','','',2,1,'2014-05-30 10:28:41'),
	(105,23,NULL,0,NULL,'Uneven skin tone','','',3,2,'2014-05-30 10:29:05'),
	(106,23,NULL,0,NULL,'Dull skin','','',4,1,'2014-05-30 10:29:59'),
	(107,23,NULL,0,NULL,'Hydration','','',5,1,'2014-05-30 10:30:11'),
	(108,23,NULL,0,NULL,'Ageing','','',6,1,'2014-05-30 10:30:18'),
	(109,23,NULL,0,NULL,'Sensitivity','','',7,1,'2014-05-30 10:30:33'),
	(110,23,NULL,0,NULL,'Grooming','','',8,1,'2014-05-30 10:30:40'),
	(111,24,NULL,0,NULL,'Under 20','aged <a>under 20</a>','',1,1,'2014-05-30 10:38:22'),
	(112,24,NULL,0,NULL,'20-29','aged between <a>20 and 29</a>','',2,1,'2014-05-30 10:38:30'),
	(114,24,NULL,0,NULL,'30-39','aged between <a>30 and 39</a>','',3,1,'2014-05-30 10:38:47'),
	(115,24,NULL,0,NULL,'40-49','aged between <a>40 and 49</a>','',4,1,'2014-05-30 10:38:53'),
	(116,24,NULL,0,NULL,'50+','aged <a>over 50</a>','',5,1,'2014-05-30 10:38:59'),
	(117,25,NULL,0,NULL,'Oily','','Your skin often looks shiny even after cleansing due to excess sebum production. You may notice that you have large pores. Keeping oil under control is your biggest challenge.',1,1,'2014-05-30 10:40:13'),
	(118,25,NULL,0,NULL,'Combination','','Your skin may be oily around your t-zone yet normal or even dry on other areas of your face. Keeping dry areas hydrated whilst tackling oily areas is your biggest challenge.',2,1,'2014-05-30 10:40:32'),
	(119,25,NULL,0,NULL,'Normal','','Your skin generally looks healthy and practically blemish free – you lucky thing! Maintaining your complexion is your biggest challenge.',3,1,'2014-05-30 10:40:54'),
	(120,25,NULL,0,NULL,'Dry','','Your skin usually looks healthy; it does however feel dry at times. Keeping your skin hydrated is your biggest challenge.',4,1,'2014-05-30 10:41:10'),
	(121,25,NULL,0,NULL,'Very Dry','','Your skin often feels dehydrated, making it feel tight and occasionally flaky. Keeping your parched skin hydrated is your biggest challenge.',5,1,'2014-05-30 10:41:27'),
	(122,26,NULL,0,NULL,'Occasional blemishes','','',1,1,'2014-05-30 10:44:31'),
	(123,26,NULL,0,NULL,'Regular blemishes','','',2,1,'2014-05-30 10:44:47'),
	(124,26,NULL,0,NULL,'Uneven skin tone','','',3,2,'2014-05-30 10:44:56'),
	(125,26,NULL,0,NULL,'Dull skin','','',4,1,'2014-05-30 10:45:19'),
	(126,26,NULL,0,NULL,'Hydration','','',5,1,'2014-05-30 10:45:35'),
	(127,26,NULL,0,NULL,'Ageing','','',6,1,'2014-05-30 10:45:42'),
	(128,26,NULL,0,NULL,'Sensitivity','','',7,1,'2014-05-30 10:46:18'),
	(129,8,NULL,0,1,'No Concerns','','',5,1,'2014-06-02 11:57:40'),
	(130,18,NULL,0,1,'No Concerns','','',5,1,'2014-06-02 11:57:53');

/*!40000 ALTER TABLE `answer` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table question
# ------------------------------------------------------------

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;

INSERT INTO `question` (`id`, `quizId`, `type`, `typeArg`, `title`, `breadcrumb`, `text`, `summaryText`, `didYouKnow`, `showBreadcrumb`, `order`, `status`, `created`, `siteCatalyst_pageInfo`)
VALUES
	(1,1,1,NULL,'Shh...','Age','Just between us, how many years young are you?',NULL,'Our tea tree leaves are hand harvested in Kenya & distilled to capture their purifying properties.',1,1,1,'2014-05-15 13:06:36','Age: Step 2'),
	(2,1,1,NULL,'What\'s your type?','Skin Type','Your skin type is something you were born with, but it can change over time. Not sure about yours? Simply click each skin type for a description and choose the option that sounds most like you.','You have %s skin','Our Amazonian Camu Camu berries contain up to 60x the vitamin C levels of an orange.',1,2,1,'2014-05-21 16:28:02','Skin Type: Step 3'),
	(3,1,4,1,'Your complexion challenges','Skincare','Can we help you with any of these skin dilemmas? Click all that apply.','You\'d like to tackle %s','Our high alpine edelweiss survives through its renewing properties. We use its precious stem cells to support skin cell renewal.',1,3,1,'2014-05-23 08:28:32','Complexion: Step 4'),
	(4,1,1,NULL,'Spot the difference','Blemishes','Blemishes can happen regardless of your skin type. Why you get them can vary – diet, stress, hormones and lifestyle can all be factors. But how often do you get them?',NULL,'Our tea tree leaves are hand harvested in Kenya & distilled to capture their anti bacterial properties.',0,4,1,'2014-05-23 08:29:29','Blemishes: Step 5'),
	(5,1,1,NULL,'Smooth talk','Hydration','From heating to harsh products, your skin can feel dried out for all kinds of reasons. How dry does your skin usually feel?',NULL,'Our English Camomile is hand harvested In high summer when its calming benefits are most potent.',0,5,1,'2014-05-23 08:29:55','Hydration: Step 6'),
	(6,1,1,NULL,'Your skin stage','Ageing','Age is just a number – your laughter lines are something to celebrate! But where would you say your skin is at?',NULL,'Discover our Drops of Youth™ Eye Concentrate enriched with Edelweiss stem cells to smooth the appearance of bags, lines and fatigue.',0,6,1,'2014-05-23 08:31:34','Ageing: Step 7'),
	(7,1,1,NULL,'A touchy subject','Sensitivity','Redness, itchiness, dryness – sensitive skin has a lot to deal with. To treat it you need to know just how reactive your skin is to the world around you.  \r\n\r\nHow sensitive would you say your skin is?',NULL,'Our Community Fair Trade aloe vera from the Guastatoya farmers in Guatemala Is renowned for its incredible soothing and hydrating properties making it perfect for sensitive skin.',0,7,1,'2014-05-23 08:32:08','Sensitivity: Step 8'),
	(8,1,4,1,'Eye spy','Eyes','Your skin is at its thinnest around your eye area, so it’s important to look after it. Do you see any of these around your eyes?','You have %s around your eyes','The skin around the eyes shows the earliest and most visible signs of ageing, therefor caring for this area is key in any beauty routine.',1,8,1,'2014-05-23 08:37:35','Eyes: Step 9'),
	(9,1,4,1,'Your Lifestyle','Lifestyle','Your skin often reflects your lifestyle and the world around you. What does life look like for you? Click all that apply.','You %s','Our superfood wheatgerm oil is rich in antioxidant vitamin E.',1,9,1,'2014-05-28 10:30:16','Lifestyle: Step 10'),
	(10,1,4,1,'Your make-up routine','Make-up','Make-up can be a great way to enhance your natural beauty but it’s important to remove it properly to help keep pores unclogged. Do you usually wear any of these?','','Our Community Fair Trade chamomile oil comes from Norfolk Essential Oils, a family-run co-operative in England.',0,10,1,'2014-05-28 10:52:16','Make-up: Step 11'),
	(11,2,1,NULL,'Shh...','Age','Just between us, how many years young are you?',NULL,'Our tea tree leaves are hand harvested in Kenya & distilled to capture their purifying properties.',1,1,1,'2014-05-15 13:06:36','Age: Step 2'),
	(12,2,1,NULL,'What\'s your type?','Skin Type','Your skin type is something you were born with, but it can change over time. Not sure about yours? Simply click each skin type for a description and choose the option that sounds most like you.','You have %s skin','Our Amazonian Camu Camu berries contain up to 60x the vitamin C levels of an orange.',1,2,1,'2014-05-21 16:28:02','Skin Type: Step 3'),
	(13,2,4,1,'Your complexion challenges','Skincare','Which of these can we help you with? Click all that apply.','You\'d like to tackle %s','Our high alpine edelweiss survives through its renewing properties. We use its precious stem cells to support skin cell renewal.',1,3,1,'2014-05-23 08:28:32','Complexion: Step 4'),
	(14,2,1,NULL,'Spot the difference','Blemishes','Blemishes can happen regardless of your skin type. Why you get them can vary – diet, stress, hormones and lifestyle can all be factors. But how often do you get them?',NULL,'Our tea tree leaves are hand harvested in Kenya & distilled to capture their anti bacterial properties.',0,4,1,'2014-05-23 08:29:29','Blemishes: Step 5'),
	(15,2,1,NULL,'Smooth talk','Hydration','From heating to harsh products, your skin can feel dried out for all kinds of reasons. How dry does your skin usually feel?','','Our English Camomile is hand harvested In high summer when its calming benefits are most potent.',0,5,1,'2014-05-23 08:29:55','Hydration: Step 6'),
	(16,2,1,NULL,'Your skin stage','Ageing','Age is just a number – your laughter lines are something to celebrate! But where would you say your skin is at?',NULL,'Discover our Drops of Youth™ Eye Concentrate enriched with Edelweiss stem cells to smooth the appearance of bags, lines and fatigue.',0,6,1,'2014-05-23 08:31:34','Ageing: Step 7'),
	(17,2,1,NULL,'A touchy subject','Sensitivity','Redness, itchiness, dryness – sensitive skin has a lot to deal with. To treat it you need to know just how reactive your skin is to the world around you.  \r\n\r\nHow sensitive would you say your skin is?',NULL,'Our Community Fair Trade aloe vera from the Guastatoya farmers in Guatemala Is renowned for its incredible soothing and hydrating properties making it perfect for sensitive skin.',0,7,1,'2014-05-23 08:32:08','Sensitivity: Step 8'),
	(18,2,4,1,'Eye spy','Eyes','Your skin is at its thinnest around your eye area, so it’s important to look after it. Do you see any of these around your eyes?','You have %s around your eyes','The skin around the eyes shows the earliest and most visible signs of ageing, therefor caring for this area is key in any beauty routine.',1,8,1,'2014-05-23 08:37:35','Eyes: Step 9'),
	(19,2,4,1,'Your Lifestyle','Lifestyle','Your skin often reflects your lifestyle and the world around you. What does life look like for you? Click all that apply.','You %s','Our superfood wheatgerm oil is rich in antioxidant vitamin E.',1,9,1,'2014-05-28 10:30:16','Lifestyle: Step 10'),
	(21,3,1,NULL,'Shh...','Age','Just between us, how many years young are you?','','Our tea tree leaves are hand harvested in Kenya & distilled to capture their purifying properties.',1,1,1,'2014-05-30 10:17:49','Age: Step 2'),
	(22,3,1,NULL,'What\'s Your Type','Skin Type','Your skin type is something you were born with, but it can change over time. Not sure about yours? Simply click each skin type for a description and choose the option that sounds most like you.','You have %s skin','Our Amazonian Camu Camu berries contain up to 60x the vitamin C levels of an orange.',1,2,1,'2014-05-30 10:20:08','Skin Type: Step 3'),
	(23,3,4,1,'Your complexion challenges','Skincare','Which of these can we help you with? Click all that apply.','You\'d like to tackle %s','Our high alpine edelweiss survives through its renewing properties. We use its precious stem cells to support skin cell renewal.',1,3,1,'2014-05-30 10:27:48','Complexion: Step 4'),
	(24,4,1,NULL,'Shh...','Age','Just between us, how many years young are you?','','Our tea tree leaves are hand harvested in Kenya & distilled to capture their purifying properties.',1,1,1,'2014-05-30 10:38:04','Age: Step 2'),
	(25,4,1,NULL,'What\'s Your Type','Skin Type','Your skin type is something you were born with, but it can change over time. Not sure about yours? Simply click each skin type for a description and choose the option that sounds most like you.','You have %s skin','Our Amazonian Camu Camu berries contain up to 60x the vitamin C levels of an orange.',1,2,1,'2014-05-30 10:39:55','Skin Type: Step 3'),
	(26,4,4,1,'Your complexion challenges','Skincare','Can we help you with any of these skin dilemmas? Click all that apply.','You\'d like to tackle %s','Our high alpine edelweiss survives through its renewing properties. We use its precious stem cells to support skin cell renewal.',1,3,1,'2014-05-30 10:44:08','Complexion: Step 4');

/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table quiz
# ------------------------------------------------------------

LOCK TABLES `quiz` WRITE;
/*!40000 ALTER TABLE `quiz` DISABLE KEYS */;

INSERT INTO `quiz` (`id`, `regime`, `gender`, `status`, `created`, `siteCatalyst_subCategoryKey`, `siteCatalyst_subCategoryValue`)
VALUES
	(1,2,3,1,'2014-05-15 13:06:26','subCategory1','Female: Complete'),
	(2,2,2,1,'2014-05-28 11:04:49','subCategory1','Male: Complete'),
	(3,1,2,1,'2014-05-30 10:15:14','subCategory1','Male: Express'),
	(4,1,3,1,'2014-05-30 10:37:03','subCategory1','Female: Express');

/*!40000 ALTER TABLE `quiz` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
