<?php
/* @var $this ProductVariantController */
/* @var $model ProductVariant */

$this->breadcrumbs=array(
	_('Products')=>array('product/admin'),
	$model->product->name=>array('product/view','id'=>$model->product->id),
	$model->name,
);

$this->menu=array(
	array('label'=>_('Update Product Variant'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>_('Delete Product Variant'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('zii','Are you sure you want to delete this item?'))),
	array('label'=>'<hr style="margin: 0;" />'),
	array('label'=>_('Back to Product'), 'url'=>array('product/view','id'=>$model->product->id)),
);
?>

<h1><?php echo _('View Product Variant'); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'code',
		'name',
		'description',
		'ean',
		array(
			'name'=>'price',
			'value'=>money_format("%.2n", $model->price),
		),
		'stock',
		'statusName',
	),
)); ?>
