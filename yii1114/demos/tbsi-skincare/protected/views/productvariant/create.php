<?php
/* @var $this ProductVariantController */
/* @var $model ProductVariant */

$this->breadcrumbs=array(
	_('Products')=>array('product/admin'),
	$product->name=>array('product/view','id'=>$product->id),
	_('Create Product Variant'),
);

$this->menu=array(
	array('label'=>_('Back to Product'), 'url'=>array('product/view','id'=>$product->id)),
);
?>

<h1><?php echo _('Create Product Variant'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>