<?php
/* @var $this ProductRangeController */
/* @var $model ProductRange */

$this->breadcrumbs=array(
	_('Products')=>array('product/admin'),
	_('Product Ranges')=>array('productrange/admin'),
	$model->name,
);

$this->menu=array(
	array('label'=>_('Update Product Range'), 'url'=>array('update', 'id'=>$model->id)),
);
?>

<h1><?php echo _('View Product Range'); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'name',
		'description',
		'hexCode',
		array(
			'name'=>'file',
			'type'=>'raw',
			'value'=>$model->fileName ? '<img src="'.$model->getUrl().'" alt="" />' : '',
		),
	),
)); ?>
