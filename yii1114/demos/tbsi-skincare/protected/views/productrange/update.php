<?php
/* @var $this ProductRangeController */
/* @var $model ProductRange */

$this->breadcrumbs=array(
	_('Products')=>array('product/admin'),
	_('Product Ranges')=>array('productrange/admin'),
	$model->name=>array('view','id'=>$model->id),
	_('Update'),
);

$this->menu=array(
	array('label'=>_('View Product Range'), 'url'=>array('view', 'id'=>$model->id)),
);
?>

<h1><?php echo _('Update Product Range'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>