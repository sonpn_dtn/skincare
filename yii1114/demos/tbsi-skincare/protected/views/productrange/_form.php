<?php
/* @var $this ProductRangeController */
/* @var $model ProductRange */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'product-range-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>

	<p class="note"><?php echo _('Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hexCode'); ?>
		<?php echo $form->textField($model,'hexCode',array('maxlength'=>6)); ?>
		<?php echo $form->error($model,'hexCode'); ?>
	</div>

	<div class="row">
		<div>
			<?php echo $form->labelEx($model,'file'); ?>
			
			<span class="file-preview">
				<?php if($model->fileName) { ?>
				<span>
					<img src="<?php echo $model->getUrl(); ?>" alt="" />
				</span>
				<?php } ?>
			</span>
		</div>
			
        <?php echo $form->fileField($model, 'file');?>
		<?php echo $form->error($model,'file'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? _('Create') : _('Save')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->