<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Scraping Price & Stock')
);

?>

<h1><?php echo _('Scraping Price & Stock'); ?></h1>

<p><?php echo _('Price and stock information is updated for every product variant every hour. It is also updated when you view a product in the CMS, ensuring that you always see the most up-to-date information. There is no need to manually enter price and stock information in to each product variant.'); ?></p>
<p><?php echo _('The price and stock information can be updated on demand by clicking the "Products" link in the main menu at the top of the screen, then click the "Scrape Price & Stock" link in the "Operations" menu to the right of the screen.'); ?></p>