<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Managing Products')=>array('help/page','view'=>'managingproducts'),
	_('Searching Products')
);

?>

<h1><?php echo _('Searching Products'); ?></h1>

<p><?php echo _('To search for products, click the "Products" link in the main menu at the top of the screen.'); ?></p>
<p><?php echo _('Alternatively, follow the link below.'); ?></p>
<p><a href="<?php echo $this->createUrl('product/admin'); ?>"><?php echo $this->createAbsoluteUrl('product/admin'); ?></a></p>

<br />

<ul>
	<li><?php echo _('Use the fields at the top of the grid to search and filter the data.'); ?></li>
	<li><?php echo _('Use the page numbers at the bottom of the grid to browse through the data.'); ?></li>
	<li><?php echo sprintf(_('You can click the %s icon to view more information on a specific product.'), '<img src="'.Yii::app()->baseUrl.'/images/blueprint/view.png" alt="" />'); ?></li>
</ul>