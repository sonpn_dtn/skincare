<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Product Ranges')
);

?>

<h1><?php echo _('Product Ranges'); ?></h1>

<p><?php echo _('To manage product ranges, click the "Products" link in the main menu at the top of the screen, then click the "Manage Product Ranges" link in the "Operations" menu to the right of the screen.'); ?></p>
<p><?php echo _('Alternatively, follow the link below.'); ?></p>
<p><a href="<?php echo $this->createUrl('productrange/admin'); ?>"><?php echo $this->createAbsoluteUrl('productrange/admin'); ?></a></p>

<br />

<ul>
	<li><?php echo _('Product ranges are used to categorise products. They are shown on the results page to help visually separate products.'); ?></li>
	<li><?php echo _('Product ranges are managed separately to products. A new product range must be created before creating the products that go in it. Updating an existing product range will affect all associated products.'); ?></li>
	<li><?php echo _('Product ranges cannot be deleted.'); ?></li>
</ul>