<?php

$this->breadcrumbs=array(
	_('Help'),
);

?>

<h1><?php echo _('About This CMS'); ?></h1>

<p><?php echo _('This CMS can be used to manage the online skincare diagnostic tool for the Body Shop.'); ?></p>
<p><?php echo _('The CMS can be broken down in to 2 top-level functions.'); ?></p>

<ul>
	<li><?php echo _('Management of products.'); ?></li>
	<li><?php echo _('Management of quizzes.'); ?></li>
</ul>

<p><?php echo _('When using the CMS, please take note of the following conventions.'); ?></p>

<ul>
	<li><?php echo _('Navigate between managing products and quizzes using the main menu bar at the top of the screen.'); ?></li>
	<li><?php echo _('When managing products or quizzes, you will have the option to create new data, update existing data and delete old data. Links to these options, as well as other options, can always be found in the "Operations" menu to the right of the screen.'); ?></li>
	<li><?php echo _('You can always check your current position in the CMS by following the "breadcrumb" trail which is shown directly underneath the main menu bar at the top of the screen'); ?></li>
	<li><?php echo _('Where there are "lists" of data, the CMS will always display the data in a grid. Use the fields at the top of the grid to search and filter the data. Use the page numbers at the bottom of the grid to browse through the data.'); ?></li>
	<li><?php echo sprintf(_('When working with grids, you can use the icons in the right-most column to view %s, edit %s or delete %s the current row.'), '<img src="'.Yii::app()->baseUrl.'/images/blueprint/view.png" alt="" />', '<img src="'.Yii::app()->baseUrl.'/images/blueprint/update.png" alt="" />', '<img src="'.Yii::app()->baseUrl.'/images/blueprint/delete.png" alt="" />'); ?></li>
	<li><?php echo sprintf(_('Some grids allow for sorting of data. If when you hover over a grid you see the %s icon, you can click and drag rows up and down to reorder the data.'), '<img src="'.Yii::app()->baseUrl.'/images/blueprint/move.gif" alt="" />'); ?></li>
</ul>

<p>
	<strong><?php echo _('An example of grid data:'); ?></strong><br />
	<img src="<?php echo Yii::app()->baseUrl; ?>/images/blueprint/screenshot.png" alt="" />
</p>