<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Product Tags')=>array('help/page','view'=>'producttags'),
	_('Updating Product Tags')
);

?>

<h1><?php echo _('Updating Product Tags'); ?></h1>

<p><?php echo sprintf(_('To update an existing product tag, click the "Products" link in the main menu at the top of the screen, then click the "Manage Product Tags". Find the product tag you wish to update and then click the %s icon.'), '<img src="'.Yii::app()->baseUrl.'/images/blueprint/update.png" alt="" />'); ?></a></p>

<br />

<ul>
	<li><?php echo _('Complete the form to update a product tag.'); ?></li>
	<li><?php echo _('The "Name" will be displayed on the results page.'); ?></li>
</ul>