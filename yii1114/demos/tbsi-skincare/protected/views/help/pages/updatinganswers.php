<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Answers')=>array('help/page','view'=>'answers'),
	_('Updating Answers')
);

?>

<h1><?php echo _('Updating Answers'); ?></h1>

<p><?php echo sprintf(_('To update an answer, first find an answer list (see <a href="%s">Answers</a>).'), $this->createUrl('help/page',array('view'=>'answers'))); ?></p>
<p><?php echo sprintf(_('Then click the %s icon next to the answer you want to update.'), '<img src="'.Yii::app()->baseUrl.'/images/blueprint/update.png" alt="" />'); ?></p>

<br />

<ul>
	<li><?php echo _('Complete the form to update an answer.'); ?></li>
	<li><?php echo _('The "Text" will be displayed as a selection for the user.'); ?></li>
	<li><?php echo _('The "Description" can be used to show more information to the user when they click on the answer.'); ?></li>
	<li><?php echo _('The "Go To Question" can be used to skip over questions that will not be relevant if this answer is chosen.'); ?></li>
	<li><?php echo _('If you enter "Summary Text" then this will show on the summary page when the user completes the quiz. This is different to the summary text of a question as it provides a finer grain of control (i.e. if the user chooses this answer, the summary text will be shown exactly as entered in this box).'); ?></li>
	<li><?php echo _('You can set an answer "Status" to "Active" or "Inactive".'); ?></li>
</ul>

<strong><?php echo _('An example of skipping questions:'); ?></strong>
<p><?php echo _('In this example there is a multiple-choice question with 4 possible answers for skin concerns: Dry, Normal, Oily and Combination. We will call this Question 1.'); ?></p>
<p><?php echo _('2 other questions have also been created to find out more about those users with Dry skin concerns (Question 2) and Oily skin concerns (Question 3).'); ?></p>
<p><?php echo _('For the Dry answer, the "Go To Question" should be set to Question 2.'); ?></p>
<p><?php echo _('For the Oily answer, the "Go To Question" should be set to Question 3.'); ?></p>
<p><?php echo _('For the Normal and Combinations answers, the "Go To Question" should be set to Question 4 (i.e. skipping past Question 2 and 3).'); ?></p>
<p><?php echo _('Furthermore, when setting up the answers for Questions 2 and 3, all the answers should also be set to "Go To Question 4". This is to prevent the default functionality which always asks the next question in sequence.'); ?></p>
<p><?php echo _('If a user makes multiple selections where the "Go To Question" is set, the tool will make sure the user skips through the correct questions sequentially.'); ?></p>