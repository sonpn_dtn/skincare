<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Managing Products')
);

?>

<h1><?php echo _('Managing Products'); ?></h1>

<p><?php echo _('Products are broken down in to 2 levels.'); ?></p>

<ul>
	<li>
		<p><strong><?php echo _('Products'); ?></strong></p>

		<p><?php echo _('As seen on the Body Shop website (e.g. Nutriganics™ Drops of Youth).'); ?></p>
		<p><?php echo _('The following product information can be stored against each product.'); ?></p>
		
		<ul>
			<li><?php echo _('Name'); ?></li>
			<li><?php echo _('Code'); ?></li>
			<li><?php echo _('Details'); ?></li>
			<li><?php echo _('Detail Bullet Points'); ?></li>
			<li><?php echo _('Gender'); ?></li>
			<li><?php echo _('Day/Night'); ?></li>
			<li><em><?php echo _('Range'); ?></em></li>
			<li><em><?php echo _('Tag'); ?></em></li>
		</ul>
		
		<p><em><?php echo sprintf(_('Note: Product ranges/tags and managed separately to individual products. Editing a range/tag will update every product under that range or tag. For more information, see <a href="%s">Product Ranges</a> and <a href="%s">Product Tags</a>.'), $this->createUrl('help/page',array('view'=>'productranges')), $this->createUrl('help/page',array('view'=>'producttags'))); ?></em></p>
	</li>
</ul>

<ul>
	<li>
		<p><strong><?php echo _('Product Variants'); ?></strong></p>

		<p><?php echo _('Variations of an individual product (e.g. bottle sizes 100ml, 200ml, etc.).'); ?></p>
		<p><?php echo _('The following product information can be stored against each product variant.'); ?></p>

		<ul>
			<li><?php echo _('Name'); ?></li>
			<li><?php echo _('Code'); ?></li>
			<li><?php echo _('Details'); ?></li>
			<li><?php echo _('Detail Bullet Points'); ?></li>
			<li><?php echo _('Gender'); ?></li>
			<li><?php echo _('Day/Night'); ?></li>
			<li><em><?php echo _('Range'); ?></em></li>
			<li><em><?php echo _('Tag'); ?></em></li>
		</ul>

		<p><?php echo sprintf(_('For more information, see <a href="%s">Product Variants</a>.'), $this->createUrl('help/page',array('view'=>'productvariants'))); ?></p>
	</li>
</ul>

<p><?php echo sprintf(_('roducts can be uploaded in bulk via CSV, but only when the skincare tool is being set up for the very first time. For more information, see <a href="%s">Bulk Upload</a>.'), $this->createUrl('help/page',array('view'=>'bulkupload'))); ?></p>