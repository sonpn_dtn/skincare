<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Questions')=>array('help/page','view'=>'questions'),
	_('Adding Questions'),
);

?>

<h1><?php echo _('Adding Questions'); ?></h1>

<p><?php echo sprintf(_('To add a new question, first find a question list (see <a href="%s">Questions</a>).'), $this->createUrl('help/page',array('view'=>'questions'))); ?></p>
<p><?php echo _('Then click the "Add Question" link in the "Operations" menu to the right of the screen.'); ?></p>

<br />

<ul>
	<li><?php echo _('Complete the form to add a new question.'); ?></li>
	<li><?php echo _('The "Type" of the question can be "Single-select" or "Multiple-select".'); ?></li>
	<li><?php echo _('If you choose multiple-select, you must also choose the minimum number of selections the user must make to proceed.'); ?></li>
	<li><?php echo _('The "Title" and "Text" will be displayed at the top of the question screen to pose the question to the user.'); ?></li>
	<li><?php echo _('The "Did You Know" will be displayed to the right of the screen for factual information related to the question.'); ?></li>
	<li><?php echo _('The "Breadcrumb" is the text shown in the progress bar at the bottom of the screen. If you do not check "Show Breadcrumb", then the question will not appear in the progress bar.'); ?></li>
	<li><?php echo sprintf(_('If you enter "Summary Text" then this will show on the summary page when the user completes the quiz. You can use the code %s to show the given answer.'), htmlentities('<span>%s</span>')); ?></li>
	<li><?php echo _('You can set a quiz "Status" to "Active" or "Inactive".'); ?></li>
</ul>

<p>
	<strong><?php echo _('An example of summary text:'); ?></strong>
	<?php echo sprintf(_('You have chosen %s.'), htmlentities('<span>%s</span>')); ?>
</p>

<p><?php echo _('If the user chooses "Oily Skin", on the summary page the above text would show "You have chosen Oily Skin".'); ?></p>