<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Related Products')
);

?>

<h1><?php echo _('Related Products'); ?></h1>

<p><?php echo _('In this CMS, the term "Related Products" refers to the relationship between products and answers to questions. Once the relationship between a product and an answer has been defined, it can be given a weight (i.e. a number) to further define the strength of the relationship.'); ?></p>
<p><?php echo _('Typically the number should be between 0 and 100. The higher the number, the stronger the relationship. However, you can give weights adversely high or low numbers depending on the desired outcome. For example, if you input a weight of -90000, the product will never be shown as a result. Conversely if you input a weight of 90000, the product will guarantee to show.'); ?></p>

<br />

<p><?php echo _('The results can be significantly improved by understanding how weights are used to calculate the results. Everytime a user selects an answer, the related products for that answer are stored. As the user answers more questions, the list of related products grows. An example of this is shown in the table below.'); ?></p>

<br />

<strong><?php echo _('Related products after answering 1 question.'); ?></strong>

<table>
	<tr>
		<th><?php echo _('Product'); ?></th>
		<th><?php echo _('Weight'); ?></th>
	</tr>
	<tr>
		<td><?php echo _('Moisture White Eye Serum'); ?></td>
		<td>80</td>
	</tr>
	<tr>
		<td><?php echo _('Pomegranate Softening Facial Wash'); ?></td>
		<td>60</td>
	</tr>
	<tr>
		<td><?php echo _('Tea Tree Cool & Creamy Wash'); ?></td>
		<td>-90000</td>
	</tr>
</table>

<strong><?php echo _('Related products after answering 2 questions.'); ?></strong>

<table>
	<tr>
		<th><?php echo _('Product'); ?></th>
		<th><?php echo _('Weight'); ?></th>
	</tr>
	<tr>
		<td><?php echo _('Moisture White Eye Serum'); ?></td>
		<td>80</td>
	</tr>
	<tr>
		<td><?php echo _('Pomegranate Softening Facial Wash'); ?></td>
		<td>60</td>
	</tr>
	<tr>
		<td><?php echo _('Tea Tree Cool & Creamy Wash'); ?></td>
		<td>-90000</td>
	</tr>
	<tr>
		<td><?php echo _('Moisture White Eye Serum'); ?></td>
		<td>60</td>
	</tr>
	<tr>
		<td><?php echo _('Vitamin E Night Cream'); ?></td>
		<td>60</td>
	</tr>
	<tr>
		<td><?php echo _('Tea Tree Cool & Creamy Wash'); ?></td>
		<td>80</td>
	</tr>
</table>

<p><?php echo _('You can see products can appear more than once, depending on the answers selected.'); ?></p>
<p><?php echo _('At the end of the quiz, the tool takes the mean average of all the related products.'); ?></p>

<br />

<strong><?php echo _('From the example above, the following results would be generated.'); ?></strong>

<table>
	<tr>
		<th><?php echo _('Product'); ?></th>
		<th><?php echo _('Weight'); ?></th>
	</tr>
	<tr>
		<td><?php echo _('Moisture White Eye Serum'); ?></td>
		<td>70</td>
	</tr>
	<tr>
		<td><?php echo _('Pomegranate Softening Facial Wash'); ?></td>
		<td>30</td>
	</tr>
	<tr>
		<td><?php echo _('Vitamin E Night Cream'); ?></td>
		<td>30</td>
	</tr>
	<tr>
		<td><?php echo _('Tea Tree Cool & Creamy Wash'); ?></td>
		<td>-44960</td>
	</tr>
</table>

<p><?php echo _('So in this example, "Moisture White Eye Serum" would be the most relevant result, whereas "Tea Tree Cool & Creamy Wash" would never be suggested.'); ?></p>
<p><em><?php echo _('Note: The tool also reads other product attributes such as "Gender", "Tag", "Day/Night", "Stock" and "Status" to ensure only the most relevant products are shown.'); ?></em></p>

<br />

<p><?php echo sprintf(_('To view related products, first find a answer list (see <a href="%s">Answers</a>).'), $this->createUrl('help/page',array('view'=>'answers'))); ?></p>
<p><?php echo sprintf(_('Then click the %s icon next to the answer you want view related products of.'), '<img src="'.Yii::app()->baseUrl.'/images/blueprint/view.png" alt="" />'); ?></p>

<br />

<p><?php echo _('From this screen, use the "Product" box to search for a product (you can type a product code or name).'); ?></p>
<p><?php echo _('Enter a "Weight" as described above, and click "Add" to add the related product.'); ?></p>