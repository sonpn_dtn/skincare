<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Managing Products')=>array('help/page','view'=>'managingproducts'),
	_('Deleting Products')
);

?>

<h1><?php echo _('Deleting Products'); ?></h1>

<p><?php echo sprintf(_('To delete a product, click the "Products" link in the main menu at the top of the screen. Find the product you wish to delete and then click the %s icon.'), '<img src="'.Yii::app()->baseUrl.'/images/blueprint/delete.png" alt="" />'); ?></a></p>

<br />

<ul>
	<li><?php echo _('You will be prompted to confirm your actions.'); ?></li>
	<li><?php echo _('Deleting is irreversible.'); ?></li>
	<li><?php echo _('The product (including all product variants) will be deleted.'); ?></li>
</ul>