<?php

$this->breadcrumbs=array(
	_('Help')=>array('help/page','view'=>'index'),
	_('Product Tags')=>array('help/page','view'=>'producttags'),
	_('Adding Product Tags')
);

?>

<h1><?php echo _('Adding Product Tags'); ?></h1>

<p><?php echo _('To add a new product tag, click the "Products" link in the main menu at the top of the screen, then click the "Manage Product Tags" then "Create Product Tag" links in the "Operations" menu to the right of the screen.'); ?></p>
<p><?php echo _('Alternatively, follow the link below.'); ?></p>
<p><a href="<?php echo $this->createUrl('tag/create'); ?>"><?php echo $this->createAbsoluteUrl('tag/create'); ?></a></p>

<br />

<ul>
	<li><?php echo _('Complete the form to add a new product tag.'); ?></li>
	<li><?php echo _('The "Name" will be displayed on the results page.'); ?></li>
</ul>