<?php
/* @var $this ProductController */
/* @var $model Product */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'product-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo _('Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'productRangeId'); ?>
		<?php echo $form->dropDownList($model,'productRangeId',array(null=>_('Please select')) + CHtml::listData($range, 'id', 'name')); ?>
		<?php echo $form->error($model,'productRangeId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tagId'); ?>
		<?php echo $form->dropDownList($model,'tagId',array(null=>_('Please select')) + CHtml::listData($tag, 'id', 'name')); ?>
		<?php echo $form->error($model,'tagId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'code'); ?>
		<?php echo $form->textField($model,'code',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row rememberMe">
		<?php echo $form->checkbox($model,'bestSeller'); ?>
		<?php echo $form->labelEx($model,'bestSeller'); ?>
		<?php echo $form->error($model,'bestSeller'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'details'); ?>
		<?php echo $form->textArea($model,'details',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'details'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'detailBullets'); ?>
		<p class="hint"><?php echo _('Enter a pipe "|" to separate each bullet point.'); ?></p>
		<?php echo $form->textArea($model,'detailBullets',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'detailBullets'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'url'); ?>
		<?php echo $form->textArea($model,'url',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'url'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gender'); ?>
		<?php echo $form->dropDownList($model,'gender',array(null=>_('Please select')) + $model->genders()); ?>
		<?php echo $form->error($model,'gender'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dayNight'); ?>
		<?php echo $form->dropDownList($model,'dayNight',array(null=>_('Please select')) + $model->dayNightNames()); ?>
		<?php echo $form->error($model,'dayNight'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',$model->statuses()); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? _('Create') : _('Save')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
