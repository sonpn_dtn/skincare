<?php
/* @var $this ProductController */
/* @var $model Product */

$this->breadcrumbs=array(
	_('Products')=>array('admin'),
	$model->name,
);

$this->menu=array(
	array('label'=>_('Update Product'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>_('Delete Product'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('zii','Are you sure you want to delete this item?'))),
	array('label'=>'<hr style="margin: 0;" />'),
	array('label'=>_('Create Product Variant'), 'url'=>array('productvariant/create','id'=>$model->id)),
);
?>

<h1><?php echo _('View Product'); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		array(
			'label'=>_('Range'),
			'value'=>$model->productRange->name,
		),
		'code',
		'name',
		'details',
		'detailBulletsHtml:html',
		'url:url',
		'genderName',
		'dayNightName',
		array(
			'name'=>'bestSeller',
			'value'=>$model->bestSeller ? _('Yes') : _('No'),
		),
		array(
			'label'=>_('Tag'),
			'value'=>$model->tag ? $model->tag->name : null,
		),
		'statusName',
		array(
			'label'=>_('Image'),
			'type'=>'html',
			'value'=>'<img src="'.$model->getImageUrl(Product::IMAGE_SIZE_LARGE).'" alt="" />'
		),
	),
)); ?>

<h2 style="margin: 1em 0 0;"><?php echo _('Variants'); ?></h2>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'variant-grid',
	'dataProvider'=>new CArrayDataProvider($model->productVariants),
	'filter'=>null,
	'template'=>'{items}',
	'columns'=>array(
		array(
			'header'=>_('Code'),
			'name'=>'code',
		),
		array(
			'header'=>_('Name'),
			'name'=>'name',
		),
		array(
			'header'=>_('Description'),
			'name'=>'description',
		),
		array(
			'header'=>_('Price'),
			'value'=>'money_format("%.2n", $data->price)',
		),
		array(
			'header'=>_('Stock'),
			'value'=>'$data->stock ? $data->stock : 0',
		),
		array(
			'header'=>_('Status'),
			'value'=>'$data->statusName',
		),
		array(
			'class'=>'CButtonColumn',
			'viewButtonUrl'=>'Yii::app()->urlManager->createUrl("productvariant/view", array("id"=>$data->id))',
			'updateButtonUrl'=>'Yii::app()->urlManager->createUrl("productvariant/update", array("id"=>$data->id))',
			'deleteButtonUrl'=>'Yii::app()->urlManager->createUrl("productvariant/delete", array("id"=>$data->id))',
		),
	),
));

/* if($model->categories) { ?>

<h2 style="margin: 0;"><?php echo _('Categories'); ?></h2>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'category-grid',
	'dataProvider'=>new CArrayDataProvider($model->categories),
	'filter'=>null,
	'template'=>'{items}',
	'columns'=>array(
		array(
			'header'=>_('Name'),
			'name'=>'name',
		),
		array(
			'header'=>_('Description'),
			'type'=>'html',
			'name'=>'description',
		),
		array(
			'header'=>_('Status'),
			'value'=>'$data->statusName',
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{update}',
			'viewButtonUrl'=>'Yii::app()->urlManager->createUrl("category/view", array("id"=>$data->id))',
			'updateButtonUrl'=>'Yii::app()->urlManager->createUrl("category/update", array("id"=>$data->id))',
		),
	),
));

} */
