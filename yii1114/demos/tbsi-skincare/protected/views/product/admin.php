<?php
/* @var $this ProductController */
/* @var $model Product */

$this->breadcrumbs=array(
	_('Products')=>array('admin'),
	_('Manage'),
);

$this->menu=array(
	array('label'=>_('Create Product'), 'url'=>array('create')),
	array('label'=>'<hr style="margin: 0;" />'),
	array('label'=>_('Manage Product Ranges'), 'url'=>array('productrange/admin')),
	array('label'=>_('Manage Product Tags'), 'url'=>array('tag/admin')),
	array('label'=>'<hr style="margin: 0;" />'),
	array('label'=>_('Bulk Upload'), 'url'=>array('bulkupload')),
	array('label'=>_('Scrape Price & Stock'), 'url'=>array('scrapepricestock')),
);
?>

<h1><?php echo _('Manage Products'); ?></h1>

<?php if(Yii::app()->user->hasFlash('message')) { ?>
	<div class="flash-success">
		<?php echo Yii::app()->user->getFlash('message'); ?>
	</div>
<?php }

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'product-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'code',
		'name',
		array(
			'name'=>'gender',
			'value'=>'$data->genderName',
			'filter'=>$model->genders(),
		),
		array(
			'name'=>'status',
			'value'=>'$data->statusName',
			'filter'=>$model->statuses(),
		),
		array(
			'class'=>'CButtonColumn',
		),
	),
));