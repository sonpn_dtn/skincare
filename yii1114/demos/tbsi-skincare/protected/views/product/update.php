<?php
/* @var $this ProductController */
/* @var $model Product */

$this->breadcrumbs=array(
	_('Products')=>array('admin'),
	$model->name=>array('view','id'=>$model->id),
	_('Update'),
);

$this->menu=array(
	array('label'=>_('Manage Products'), 'url'=>array('admin')),
);
?>

<h1><?php echo _('Update Product'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'range'=>ProductRange::model()->findAll(),'tag'=>Tag::model()->findAll())); ?>