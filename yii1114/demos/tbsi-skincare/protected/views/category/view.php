<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->breadcrumbs=array(
	_('Categories'),
	$model->name,
);

$this->menu=array(
	array('label'=>_('Update Category'), 'url'=>array('update', 'id'=>$model->id)),
);
?>

<h1><?php echo _('View Category'); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'code',
		'name',
		'description:html',
		'statusName',
	),
)); ?>
