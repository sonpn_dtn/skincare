<?php
/* @var $this AnswerController */
/* @var $model Answer */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'answer-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo _('Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo $form->errorSummary($model); ?>
	<?php echo $form->hiddenField($model,'questionId',array('value'=>$questionModel->id)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'text'); ?>
		<?php echo $form->textField($model,'text',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'text'); ?>
	</div>

	<div class="row rememberMe">
		<?php echo $form->checkbox($model,'unique'); ?>
		<?php echo $form->labelEx($model,'unique'); ?>
		<p class="hint">
			<?php echo _('For multiple-select questions: when unique is checked, this answer will act as a single-select answer.'); ?>
		</p>
		<?php echo $form->error($model,'unique'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'goToQuestionId'); ?>
		<p class="hint">
			<?php echo _('Select a question below to skip to if the user chooses this answer.<br />For multiple-select questions, the user will skip through each question sequentially.'); ?>
		</p>
		<?php echo $form->dropDownList($model,'goToQuestionId',array(null=>_('Please select')) + CHtml::listData($questionModel->quiz->questions, 'id', 'title')); ?>
		<?php echo $form->error($model,'goToQuestionId'); ?>
	</div>

	<div class="row rememberMe">
		<?php echo $form->checkbox($model,'skipToEnd'); ?>
		<?php echo $form->labelEx($model,'skipToEnd'); ?>
		<?php echo $form->error($model,'skipToEnd'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'summaryText'); ?>
		<?php echo $form->textField($model,'summaryText',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'summaryText'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',$model->statuses()); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? _('Create') : _('Save')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
