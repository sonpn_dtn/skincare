<?php
/* @var $this RegionController */
/* @var $model Region */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'region-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo _('Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'countryCode'); ?>
		<p class="hint"><?php echo _('Look up <a href="http://en.wikipedia.org/wiki/ISO_3166-1" target="_blank">ISO 3166-1 codes</a>'); ?></p>
		<?php echo $form->textField($model,'countryCode',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'countryCode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'languageCode'); ?>
		<p class="hint"><?php echo _('Look up <a href="http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes" target="_blank">ISO 639-1 codes</a>'); ?></p>
		<?php echo $form->textField($model,'languageCode',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'languageCode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',$model->statuses()); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? _('Create') : _('Save')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->