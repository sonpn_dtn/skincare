<?php
/* @var $this RegionController */
/* @var $model Region */

$this->breadcrumbs=array(
	_('Regions')=>array('admin'),
	_('Manage'),
);

$this->menu=array(
	array('label'=>_('Create Region'), 'url'=>array('create')),
);
?>

<h1><?php echo _('Manage Regions'); ?></h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'region-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'name',
		'countryCode',
		'languageCode',
		array(
			'name'=>'status',
			'value'=>'$data->statusName',
			'filter'=>$model->statuses(),
		),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
