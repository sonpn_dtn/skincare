<?php
/* @var $this RegionController */
/* @var $model Region */

$this->breadcrumbs=array(
	_('Regions')=>array('admin'),
	$model->name,
);

$this->menu=array(
	array('label'=>_('Update Region'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>_('Delete Region'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('zii','Are you sure you want to delete this item?'))),
);
?>

<h1><?php echo _('View Region'); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'name',
		'countryCode',
		'languageCode',
		'statusName',
	),
)); ?>
