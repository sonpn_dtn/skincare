<div class="clear">
	<div class="clear">
		<div class="left stat-box">
			<h3>Female Express</h3>
			<p>Females who have completed the express quiz</p>
			<div class="count"><?php echo number_format($femaleExpress); ?></div>
			<p>
				<?php foreach($femaleExpressA as $key=>$v)
					echo 'Q.'.($key+1).' ('.$questionArray[$key].'): <span>'.$v.'</span> <br/>';
				?>
			</p>	
		</div>	

		<div class="left stat-box">
			<h3>Male Express</h3>
			<p>Males who have completed the express quiz</p>
			<div class="count"><?php echo number_format($maleExpress); ?></div>
			<p>
				<?php foreach($maleExpressA as $key=>$v)
					echo 'Q.'.($key+1).' ('.$questionArray[$key].'): <span>'.$v.'</span> <br/>';

				?>
			</p>	

		</div>
	</div>
	<div class="clear">
		<div class="left stat-box">
			<h3>Female Complete</h3>
			<p>Females who have completed the full quiz</p>
			<div class="count"><?php echo number_format($femaleComplete); ?></div>
			<p>
				<?php foreach($femaleCompleteA as $key=>$v)
					echo 'Q.'.($key+1).' ('.$questionArray[$key].'): <span>'.$v.'</span> <br/>';
				?>
			</p>
		</div>

		<div class="left stat-box">
			<h3>Male Complete</h3>
			<p>Males who have completed the full quiz</p>
			<div class="count"><?php echo number_format($maleComplete+1); ?></div>
			<p>
				<?php foreach($maleCompleteA as $key=>$v)
					echo 'Q.'.($key+1).' ('.$questionArray[$key].'): <span>'.$v.'</span> <br/>';
				?>
			</p>	
		</div>
	</div>	

	<div class="left stat-box">
		<h3>Newsletter</h3>
		<p>People who have opted in for the email newsletter</p>
		<div class="count"><?php echo number_format($emails); ?></div>
		
		<a class="no-display" href="<?php echo $this->createUrl('email'); ?>">Download all users (CSV)</a>
	</div>

	<div class="left stat-box">
		<h3>Factsheet</h3>
		<p>People who have sent the factsheet to their email address</p>
		<div class="count"><?php echo number_format($factsheet); ?></div>
		<a class="no-display" href="<?php echo $this->createUrl('factsheets'); ?>">Download all users (CSV)</a>
	</div>

	<div class="left stat-box">
		<h3>Facebook</h3>
		<p>Number of people who have shared the tool on facebook</p>
		<div class="count"><?php echo $facebook; ?></div>
		
	</div>

	<div class="left stat-box">
		<h3>Twitter</h3>
		<p>Number of people who have tweeted the tool</p>
		<div class="count"><?php echo $twitter; ?></div>
	</div>
</div>	