<?php
/* @var $this TagController */
/* @var $model Tag */

$this->breadcrumbs=array(
	_('Products')=>array('product/admin'),
	_('Tags'),
);

$this->menu=array(
	array('label'=>_('Create Tag'), 'url'=>array('create')),
);
?>

<h1><?php echo _('Manage Tags'); ?></h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tag-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'name',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{update}',
		),
	),
));