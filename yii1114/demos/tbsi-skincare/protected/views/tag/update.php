<?php
/* @var $this TagController */
/* @var $model Tag */

$this->breadcrumbs=array(
	_('Products')=>array('product/admin'),
	_('Tags')=>array('tag/admin'),
	$model->name=>array('view','id'=>$model->id),
	_('Update'),
);

$this->menu=array(
	array('label'=>_('View Tag'), 'url'=>array('view', 'id'=>$model->id)),
);
?>

<h1><?php echo _('Update Tag'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>