<?php
/* @var $this TagController */
/* @var $model Tag */

$this->breadcrumbs=array(
	_('Products')=>array('product/admin'),
	_('Tags')=>array('tag/admin'),
	$model->name,
);

$this->menu=array(
	array('label'=>_('Update Tag'), 'url'=>array('update', 'id'=>$model->id)),
);
?>

<h1><?php echo _('View Tag'); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'name',
	),
)); ?>
