<?php
/* @var $this QuestionController */
/* @var $model Question */

$this->breadcrumbs=array(
	_('Quizzes')=>array('quiz/admin'),
	$model->quiz->regimeName.' | '.$model->quiz->genderName=>array('quiz/view','id'=>$model->quiz->id),
	_('Update'),
);

$this->menu=array(
	array('label'=>_('Back to Quiz'), 'url'=>array('quiz/view','id'=>$model->quiz->id)),
);
?>

<h1><?php echo _('Update Question'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'quizModel'=>$model->quiz)); ?>