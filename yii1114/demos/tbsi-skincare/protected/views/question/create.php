<?php
/* @var $this QuestionController */
/* @var $model Question */

$this->breadcrumbs=array(
	_('Quizzes')=>array('quiz/admin'),
	$quizModel->regimeName.' | '.$quizModel->genderName=>array('quiz/view','id'=>$quizModel->id),
	_('Add Question'),
);

$this->menu=array(
	array('label'=>_('Back to Quiz'), 'url'=>array('quiz/view','id'=>$quizModel->id)),
);
?>

<h1><?php echo _('Add Question'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'quizModel'=>$quizModel)); ?>