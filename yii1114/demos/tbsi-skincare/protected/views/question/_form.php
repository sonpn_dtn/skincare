<?php
/* @var $this QuestionController */
/* @var $model Question */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'question-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo _('Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo $form->errorSummary($model); ?>
	<?php echo $form->hiddenField($model,'quizId',array('value'=>$quizModel->id)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->dropDownList($model,'type',array(null=>_('Please select')) + $model->types()); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="row" <?php if(!$model->typeArg) echo 'style="display: none;"'; ?>>
		<?php

		/* echo $form->labelEx(
			$model,
			Question::TYPE_MULTIPLE_SELECT_UP_TO,
			array(
				'data-type' => Question::TYPE_MULTIPLE_SELECT_UP_TO,
				'style' => ($model->type == Question::TYPE_MULTIPLE_SELECT_UP_TO ? '' : 'display: none;')
			)
		);

		echo $form->labelEx(
			$model,
			Question::TYPE_MULTIPLE_SELECT_EXACTLY,
			array(
				'data-type' => Question::TYPE_MULTIPLE_SELECT_EXACTLY,
				'style' => ($model->type == Question::TYPE_MULTIPLE_SELECT_EXACTLY ? '' : 'display: none;')
			)
		); */

		echo $form->labelEx(
			$model,
			Question::TYPE_MULTIPLE_SELECT_AT_LEAST,
			array(
				'data-type' => Question::TYPE_MULTIPLE_SELECT_AT_LEAST,
				'style' => ($model->type == Question::TYPE_MULTIPLE_SELECT_AT_LEAST ? '' : 'display: none;')
			)
		);

		?>
		<?php echo $form->textField($model,'typeArg'); ?>
		<?php echo $form->error($model,'typeArg'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'text'); ?>
		<?php echo $form->textArea($model,'text',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'text'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'didYouKnow'); ?>
		<?php echo $form->textArea($model,'didYouKnow',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'didYouKnow'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'breadcrumb'); ?>
		<?php echo $form->textField($model,'breadcrumb',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'breadcrumb'); ?>
	</div>

	<div class="row rememberMe">
		<?php echo $form->checkbox($model,'showBreadcrumb'); ?>
		<?php echo $form->labelEx($model,'showBreadcrumb'); ?>
		<?php echo $form->error($model,'showBreadcrumb'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'summaryText'); ?>
		<p class="hint"><?php echo _('Type the code %s to designate the position in the sentence to be replaced with the given answer to this question.'); ?></p>
		<?php echo $form->textField($model,'summaryText',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'summaryText'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',$model->statuses()); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'siteCatalyst_pageInfo'); ?>
		<?php echo $form->textField($model,'siteCatalyst_pageInfo',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'siteCatalyst_pageInfo'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? _('Create') : _('Save')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
