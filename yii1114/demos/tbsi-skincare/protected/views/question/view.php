<?php
/* @var $this QuestionController */
/* @var $model Question */

$this->breadcrumbs=array(
	_('Quizzes')=>array('quiz/admin'),
	$model->quiz->regimeName.' | '.$model->quiz->genderName=>array('quiz/view','id'=>$model->quiz->id),
	$model->title,
);

$this->menu=array(
	array('label'=>_('Update Question'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>_('Delete Question'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('zii','Are you sure you want to delete this item?'))),
	array('label'=>'<hr style="margin: 0;" />'),
	array('label'=>_('Add Answer'), 'url'=>array('answer/create','questionid'=>$model->id)),
	array('label'=>'<hr style="margin: 0;" />'),
	array('label'=>_('Back to Quiz'), 'url'=>array('quiz/view','id'=>$model->quiz->id)),
);
?>

<h1><?php echo _('View Question'); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		array(
			'name'=>'type',
			'value'=>str_replace("...", $model->typeArg, $model->typeName),
		),
		'title',
		'breadcrumb',
		'text',
		'summaryText',
		'didYouKnow',
		array(
			'name'=>'showBreadcrumb',
			'value'=>$model->showBreadcrumb ? _('Yes') : _('No'),
		),
		'statusName',
		'siteCatalyst_pageInfo',
	),
)); ?>

<h3 style="margin: 1em 0 0;"><?php echo _('Answers'); ?></h3>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'answer-grid',
	'dataProvider'=>$answerModel->search(),
	'htmlOptions'=>array('data-updateorderurl'=>$this->createUrl('answer/updateorder')),
	'rowHtmlOptionsExpression'=>'array("data-id"=>$data->id)',
	'template'=>'{items}',
	'columns'=>array(
		'text',
		'description',
		array(
			'name'=>'status',
			'value'=>'$data->statusName',
			'filter'=>$answerModel->statuses(),
		),
		array(
			'class'=>'CButtonColumn',
			'viewButtonUrl'=>'Yii::app()->urlManager->createUrl("answer/view", array("id"=>$data->id))',
			'updateButtonUrl'=>'Yii::app()->urlManager->createUrl("answer/update", array("id"=>$data->id))',
			'deleteButtonUrl'=>'Yii::app()->urlManager->createUrl("answer/delete", array("id"=>$data->id))',
		),
	),
)); ?>
