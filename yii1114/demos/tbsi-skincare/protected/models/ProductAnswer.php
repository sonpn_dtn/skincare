<?php

/**
 * This is the model class for table "product_answer".
 *
 * The followings are the available columns in table 'product_answer':
 * @property string $productId
 * @property string $answerId
 * @property integer $weight
 */
class ProductAnswer extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'product_answer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('productId, answerId, weight', 'required'),
			array('answerId','uniqueCompositeKey','on'=>'insert'),
			array('weight', 'numerical', 'integerOnly'=>true),
			array('productId, answerId', 'length', 'max'=>10),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'product' => array(self::HAS_ONE, 'Product', 'id'),
			'answer' => array(self::HAS_ONE, 'Answer', array('id'=>'answerId')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'productId' => _('Product'),
			'answerId' => _('Answer'),
			'weight' => _('Weight'),
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductAnswer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Validate unique composite key
	 */
	public function uniqueCompositeKey($attribute, $params)
	{
		if(!$this->hasErrors())
			if($this->findByAttributes(array('answerId'=>$this->answerId,'productId'=>$this->productId)))
				$this->addError($attribute,_('The product you tried to add is already related to this answer.'));
	}
}
