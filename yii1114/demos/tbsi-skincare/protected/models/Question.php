<?php

/**
 * This is the model class for table "question".
 *
 * The followings are the available columns in table 'question':
 * @property string $id
 * @property string $quizId
 * @property integer $type
 * @property integer $typeArg
 * @property string $title
 * @property string $breadcrumb
 * @property string $text
 * @property string $summaryText
 * @property string $didYouKnow
 * @property integer $showBreadcrumb
 * @property string $order
 * @property integer $status
 * @property string $created
 * @property string $siteCatalyst_pageInfo
 *
 * @property string $typeName
 * @property string $statusName
 *
 * The followings are the available model relations:
 * @property Answer[] $answers
 * @property Quiz $quiz
 * @property Didyouknow $didyouknow
 */
class Question extends ActiveRecord
{
	public $nonTranslatables = array('id', 'quizId', 'type', 'typeArg', 'showBreadcrumb', 'order', 'status', 'created', 'siteCatalyst_pageInfo');
	public $translatables = array('title', 'breadcrumb', 'text', 'summaryText', 'didYouKnow');

	const TYPE_SINGLE_SELECT = 1;
	// const TYPE_MULTIPLE_SELECT_UP_TO = 2;
	// const TYPE_MULTIPLE_SELECT_EXACTLY = 3;
	const TYPE_MULTIPLE_SELECT_AT_LEAST = 4;

	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 2;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'question';
	}

	/**
	 * Returns the default named scope that should be implicitly applied to all queries for this model.
	 * Note, default scope only applies to SELECT queries. It is ignored for INSERT, UPDATE and DELETE queries.
	 * The default implementation simply returns an empty array. You may override this method
	 * if the model needs to be queried with some default criteria (e.g. only active records should be returned).
	 * @return array the query criteria. This will be used as the parameter to the constructor
	 * of {@link CDbCriteria}.
	 */
	public function defaultScope()
	{
		return array(
			'order'=>$this->getTableAlias(false, false).'.`order`',
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('quizId, type, title, breadcrumb, text', 'required'),
			array('type, typeArg, showBreadcrumb, status', 'numerical', 'integerOnly'=>true),
			array('quizId, order', 'length', 'max'=>10),
			array('title, breadcrumb, summaryText, siteCatalyst_pageInfo', 'length', 'max'=>255),
			array('didYouKnow', 'safe'),

			// The following rule is used by search().
			array('quizId', 'safe', 'on'=>'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'answers' => array(self::HAS_MANY, 'Answer', 'questionId'),
			'quiz' => array(self::BELONGS_TO, 'Quiz', 'quizId'),

			'activeAnswers' => array(self::HAS_MANY, 'Answer', 'questionId', 'on'=>'activeAnswers.status = '.Answer::STATUS_ACTIVE),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => _('ID'),
			'quizId' => _('Quiz'),
			'type' => _('Type'),
			'typeArg' => 'Type Arg',
			'title' => _('Title'),
			'breadcrumb' => _('Breadcrumb'),
			'text' => _('Text'),
			'summaryText' => _('Summary Text'),
			'didYouKnow' => _('Did You Know'),
			'showBreadcrumb' => _('Show Breadcrumb'),
			'order' => _('Order'),
			'status' => _('Status'),
			'created' => _('Created'),
			'siteCatalyst_pageInfo' => _('Site Catalyst: Page Info'),

			'typeName' => _('Type'),
			'statusName' => _('Status'),

			// self::TYPE_MULTIPLE_SELECT_UP_TO => _('Maximum number of selections'),
			// self::TYPE_MULTIPLE_SELECT_EXACTLY => _('Exact number of selections'),
			self::TYPE_MULTIPLE_SELECT_AT_LEAST => _('Minimum number of selections'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('quizId',$this->quizId);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Question the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return array type values and names
	 */
	public function types()
	{
		return array(
			self::TYPE_SINGLE_SELECT => _('Single-select'),
			// self::TYPE_MULTIPLE_SELECT_UP_TO => _('Multiple-select (up to ...)'),
			// self::TYPE_MULTIPLE_SELECT_EXACTLY => _('Multiple-select (exactly ...)'),
			self::TYPE_MULTIPLE_SELECT_AT_LEAST => _('Multiple-select (at least ...)'),
		);
	}
	
	/**
	 * @return string a human-readable name for the type
	 */
	public function getTypeName()
	{
		if($this->type)
		{
			$types = $this->types();
			return $types[$this->type];
		}
	}

	/**
	 * @return array status values and names
	 */
	public function statuses()
	{
		return array(
			self::STATUS_ACTIVE => _('Active'),
			self::STATUS_INACTIVE => _('Inactive'),
		);
	}
	
	/**
	 * @return string a human-readable name for the status
	 */
	public function getStatusName()
	{
		if($this->status)
		{
			$statuses = $this->statuses();
			return $statuses[$this->status];
		}
	}

	/**
	 * @return Question the next question model
	 */
	public function getNextQuestion()
	{
		$questions = $this->quiz->questions;

		while(list($index, $question) = each($questions))
			if($this->id == $question->id)
				return current($questions);

		return false;
	}

	/**
	 * @return array answer IDs for the user has answering this question
	 */
	public function getAnswerIdsByUser($userId)
	{
		return Yii::app()->db->createCommand(
		   "SELECT `answer`.`id`
			FROM `question`
			JOIN `answer` ON `question`.`id` = `answer`.`questionId`
			JOIN `answer_user` ON `answer`.`id` = `answer_user`.`answerId` AND `answer_user`.`userId` = $userId
			WHERE `question`.`id` = ".$this->id
		)->queryColumn();
	}

	/**
	 * @return string Randomly pick a Did You Know from the database
	 */
	public function getRandomDidYouKnow($limit)
	{
		return Yii::app()->db->createCommand(
		   "SELECT DISTINCT `question`.`didYouKnow`
			FROM `question`
			ORDER BY RAND();
			LIMIT $limit;"
		)->queryColumn();
	}
}
