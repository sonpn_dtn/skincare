<?php

/**
 * This is the model class for table "quiz".
 *
 * The followings are the available columns in table 'quiz':
 * @property string $id
 * @property integer $regime
 * @property integer $gender
 * @property integer $status
 * @property string $created
 * @property string $siteCatalyst_subCategoryKey
 * @property string $siteCatalyst_subCategoryValue
 *
 * @property string $regimeName
 * @property string $genderName
 * @property string $statusName
 *
 * The followings are the available model relations:
 * @property Question[] $questions
 *
 * @property Question[] $breadcrumbs
 */
class Quiz extends CActiveRecord
{
	const REGIME_EXPRESS = 1;
	const REGIME_COMPLETE = 2;

	const GENDER_MALE = 2;
	const GENDER_FEMALE = 3;

	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 2;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'quiz';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('regime, gender, status', 'numerical', 'integerOnly'=>true),
			array('siteCatalyst_subCategoryKey, siteCatalyst_subCategoryValue', 'length', 'max'=>255),

			// The following rule is used by search().
			array('regime, gender, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'questions' => array(self::HAS_MANY, 'Question', 'quizId'),

			'breadcrumbs' => array(self::HAS_MANY, 'Question', 'quizId', 'on'=>'breadcrumbs.showBreadcrumb = 1'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => _('ID'),
			'regime' => _('Regime'),
			'gender' => _('Gender'),
			'status' => _('Status'),
			'created' => _('Created'),
			'siteCatalyst_subCategoryKey' => _('Site Catalyst: Sub-Category Key'),
			'siteCatalyst_subCategoryValue' => _('Site Catalyst: Sub-Category Value'),

			'regimeName' => _('Status'),
			'genderName' => _('Gender'),
			'statusName' => _('Status'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('regime',$this->regime);
		$criteria->compare('gender',$this->gender);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Quiz the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return array regime values and names
	 */
	public function regimes()
	{
		return array(
			self::REGIME_EXPRESS => _('Express'),
			self::REGIME_COMPLETE => _('Complete'),
		);
	}
	
	/**
	 * @return string a human-readable name for the regime
	 */
	public function getRegimeName()
	{
		if($this->regime)
		{
			$regimes = $this->regimes();
			return $regimes[$this->regime];
		}
	}

	/**
	 * @return array gender values and names
	 */
	public function genders()
	{
		return array(
			self::GENDER_MALE => _('Male'),
			self::GENDER_FEMALE => _('Female'),
		);
	}
	
	/**
	 * @return string a human-readable name for the gender
	 */
	public function getGenderName()
	{
		if($this->gender)
		{
			$genders = $this->genders();
			return $genders[$this->gender];
		}
	}

	/**
	 * @return array status values and names
	 */
	public function statuses()
	{
		return array(
			self::STATUS_ACTIVE => _('Active'),
			self::STATUS_INACTIVE => _('Inactive'),
		);
	}
	
	/**
	 * @return string a human-readable name for the status
	 */
	public function getStatusName()
	{
		if($this->status)
		{
			$statuses = $this->statuses();
			return $statuses[$this->status];
		}
	}
}
