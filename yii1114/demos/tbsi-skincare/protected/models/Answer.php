<?php

/**
 * This is the model class for table "answer".
 *
 * The followings are the available columns in table 'answer':
 * @property string $id
 * @property string $questionId
 * @property string $goToQuestionId
 * @property integer $skipToEnd
 * @property integer $unique
 * @property string $text
 * @property string $summaryText
 * @property string $description
 * @property integer $order
 * @property integer $status
 * @property string $created
 *
 * @property string $statusName
 *
 * The followings are the available model relations:
 * @property Question $goToQuestion
 * @property Question $question
 * @property User[] $users
 * @property Product[] $products
 */
class Answer extends ActiveRecord
{
	public $nonTranslatables = array('id', 'questionId', 'goToQuestionId', 'skipToEnd', 'unique', 'order', 'status', 'created');
	public $translatables = array('text', 'summaryText', 'description');

	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 2;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'answer';
	}

	/**
	 * Returns the default named scope that should be implicitly applied to all queries for this model.
	 * Note, default scope only applies to SELECT queries. It is ignored for INSERT, UPDATE and DELETE queries.
	 * The default implementation simply returns an empty array. You may override this method
	 * if the model needs to be queried with some default criteria (e.g. only active records should be returned).
	 * @return array the query criteria. This will be used as the parameter to the constructor
	 * of {@link CDbCriteria}.
	 */
	public function defaultScope()
	{
		return array(
			'order'=>$this->getTableAlias(false, false).'.`order`',
		);
	}
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('questionId, text', 'required'),
			array('skipToEnd, unique, order, status', 'numerical', 'integerOnly'=>true),
			array('questionId, goToQuestionId, order', 'length', 'max'=>10),
			array('text, summaryText', 'length', 'max'=>255),
			array('description', 'safe'),

			// The following rule is used by search().
			array('questionId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'goToQuestion' => array(self::BELONGS_TO, 'Question', 'goToQuestionId'),
			'question' => array(self::BELONGS_TO, 'Question', 'questionId'),
			'users' => array(self::MANY_MANY, 'User', 'answer_user(answerId, userId)'),
			'products' => array(self::MANY_MANY, 'Product', 'product_answer(answerId, productId)'),

			'productAnswers' => array(self::HAS_MANY, 'ProductAnswer', 'answerId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => _('ID'),
			'questionId' => _('Question'),
			'goToQuestionId' => _('Go To Question'),
			'skipToEnd' => _('Skip to End'),
			'unique' => _('Unique'),
			'text' => _('Text'),
			'summaryText' => _('Summary Text'),
			'description' => _('Description'),
			'order' => _('Order'),
			'status' => _('Status'),
			'created' => _('Created'),

			'statusName' => _('Status'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('questionId',$this->questionId);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Answer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return array status values and names
	 */
	public function statuses()
	{
		return array(
			self::STATUS_ACTIVE => _('Active'),
			self::STATUS_INACTIVE => _('Inactive'),
		);
	}
	
	/**
	 * @return string a human-readable name for the status
	 */
	public function getStatusName()
	{
		if($this->status)
		{
			$statuses = $this->statuses();
			return $statuses[$this->status];
		}
	}

	/**
	 * @return Answer the Answer that was given for the passed question/user ID
	 */
	public function findByQuestionUser($questionId, $userId)
	{
		return $this->findBySql(
		   "SELECT `answer`.*
			FROM `answer`
			JOIN `question` ON `answer`.`questionId` = `question`.`id` AND `question`.`id` = $questionId
			JOIN `answer_user` ON `answer`.`id` = `answer_user`.`answerId` AND `answer_user`.`userId` = $userId"
		);
	}

	/**
	 * @return array the array of Answer that was given for the passed question/user ID
	 */
	public function findAllByQuestionUser($questionId, $userId)
	{
		return $this->findAllBySql(
		   "SELECT `answer`.*
			FROM `answer`
			JOIN `question` ON `answer`.`questionId` = `question`.`id` AND `question`.`id` = $questionId
			JOIN `answer_user` ON `answer`.`id` = `answer_user`.`answerId` AND `answer_user`.`userId` = $userId"
		);
	}
}