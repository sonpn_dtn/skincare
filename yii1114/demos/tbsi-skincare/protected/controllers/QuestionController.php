<?php

class QuestionController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('view'),
				'users'=>array('*'),
			),
			array('allow',
				'actions'=>array('create','update','delete','updateorder'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		$this->siteCatalyst_pageInfo = $model->siteCatalyst_pageInfo;
		$this->siteCatalyst_subCategoryKey = $model->quiz->siteCatalyst_subCategoryKey;
		$this->siteCatalyst_subCategoryValue = $model->quiz->siteCatalyst_subCategoryValue;

		// Admin
		if(isset($_GET['r']))
		{
			$answerModel = new Answer('search');
			$answerModel->unsetAttributes();  // clear any default values
			$answerModel->questionId = $id;

			$this->render('view', array(
				'model'=>$model,
				'answerModel'=>$answerModel,
			));
		}

		// Public
		else
		{
			if(!empty($_POST['AnswerUser']['answerId']))
			{
				$answer = Answer::model()->findByPk($_POST['AnswerUser']['answerId']);
				$nextQuestion = array();
				$skipToEnd = false;

				// Delete the users answers for this question, and all questions after
				$futureQuestions = Question::model()->findAll('`quizId` = '.$answer->question->quiz->id.' AND `order` >= '.$answer->question->order);
				$possibleAnswers = array();

				foreach($futureQuestions as $question)
					foreach($question->answers as $possibleAnswer)
						$possibleAnswers[] = $possibleAnswer->id;

				AnswerUser::model()->deleteAllByAttributes(array('userId' => Yii::app()->request->getParam('userId')), 'answerId IN ('.implode(',', $possibleAnswers).')');

				// Save new answers
				$answerIds = explode(',', $_POST['AnswerUser']['answerId']);

				foreach($answerIds as $answerId)
				{
					$answerUserModel = new AnswerUser;
					$answerUserModel->userId = Yii::app()->request->getParam('userId');
					$answerUserModel->answerId = $answerId;					
					$answerUserModel->save();
				}

				// If there is an existing decision tree, follow it
				if(Yii::app()->request->getParam('nextQuestion'))
					$nextQuestion = explode(',', Yii::app()->request->getParam('nextQuestion'));

				// If this is a multiple-choice question, check to see if a question pathway needs to be built
				if(empty($nextQuestion) && $answer->question->type == Question::TYPE_MULTIPLE_SELECT_AT_LEAST)
				{
					foreach($answerIds as $answerId)
					{
						$answer = Answer::model()->findByPk($answerId);
						
						if($answer->goToQuestionId)
							$nextQuestion[] = $answer->goToQuestionId;
						elseif($answer->skipToEnd)
							$skipToEnd = true;
					}

					sort($nextQuestion);

					if($skipToEnd)
						$nextQuestion[] = 'end';
				}

				// If there is no question pathway at this point, default to regular decision functionality
				if(empty($nextQuestion))
				{
					// Check to see if this answer directs to another question
					if($answer->goToQuestionId)
						$nextQuestion = $answer->goToQuestionId;

					elseif($answer->skipToEnd)
						$nextQuestion = 'end';

					// Otherwise, parse the next question
					elseif($nextQuestion = $answer->question->nextQuestion)
						$nextQuestion = $nextQuestion->id;
				}

				// Judgement, do we have a question pathway, do we jump to the next question or are we finished?
				if(!empty($nextQuestion) && is_array($nextQuestion))
				{
					$nextQuestionId = array_shift($nextQuestion);

					if($nextQuestionId != 'end')
						$this->redirect(array('view',
							'id' => $nextQuestionId,
							'userId' => Yii::app()->request->getParam('userId'),
							'nextQuestion' => implode(',', $nextQuestion),
						));
				}
				elseif(!empty($nextQuestion) && $nextQuestion != 'end')
					$this->redirect(array('view',
						'id' => $nextQuestion,
						'userId' => Yii::app()->request->getParam('userId'),
					));

				$this->redirect(array('site/summary',
					'userId' => Yii::app()->request->getParam('userId'),
				));
			}

			$this->render('view', array(
				'model'=>$model,
				'answerUserModel'=>new AnswerUser,
				'questionCount'=>count($model->quiz->breadcrumbs),
				'pastCurrent'=>false,
			));
		}
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @param integer $quizid the ID of the quiz
	 */
	public function actionCreate($quizid)
	{
		$model=new Question;
		$quizModel=Quiz::model()->findByPk($quizid);

		if(isset($_POST['Question']))
		{
			$model->attributes=$_POST['Question'];
			$model->order=count($quizModel->questions) + 1;

			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
			'quizModel'=>$quizModel,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		if(isset($_POST['Question']))
		{
			$model->attributes=$_POST['Question'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model=$this->loadModel($id);
		$quizId=$model->quizId;
		$model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('quiz/view','id'=>$quizId));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Question the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Question::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Update question order
	 */
	public function actionUpdateorder()
	{
		foreach($_POST as $order => $questionId)
		{
			$question = Question::model()->findByPk($questionId);
			$question->order = $order;
			$question->save();
		}
	}
}
