<?php

class ProductController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('view','create','update','admin','delete','search','bulkupload','scrapepricestock'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);

		/* Update variant stock
		foreach(Region::model()->findAll() as $region)
			$model->updatePriceStock($region->languageCode.'_'.$region->countryCode); */

		$this->render('view', array(
			'model' => $model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Product;

		if(isset($_POST['Product']))
		{
			$model->attributes=$_POST['Product'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		if(isset($_POST['Product']))
		{
			$model->attributes=$_POST['Product'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Product('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Product']))
			$model->attributes=$_GET['Product'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Product the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Product::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * JSON search used by the answer controller
	 */
	public function actionSearch()
	{
		header('Content-type: application/json');

		$json=array();
		$products=Product::model()->findAll("code LIKE '%".Yii::app()->request->getParam('term')."%' OR name LIKE '%".Yii::app()->request->getParam('term')."%'");

		if($products)
			foreach($products as $product)
				$json[]=array(
					'id'=>$product->id,
					'label'=>$product->code.': '.$product->name,
					'value'=>$product->code.': '.$product->name
				);

		echo json_encode($json);
	}

	/**
	 * Takes product information in the form of 4 CSV uploads and imports it in to the relational database structure
	 */
	public function actionBulkupload()
	{
		$model = new BulkUploadForm;

		// Process upload
		if(isset($_POST['BulkUploadForm']))
		{
			$model->attributes = $_POST['BulkUploadForm'];

			// $model->categoryCsv = CUploadedFile::getInstance($model, 'categoryCsv');
			// $model->categoryProductCsv = CUploadedFile::getInstance($model, 'categoryProductCsv');
			$model->productCsv = CUploadedFile::getInstance($model, 'productCsv');
			$model->productReferenceCsv = CUploadedFile::getInstance($model, 'productReferenceCsv');

			if($model->validate())
			{
				// $model->processCsvUpload('categoryCsv', $model->categoryCsvFirstLine, $model->categoryCsvSecondLine);
				// $model->processCsvUpload('categoryProductCsv', $model->categoryProductCsvFirstLine, $model->categoryProductCsvSecondLine);
				// $model->processCsvUpload('productCsv', $model->productCsvFirstLine, $model->productCsvSecondLine);
				// $model->processCsvUpload('productReferenceCsv', $model->productReferenceCsvFirstLine, $model->productReferenceCsvSecondLine);

				// Clear out previous upload
				Category::model()->updateAll(array('parentId' => null));
				Category::model()->deleteAll();
				Yii::app()->db->createCommand('DELETE FROM `product` WHERE `id` NOT IN (SELECT DISTINCT `productId` FROM `category_product`);')->execute();

				// $model->processCategoryCsvUpload();
				$model->processProductCsvUpload();
				// $model->processCategoryProductCsvUpload();
				$model->processProductReferenceCsvUpload();

				Yii::app()->user->setFlash('message',_('Successfully uploaded products via CSV.'));
				$this->redirect(array('product/admin'));
			}
		}

		$this->render('bulkupload',array(
			'model' => $model,
		));
	}

	/**
	 * Scrapes price and stock levels from The Body Shop API
	 */
	public function actionScrapepricestock()
	{
		foreach(Region::model()->findAll() as $region)
			foreach(Product::model()->findAll() as $product)
				$product->updatePriceStock($region->languageCode.'_'.$region->countryCode);

		Yii::app()->user->setFlash('message',_('Successfully scraped product prices and stock levels.'));
		$this->redirect(array('product/admin'));
	}
}
