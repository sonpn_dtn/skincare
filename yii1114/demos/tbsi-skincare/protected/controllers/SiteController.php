<?php

class SiteController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index','summary','result','error','login'),
				'users'=>array('*'),
			),
			array('allow',
				'actions'=>array('logout','loco'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		/* if(Yii::app()->request->getParam('userId'))
			$user = User::model()->findByPk(Yii::app()->request->getParam('userId'));

		if(!$user) */

		$this->canonical = false;
		$user = new User;

		if(isset($_POST['User']))
		{
			$user->attributes = $_POST['User'];

			// Create 15 character random string for cookie
			$characterList = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*?';
			$i = 0;
			$salt = '';
			while($i < 15)
			{
				$salt .= $characterList{mt_rand(0, (strlen($characterList) - 1))};
				$i++;
			}

			if($user->validate())
			{
				// Find the quiz based on the user selsection
				$quiz = Quiz::model()->findByAttributes(array(
					'regime' => $user->regime,
					'gender' => $user->gender,
					'status' => Quiz::STATUS_ACTIVE,
				));

				$user->quizId = $quiz->id;

				if($user->save(false))
				{
					// Store cookie on machine
					setcookie($user->id, $salt, time() + 3600 * 8765);
				
					// Set same cookie in database and save user
					$user->cookiemonster = $salt;
					$user->save(false);

					$this->redirect(array('question/view',
						'id' => $quiz->questions[0]->id,
						'userId' => $user->id,
					));
				}
			}
		}
		else
		{
			$user->name = Yii::app()->request->getParam('User_name');
			$user->gender = Yii::app()->request->getParam('User_gender');
		}

		$this->render('index', array(
			'user' => $user
		));
	}

	/**
	 * The summary screen
	 */
	public function actionSummary()
	{
		$user = User::model()->findByPk(Yii::app()->request->getParam('userId'));
		$this->siteCatalyst_pageInfo = 'Skin Diagnostic Summary';
		$this->siteCatalyst_subCategoryKey = $user->quiz->siteCatalyst_subCategoryKey;
		$this->siteCatalyst_subCategoryValue = $user->quiz->siteCatalyst_subCategoryValue;

		// Cookie redirect
		if($_SERVER['SERVER_NAME'] == 'skincareonline.thebodyshop.co.uk' && (!isset($_COOKIE[$user->id]) || ($_COOKIE[$user->id]!= $user->cookiemonster)))
			$this->redirect('index');

		$this->render('summary',array(
			'user'=>$user,
			'questionCount'=>count($user->quiz->breadcrumbs),
		));
	}

	/**
	 * The result screen
	 */
	public function actionResult()
	{
		$user = User::model()->findByPk(Yii::app()->request->getParam('userId'));
		$this->siteCatalyst_pageInfo = 'Skin Diagnostic Result';
		$this->siteCatalyst_subCategoryKey = $user->quiz->siteCatalyst_subCategoryKey;
		$this->siteCatalyst_subCategoryValue = $user->quiz->siteCatalyst_subCategoryValue;
		
		// Cookie redirect
		if($_SERVER['SERVER_NAME'] == 'skincareonline.thebodyshop.co.uk' && (!isset($_COOKIE[$user->id]) || ($_COOKIE[$user->id]!= $user->cookiemonster)))
			$this->redirect('index');

		$emailSuccess = false;
		$productRanges = array();

		// ALTERNATIVE AVERAGING: SELECT `product`.*, AVG(`product_answer`.`weight`) AS 'score'
		$results = Product::model()->findAllBySql(
		   "SELECT
				`product`.*, SUM(`product_answer`.`weight`) / ".$user->answerCount." AS 'score'
			FROM
				`user`
				JOIN `answer_user` ON `user`.`id` = `answer_user`.`userId`
				JOIN `answer` ON `answer_user`.`answerId` = `answer`.`id`
				JOIN `product_answer` ON `answer`.`id` = `product_answer`.`answerId`
				JOIN `product` ON `product_answer`.`productId` = `product`.`id`
					AND `product`.`status` = ".Product::STATUS_ACTIVE."
					AND (`product`.`gender` = ".Product::GENDER_NEUTRAL." OR `product`.`gender` = ".$user->quiz->gender.")
				JOIN `productVariant` ON `product`.`id` = `productVariant`.`productId`
					AND `productVariant`.`status` = ".ProductVariant::STATUS_ACTIVE."
					AND `productVariant`.`price` > 0
					AND `productVariant`.`stock` > 0
			WHERE `user`.`id` = ".$user->id."
			GROUP BY `product_answer`.`productId`
			HAVING `score` > 0
			ORDER BY `score` DESC
			LIMIT 20;"
		);

		$resultSheet = array(
			Tag::model()->ID_TONE => array(
				Product::DAY_NIGHT_DAY => null,
				Product::DAY_NIGHT_NIGHT => null,
			),
			Tag::model()->ID_CLEANSE => array(
				Product::DAY_NIGHT_DAY => null,
				Product::DAY_NIGHT_NIGHT => null,
			),
			Tag::model()->ID_MOISTURISE => array(
				Product::DAY_NIGHT_DAY => null,
				Product::DAY_NIGHT_NIGHT => null,
			)
		);

		// First pass: look for results that have a tag and day/night set and move them to the result sheet
		foreach($results as $index => $product)
			/* Tone/Day
			if($product->tagId == Tag::model()->ID_TONE && $product->dayNight == Product::DAY_NIGHT_DAY && !$resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_DAY])
			{
				$resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_DAY] = $product;

				if($product->productRange->fileName)
					$productRanges[$product->productRange->id] = $product->productRange;

				unset($results[$index]);
			} */
			
			// Tone/Night
			if($product->tagId == Tag::model()->ID_TONE && $product->dayNight == Product::DAY_NIGHT_NIGHT && !$resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_NIGHT])
			{
				
				$resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_NIGHT] = $product;

				if($product->productRange->fileName)
					$productRanges[$product->productRange->id] = $product->productRange;

				unset($results[$index]);
			}

			/* Cleanse/Day
			elseif($product->tagId == Tag::model()->ID_CLEANSE && $product->dayNight == Product::DAY_NIGHT_DAY && !$resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_DAY])
			{
				$resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_DAY] = $product;
				unset($results[$index]);
			} */

			// Cleanse/Night
			elseif($product->tagId == Tag::model()->ID_CLEANSE && $product->dayNight == Product::DAY_NIGHT_NIGHT && !$resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_NIGHT])
			{
				$resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_NIGHT] = $product;

				if($product->productRange->fileName)
					$productRanges[$product->productRange->id] = $product->productRange;

				unset($results[$index]);
			}

			/* Moisturise/Day
			elseif($product->tagId == Tag::model()->ID_MOISTURISE && $product->dayNight == Product::DAY_NIGHT_DAY && !$resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_DAY])
			{
				$resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_DAY] = $product;

				if($product->productRange->fileName)
					$productRanges[$product->productRange->id] = $product->productRange;

				unset($results[$index]);
			} */

			// Moisturise/Night
			elseif($product->tagId == Tag::model()->ID_MOISTURISE && $product->dayNight == Product::DAY_NIGHT_NIGHT && !$resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_NIGHT])
			{
				$resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_NIGHT] = $product;

				if($product->productRange->fileName)
					$productRanges[$product->productRange->id] = $product->productRange;

				unset($results[$index]);
			}

		// Second pass: look for products that have just a tag set and move them to the result sheet
		foreach($results as $index => $product)
			// Tone/Day
			if($product->tagId == Tag::model()->ID_TONE && !$resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_DAY])
			{
				$resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_DAY] = $product;

				if($product->productRange->fileName)
					$productRanges[$product->productRange->id] = $product->productRange;

				unset($results[$index]);
			}
			
			/* Tone/Night
			elseif($product->tagId == Tag::model()->ID_TONE && !$resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_NIGHT])
			{
				
				$resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_NIGHT] = $product;

				if($product->productRange->fileName)
					$productRanges[$product->productRange->id] = $product->productRange;

				unset($results[$index]);
			} */

			// Cleanse/Day
			elseif($product->tagId == Tag::model()->ID_CLEANSE && !$resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_DAY])
			{
				$resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_DAY] = $product;

				if($product->productRange->fileName)
					$productRanges[$product->productRange->id] = $product->productRange;

				unset($results[$index]);
			}

			/* Cleanse/Night
			elseif($product->tagId == Tag::model()->ID_CLEANSE && !$resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_NIGHT])
			{
				$resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_NIGHT] = $product;

				if($product->productRange->fileName)
					$productRanges[$product->productRange->id] = $product->productRange;

				unset($results[$index]);
			} */

			// Moisturise/Day
			elseif($product->tagId == Tag::model()->ID_MOISTURISE && !$resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_DAY])
			{
				$resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_DAY] = $product;
				unset($results[$index]);
			}

			/* Moisturise/Night
			elseif($product->tagId == Tag::model()->ID_MOISTURISE && !$resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_NIGHT])
			{
				$resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_NIGHT] = $product;

				if($product->productRange->fileName)
					$productRanges[$product->productRange->id] = $product->productRange;

				unset($results[$index]);
			} */

		// Third pass: look for products that have just a day/night set and move them to the result sheet
		foreach($results as $index => $product)
			// Tone/Day
			if($product->dayNight == Product::DAY_NIGHT_DAY && !$resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_DAY])
			{
				$resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_DAY] = $product;

				if($product->productRange->fileName)
					$productRanges[$product->productRange->id] = $product->productRange;

				unset($results[$index]);
			}

			// Cleanse/Day
			elseif($product->dayNight == Product::DAY_NIGHT_DAY && !$resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_DAY])
			{
				$resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_DAY] = $product;

				if($product->productRange->fileName)
					$productRanges[$product->productRange->id] = $product->productRange;

				unset($results[$index]);
			}

			// Moisturise/Day
			elseif($product->dayNight == Product::DAY_NIGHT_DAY && !$resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_DAY])
			{
				$resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_DAY] = $product;

				if($product->productRange->fileName)
					$productRanges[$product->productRange->id] = $product->productRange;

				unset($results[$index]);
			}
			
			/* Tone/Night
			elseif($product->dayNight == Product::DAY_NIGHT_NIGHT && !$resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_NIGHT])
			{
				
				$resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_NIGHT] = $product;

				if($product->productRange->fileName)
					$productRanges[$product->productRange->id] = $product->productRange;

				unset($results[$index]);
			}

			// Cleanse/Night
			elseif($product->dayNight == Product::DAY_NIGHT_NIGHT && !$resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_NIGHT])
			{
				$resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_NIGHT] = $product;

				if($product->productRange->fileName)
					$productRanges[$product->productRange->id] = $product->productRange;

				unset($results[$index]);
			}

			// Moisturise/Night
			elseif($product->dayNight == Product::DAY_NIGHT_NIGHT && !$resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_NIGHT])
			{
				$resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_NIGHT] = $product;

				if($product->productRange->fileName)
					$productRanges[$product->productRange->id] = $product->productRange;

				unset($results[$index]);
			} */

		// Fourth pass: move all remaining products in to the day tab where there are spaces
		reset($results);

		foreach($resultSheet as $tagId => $tagResults)
			if(!$resultSheet[$tagId][Product::DAY_NIGHT_DAY])
				$resultSheet[$tagId][Product::DAY_NIGHT_DAY] = array_shift($results);

		// Cleanse the results
		$allDayTagList = array();
		foreach($results as $index => $result)
		{
			if(
				$result->tagId == Tag::model()->ID_MOISTURISE ||
				$result->tagId == Tag::model()->ID_TONE ||
				($result->tagId == Tag::model()->ID_CLEANSE && $result->productRangeId != ProductRange::model()->ID_CAMOMILE) ||
				$result->dayNight == Product::DAY_NIGHT_NIGHT ||
				!empty($allDayTagList[$result->tagId])
			)
				unset($results[$index]);
			elseif($result->productRange->fileName)
				$productRanges[$result->productRange->id] = $result->productRange;

			$allDayTagList[$result->tagId] = true;
		}

		$results = array_splice($results, 0, 3); // This will have left over products that don't fit in to the result sheet
		$productRanges = array_splice($productRanges, 0, 3);

		// Send email factsheet
		if(isset($_POST['User']))
		{
			$user->attributes=$_POST['User'];
			if($user->save())
			{
				if(Yii::app()->mailEx->pear(
					$user->name,
					$user->email,
					CString::_('your-skincare-summary'),
					$this->renderPartial('email',array(
						'user'=>$user,
						'resultSheet'=>$resultSheet,
						'results'=>$results,
						'dropsOfYouth'=>Product::model()->findByPk(Settings::model()->find()->heroProductId),
						'didYouKnow'=>Question::model()->getRandomDidYouKnow(3),
						'productRanges'=>$productRanges,
					),true)
				) === true)
					$emailSuccess = true;
			}
		}

		// Protect user information
		else
		{
			$user->email = null;
			$user->newsletter = null;
		}

		if(Yii::app()->request->getParam('print') == 'true')
			$this->bodyId = 'site-print';

		$this->render(Yii::app()->request->getParam('print') == 'true' ? 'print' : 'result',array(
			'user'=>$user,
			'emailSuccess'=>$emailSuccess,
			'resultSheet'=>$resultSheet,
			'results'=>$results,
			'dropsOfYouth'=>Product::model()->findByPk(Settings::model()->find()->heroProductId),
			'didYouKnow'=>Question::model()->getRandomDidYouKnow(2),
			'nightTabExists'=>false,
			'productRanges'=>$productRanges,
		));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
			$this->render('error', $error);
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];

			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->baseUrl.'/?r=help/page&view=index');
		}

		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->baseUrl.'/?r');
	}

	/**
	 * Updates the PO/MO translation files from Loco
	 */
	public function actionLoco()
	{
		foreach($this->regions as $region)
		{
			$po = file_get_contents('https://localise.biz/api/export/locale/'.$region->languageCode.'_'.$region->countryCode.'.po?key=2084b9d912eff16103901dd6af2508b1&index=id');
			file_put_contents('locales/'.$region->languageCode.'_'.$region->countryCode.'/LC_MESSAGES/the-body-shop-international.po', $po);

			$mo = file_get_contents('https://localise.biz/api/export/locale/'.$region->languageCode.'_'.$region->countryCode.'.mo?key=2084b9d912eff16103901dd6af2508b1&index=id');
			file_put_contents('locales/'.$region->languageCode.'_'.$region->countryCode.'/LC_MESSAGES/the-body-shop-international.mo', $mo);
		}

		Yii::app()->user->setFlash('message',_('Successfully updated localisation files.'));
		$this->redirect(array('quiz/admin'));
	}
}
