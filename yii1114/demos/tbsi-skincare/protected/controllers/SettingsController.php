<?php

class SettingsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('css'),
				'users'=>array('*'),
			),
			array('allow',
				'actions'=>array('index'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$settings = Settings::model()->find();

		if(!empty($_POST))
		{
			$settings->unsetAttributes();

			if(!empty($_POST['hero-product-id']))			
				$settings->heroProductId = $_POST['hero-product-id'];

			if(!empty($_POST['primary-colour']))			
				$settings->primaryColour = $_POST['primary-colour'];

			if(!empty($_POST['body-size']))			
				$settings->bodySize = $_POST['body-size'];			

			if(!empty($_POST['app-shading']))
				$settings->appShading = CString::hex2rgb($_POST['app-shading']);

			if(!empty($_POST['font-family']))			
				$settings->fontFamily = $_POST['font-family'];

			if(!empty($_POST['font-family-heading']))			
				$settings->fontFamilyHeading = $_POST['font-family-heading'];

			if(!empty($_POST['ga-code']))			
				$settings->gaCode = $_POST['ga-code'];

			$settings->file = CUploadedFile::getInstance($settings,'file');

			if($settings->file)
				$settings->saveImage();
			else
				$settings->save();
		}

		$this->render('index',array(
			'heroProduct'=>$settings->heroProductId ? Product::model()->findByPk($settings->heroProductId) : null,
			'settings'=>$settings,
		));
	}

	/**
	 * Render the CSS file
	 */
	public function actionCss()
	{
		header("Content-type: text/css; charset: UTF-8");

		$settings = Settings::model()->find();

		$bodySize = ($settings->bodySize!='null' && $settings->bodySize) ?  $settings->bodySize : "14px";
		$font = ($settings->fontFamily!='null' && $settings->fontFamily) ? $settings->fontFamily : "GillSansMTStd";
		$fontReg = ($settings->fontFamily!='null' && $settings->fontFamily) ? $settings->fontFamily : "GillSansRegular";
		$fontHead = ($settings->fontFamilyHeading!='null' && $settings->fontFamilyHeading) ? $settings->fontFamilyHeading : "KnockoutRegular";
		$bg = ($settings->fileName && $settings->fileName) ? $settings->getUrl(1024,680) : '../images/bg.jpg';
		$bgShaded = ($settings->fileName && $settings->fileName) ? $settings->getUrl(1024,680) : '../images/bg-shaded.jpg';
		$primaryColour = ($settings->primaryColour!='null' && $settings->primaryColour) ?  $settings->primaryColour : "#fff";
		$appShading = ($settings->appShading!='null' && $settings->appShading) ?  $settings->appShading : "0,0,0"	;

		$this->renderPartial('css', array(
			'bodySize'=>$bodySize,
			'font'=>$font,
			'fontReg'=>$fontReg,
			'fontHead'=>$fontHead,
			'bg'=>$bg,
			'bgShaded'=>$bgShaded,
			'appShading'=>$appShading,
			'primaryColour'=>$primaryColour,
		));
	}
}
