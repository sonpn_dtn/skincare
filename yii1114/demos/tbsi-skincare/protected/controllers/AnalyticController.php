<?php

class AnalyticController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index','email','femaleComplete','femaleExpress','maleComplete','maleExpress','factsheets'),
				'users'=>array('admin'),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}


	public function actionMaleExpress(){

		Yii::import('CSVExport');

		$command = User::model()->script(array(3,23,23));

		$csv = new CSVExport($command->queryAll());

		Yii::app()->getRequest()->sendFile("maleeexpress.csv", $csv->toCSV(), 'text/csv; charset=ISO-8859-1');

	}

	public function actionMaleComplete(){
		Yii::import('CSVExport');

		$command = User::model()->script(array(2,19,19));

		$csv = new CSVExport($command->queryAll());

		Yii::app()->getRequest()->sendFile("malecomplete.csv", $csv->toCSV(), 'text/csv; charset=ISO-8859-1');

	}

	public function actionFemaleExpress(){
		
		Yii::import('CSVExport');

		$command = User::model()->script(array(4,26,26));

		$csv = new CSVExport($command->queryAll());

		Yii::app()->getRequest()->sendFile("femaleexpress.csv", $csv->toCSV(), 'text/csv; charset=ISO-8859-1');
	}

	public function actionFemaleComplete(){

		Yii::import('CSVExport');

		$command = User::model()->script(array(1,9,10));

		$csv = new CSVExport($command->queryAll());

		Yii::app()->getRequest()->sendFile("femalecomplete.csv", $csv->toCSV(), 'text/csv; charset=ISO-8859-1');

	}

	public function actionFactsheets(){
		Yii::import('CSVExport');
	
		$command = Yii::app()->db->createCommand("
			SELECT
				`name` as `First Name`,
				`lastName` as `Last Name`,
				`email` as `Email`,
				`created` as `Created Date`
			FROM
				`user`
			WHERE
				`email` IS NOT NULL
		");

		$csv = new CSVExport($command->queryAll());
		Yii::app()->getRequest()->sendFile("factsheet.csv", $csv->toCSV(), 'text/csv; charset=ISO-8859-1');
	}

	public function actionEmail(){
		Yii::import('CSVExport');
	
		$command = Yii::app()->db->createCommand("
			SELECT
				`name` as `First Name`,
				`lastName` as `Last Name`,
				`email` as `Email`,
				`created` as `Created Date`
			FROM
				`user`
			WHERE
				`newsletter` = 1
		");

		$csv = new CSVExport($command->queryAll());
		Yii::app()->getRequest()->sendFile("newsletter.csv", $csv->toCSV(), 'text/csv; charset=ISO-8859-1');
	}


	public function stats(){

	}

	public function actionIndex(){

		//Need to fix this to DRY - last thing on friday and brain not working.
		$femaleCompleteA = array();
		$i=1;
		while($i<11) {
			
			$command = User::model()->questions($i);
			array_push($femaleCompleteA,$command->queryAll()[0]['COUNT(DISTINCT `user`.`id`)']);
			//var_dump($femaleCompleteA);
			//die;	
			$i++;
		}

		$maleCompleteA = array();
		$i=11;
		while($i<20) {
			
			$command = User::model()->questions($i);
			array_push($maleCompleteA,$command->queryAll()[0]['COUNT(DISTINCT `user`.`id`)']);
			//var_dump($femaleCompleteA);
			//die;	
			$i++;
		}

		$maleExpressA = array();
		$i=21;
		while($i<24) {
			
			$command = User::model()->questions($i);
			array_push($maleExpressA,$command->queryAll()[0]['COUNT(DISTINCT `user`.`id`)']);
			//var_dump($femaleCompleteA);
			//die;	
			$i++;
		}

		$femaleExpressA = array();
		$i=24;
		while($i<27) {
			
			$command = User::model()->questions($i);
			array_push($femaleExpressA,$command->queryAll()[0]['COUNT(DISTINCT `user`.`id`)']);
			//var_dump($femaleCompleteA);
			//die;	
			$i++;
		}


		//emails
		$emails = User::model()->findAllByAttributes(array('newsletter'=>1));
		$emails = count($emails);

		$criteria = new CDbCriteria;
		$criteria->mergeWith(array(
		    'condition'=>'email IS NOT NULL',
		));
		$factsheet = User::model()->findAll($criteria);
		$factsheet =  count($factsheet);

		//female complete 
		//quiz id, answer id, answer id
		$command = User::model()->script(array(1,9,10));
		$femaleComplete = $command->queryAll();
		$femaleComplete = $femaleComplete[0]["COUNT(DISTINCT `user`.`id`)"];


		//female express
		$command = User::model()->script(array(4,26,26));
		$femaleExpress = $command->queryAll();
		$femaleExpress = $femaleExpress[0]["COUNT(DISTINCT `user`.`id`)"];

		//male complete
		$command = User::model()->script(array(2,19,19));
		$maleComplete = $command->queryAll();
		$maleComplete = $maleComplete[0]["COUNT(DISTINCT `user`.`id`)"];

		//male express
		$command = User::model()->script(array(3,23,23));
		$maleExpress = $command->queryAll();
		$maleExpress = $maleExpress[0]["COUNT(DISTINCT `user`.`id`)"];


		$questions = Question::model()->findAll(array('order'=>'id'));
		$questionArray = array();
		foreach($questions as $q){
			array_push($questionArray,$q->title);
		}

		$facebook = json_decode(file_get_contents("https://graph.facebook.com/?id=http://skincareonline.thebodyshop.co.uk"), TRUE);
		$twitter =json_decode(file_get_contents("http://urls.api.twitter.com/1/urls/count.json?url=http://skincareonline.thebodyshop.co.uk"), TRUE);

		$this->render('index',array(
			'emails'=>$emails,
			'femaleComplete'=>$femaleComplete,
			'femaleExpress'=>$femaleExpress,
			'maleExpress'=>$maleExpress,
			'maleComplete'=>$maleComplete,
			'facebook' =>$facebook['shares'],
			'twitter'=>$twitter['count'],
			'factsheet'=>$factsheet,
			'femaleCompleteA'=>$femaleCompleteA,
			'maleCompleteA'=>$maleCompleteA,
			'maleExpressA'=>$maleExpressA,
			'femaleExpressA'=>$femaleExpressA,
			'questionArray'=>$questionArray

			));
	}

}	

