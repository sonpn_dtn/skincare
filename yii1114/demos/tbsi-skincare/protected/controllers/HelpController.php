<?php

class HelpController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('page'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This method is invoked right before an action is to be executed (after all possible filters.)
	 * You may override this method to do last-minute preparation for the action.
	 * @param CAction $action the action to be executed.
	 * @return boolean whether the action should be executed.
	 */
	public function beforeAction($action)
	{
		$this->menu=array(
			array('label'=>_('About This CMS'), 'url'=>array('help/page','view'=>'index')),
			array('label'=>_('Getting Started'), 'url'=>array('help/page','view'=>'gettingstarted')),
			array('label'=>_('Localisation'), 'url'=>array('help/page','view'=>'localisation')),
			array('label'=>'<hr style="margin: 0;" />'),
			array('label'=>_('Managing Products'), 'url'=>array('help/page','view'=>'managingproducts'), 'items'=>array(
				array('label'=>_('Searching Products'), 'url'=>array('help/page','view'=>'searchingproducts')),
				array('label'=>_('Adding Products'), 'url'=>array('help/page','view'=>'creatingproducts')),
				array('label'=>_('Updating Products'), 'url'=>array('help/page','view'=>'updatingproducts')),
				array('label'=>_('Deleting Products'), 'url'=>array('help/page','view'=>'deletingproducts')),
				array('label'=>'<hr style="margin: 0;" />'),
				array('label'=>_('Bulk Upload'), 'url'=>array('help/page','view'=>'bulkupload')),
				array('label'=>_('Scraping Price & Stock'), 'url'=>array('help/page','view'=>'scrapepricestock')),
			)),
			array('label'=>'<hr style="margin: 0;" />'),
			array('label'=>_('Product Variants'), 'url'=>array('help/page','view'=>'productvariants'), 'items'=>array(
				array('label'=>_('Adding Product Variants'), 'url'=>array('help/page','view'=>'creatingproductvariants')),
				array('label'=>_('Updating Product Variants'), 'url'=>array('help/page','view'=>'updatingproductvariants')),
				array('label'=>_('Deleting Product Variants'), 'url'=>array('help/page','view'=>'deletingproductvariants')),
			)),
			array('label'=>'<hr style="margin: 0;" />'),
			array('label'=>_('Product Ranges'), 'url'=>array('help/page','view'=>'productranges'), 'items'=>array(
				array('label'=>_('Adding Product Ranges'), 'url'=>array('help/page','view'=>'creatingproductranges')),
				array('label'=>_('Updating Product Ranges'), 'url'=>array('help/page','view'=>'updatingproductranges')),
			)),
			array('label'=>'<hr style="margin: 0;" />'),
			array('label'=>_('Product Tags'), 'url'=>array('help/page','view'=>'producttags'), 'items'=>array(
				array('label'=>_('Adding Product Tags'), 'url'=>array('help/page','view'=>'creatingproducttags')),
				array('label'=>_('Updating Product Tags'), 'url'=>array('help/page','view'=>'updatingproducttags')),
			)),
			array('label'=>'<hr style="margin: 0;" />'),
			array('label'=>_('Managing Quizzes'), 'url'=>array('help/page','view'=>'managingquizzes'), 'items'=>array(
				array('label'=>_('Adding Quizzes'), 'url'=>array('help/page','view'=>'creatingquizzes')),
				array('label'=>_('Updating Quizzes'), 'url'=>array('help/page','view'=>'updatingquizzes')),
			)),
			array('label'=>'<hr style="margin: 0;" />'),
			array('label'=>_(' Questions'), 'url'=>array('help/page','view'=>'questions'), 'items'=>array(
				array('label'=>_('Adding Questions'), 'url'=>array('help/page','view'=>'creatingquestions')),
				array('label'=>_('Updating Questions'), 'url'=>array('help/page','view'=>'updatingquestions')),
				array('label'=>_('Deleting Questions'), 'url'=>array('help/page','view'=>'deletingquestions')),
			)),
			array('label'=>'<hr style="margin: 0;" />'),
			array('label'=>_(' Answers'), 'url'=>array('help/page','view'=>'answers'), 'items'=>array(
				array('label'=>_('Adding Answers'), 'url'=>array('help/page','view'=>'creatinganswers')),
				array('label'=>_('Updating Answers'), 'url'=>array('help/page','view'=>'updatinganswers')),
				array('label'=>_('Deleting Answers'), 'url'=>array('help/page','view'=>'deletinganswers')),
			)),
			array('label'=>'<hr style="margin: 0;" />'),
			array('label'=>_(' Related Products'), 'url'=>array('help/page','view'=>'relatedproducts'), 'items'=>array(
				array('label'=>_('Updating Related Products'), 'url'=>array('help/page','view'=>'updatingrelatedproducts')),
				array('label'=>_('Deleting Related Products'), 'url'=>array('help/page','view'=>'deletingrelatedproducts')),
			)),
		);

		return parent::beforeAction($action);
	}
}