<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'The Body Shop',

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'components'=>array(
		'db'=>array(
			'connectionString'=>'mysql:host=127.0.0.1;dbname=tbsiuk_skincare',
			'emulatePrepare'=>true,
			'username'=>'tbsiuk',
			'password'=>'Retr0grade!',
			'charset'=>'utf8',
		),
	),
);