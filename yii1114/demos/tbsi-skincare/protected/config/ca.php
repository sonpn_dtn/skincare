<?php

return CMap::mergeArray(
	require(dirname(__FILE__).'/main.php'),
	array(
		'sourceLanguage'=>'en_CA',
		'components'=>array(
			'db'=>array(
				'connectionString'=>'mysql:host=127.0.0.1;dbname=tbsica_skincare',
				'username'=>'tbsica',
				'password'=>'Retr0grade!',	
			),
			'mailEx' => array(
				'from' => 'The Body Shop <no-reply@thebodyshop.ca>',
			),
		),
		'params'=>array(
			'imageUrl' => array(
				'60x60' => 'http://www.thebodyshop.ca/en/images/packshot/products/small/%s_s.jpg',
				'250X250' => 'http://www.thebodyshop.ca/en/images/packshot/products/med_large/%s_m_l.jpg',
				'450X450' => 'http://www.thebodyshop.ca/en/images/packshot/products/large/%s_l.jpg',
			),
			'priceStockUrl' => array(
				'en_CA' => 'http://www.thebodyshop.ca/en/ajax/catalog/skin-care-diagnostics.aspx?ids=',
				'fr_CA' => 'http://www.thebodyshop.ca/fr/ajax/catalog/skin-care-diagnostics.aspx?ids=',
			),
			'populateBasketUrl' => array(
				'en_CA' => 'http://www.thebodyshop.ca/en/populate_basket.aspx',
				'fr_CA' => 'http://www.thebodyshop.ca/fr/populate_basket.aspx',
			),
			'tag' => array(
				'cleanseId' => 235,
				'toneId' => 236,
				'moisturiseId' => 240,
			),
			'range' => array(
				'camomileId' => 242,
			),
		),
	)
);
