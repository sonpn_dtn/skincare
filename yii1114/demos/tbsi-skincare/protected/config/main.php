<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'The Body Shop',
	'sourceLanguage'=>'en_GB',
	'language'=>str_replace('.utf8', '', setlocale(LC_ALL, 0)),

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	// application components
	'components'=>array(
		/*'db'=>array(
			'connectionString'=>'mysql:host=127.0.0.1;dbname=tbsiuk_skincare',
			'emulatePrepare'=>true,
			'username'=>'tbsiuk',
			'password'=>'Retr0grade!',
			'charset'=>'utf8',
		),*/

        'db'=>array(
            'connectionString' => 'sqlite:protected/data/data.db',
        ),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		
		'imageEx'=>array(
			'class'=>'application.components.image.CImageComponent',
			'driver'=>'GD',
		),

		'mailEx' => array(
			'class' => 'MailEx',
			'from' => 'The Body Shop <no-reply@thebodyshop.co.uk>',
			'host' => '195.171.111.6',
		),

		'messages'=>array(
			'class'=>'CDbMessageSource'
		),

		'request'=>array(
			'baseUrl'=>'',
		),

		'urlManager'=>array(
			'class'=>'UrlManager',
		),
	
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		'imageUrl' => array(
			'60x60' => 'http://www.thebodyshop.co.uk/images/product/small/%s_s.jpg',
			'250X250' => 'http://www.thebodyshop.co.uk/images/product/med_large/%s_m_l.jpg',
			'450X450' => 'http://www.thebodyshop.co.uk/images/product/large/%s_l.jpg',
		),
		'priceStockUrl' => array(
			'en_GB' => 'http://www.thebodyshop.co.uk/ajax/catalog/skin-care-diagnostics.aspx?ids=',
		),
		'populateBasketUrl' => array(
			'en_GB' => 'http://www.thebodyshop.co.uk/populate_basket.aspx',
		),
		'tag' => array(
			'cleanseId' => 4,
			'toneId' => 2,
			'moisturiseId' => 1,
		),
		'range' => array(
			'camomileId' => 13,
		),
	),
);
