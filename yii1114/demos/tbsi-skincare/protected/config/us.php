<?php

return CMap::mergeArray(
	require(dirname(__FILE__).'/main.php'),
	array(
		'sourceLanguage'=>'en_US',
		'components'=>array(
			'db'=>array(
				'connectionString'=>'mysql:host=127.0.0.1;dbname=tbsius_skincare',
				'username'=>'tbsius',
				'password'=>'Retr0grade!',
			),
			'mailEx' => array(
				'from' => 'The Body Shop <no-reply@thebodyshop-usa.com>',
			),
		),
		'params'=>array(
			'imageUrl' => array(
				'60x60' => 'http://www.thebodyshop-usa.com/images/packshot/products/small/%s_s.jpg',
				'250X250' => 'http://www.thebodyshop-usa.com/images/packshot/products/med_large/%s_m_l.jpg',
				'450X450' => 'http://www.thebodyshop-usa.com/images/packshot/products/large/%s_l.jpg',
			),
			'priceStockUrl' => array(
				'en_US' => 'http://www.thebodyshop-usa.com/ajax/catalog/skin-care-diagnostics.aspx?ids=',
			),
			'populateBasketUrl' => array(
				'en_US' => 'http://www.thebodyshop-usa.com/populate_basket.aspx',
			),
			'tag' => array(
				'cleanseId' => 1,
				'toneId' => 2,
				'moisturiseId' => 6,
			),
			'range' => array(
				'camomileId' => 3,
			),
		),
	)
);
