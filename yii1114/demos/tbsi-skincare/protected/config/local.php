<?php

return CMap::mergeArray(
	require(dirname(__FILE__).'/main.php'),
	array(
		'modules'=>array(
			'gii'=>array(
				'class'=>'system.gii.GiiModule',
				'password'=>'password',
				'ipFilters'=>array('*','::1'),
			),
		),
	
		'components'=>array(
			'db'=>array(
				'connectionString'=>'mysql:host=127.0.0.1;dbname=smack_tbsi-skincare',
				'username'=>'root',
				'password'=>'root',
				'enableProfiling'=>false,
				'enableParamLogging'=>true,
			),

			'request'=>array(
				'baseUrl'=>'/tbsi-skincare',
			),
			
			'log'=>array(
				'class'=>'CLogRouter',
				'routes'=>array(
					array(
						'class'=>'CProfileLogRoute',
						'levels'=>'profile',
						'enabled'=>true,
					),
				),
			),
		),
	)
);
