<?php return CMap::mergeArray(
    require(dirname(__FILE__).'/main.php'),
    array(
        'sourceLanguage'=>'fr_FR',
        'components'=>array(
            'db'=>array(
                'connectionString'=>'mysql:host=127.0.0.1;dbname=tbsifr_skincare',
                'username'=>'tbsifr',
                'password'=>'Retr0grade!',
            ),
            'mailEx' => array(
                'from' => 'The Body Shop <no-reply@thebodyshop.fr>',
            ),
        ),
        'params'=>array(
			'imageUrl' => array(
				'60x60' => 'http://www.thebodyshop.fr/images/packshot/products/small/%s_s.jpg',
				'250X250' => 'http://www.thebodyshop.fr/images/packshot/products/med_large/%s_m_l.jpg',
				'450X450' => 'http://www.thebodyshop.fr/images/packshot/products/large/%s_l.jpg',
			),
			'priceStockUrl' => array(
				'en_US' => 'http://www.thebodyshop.fr/ajax/catalog/skin-care-diagnostics.aspx?ids=',
			),
			'populateBasketUrl' => array(
				'en_US' => 'http://www.thebodyshop.fr/populate_basket.aspx',
			),
			'tag' => array(
				'cleanseId' => 4,
				'toneId' => 8,
				'moisturiseId' => 6,
			),
			'range' => array(
				'camomileId' => 12,
			),
        ),
    )
);
