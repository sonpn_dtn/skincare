<?php

require_once 'Mail.php';
require_once 'Mail/mime.php';

class MailEx extends CApplicationComponent
{
	public $from;
	public $host;

	/**
	 * Send an email using PEAR::Mail
	 */
	public function pear($toName, $toEmail, $subject, $html)
	{
		$message = new Mail_mime();
		$message->setHTMLBody($html ? $html : $text);
		$body = $message->get();
		
		$headers = $message->headers(array(
			'From' => $this->from,
			'To' => $toName ? "$toName <$toEmail>" : $toEmail,
			'Subject' => $subject
		));

		$smtp = Mail::factory('smtp', array(
                        'host' => $this->host,
                ));;

		$mail = $smtp->send($toEmail, $headers, $body);

		if(PEAR::isError($mail))
			return $mail->getMessage();
		else
			return true;
	}
}
