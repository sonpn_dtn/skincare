<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	public $bodyId;
	public $bodyClass;
	public $regions;
	public $settings;

	public $siteCatalyst_pageInfo = 'Skincare Diagnostic: Step 1';
	public $siteCatalyst_subCategoryKey = 'subCategory1';
	public $siteCatalyst_subCategoryValue = 'Skincare Diagnostic Home';

	public $canonical = true;

	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
	
	/**
	 * This method is invoked right before an action is to be executed (after all possible filters.)
	 * You may override this method to do last-minute preparation for the action.
	 * @param CAction $action the action to be executed.
	 * @return boolean whether the action should be executed.
	 */
	public function beforeAction($action)
	{
		if($_SERVER['REQUEST_METHOD'] == 'OPTIONS')
			Yii::app()->end();
		elseif(isset($_GET['r']))
		{
			Yii::app()->clientScript->registerCoreScript('jquery');
			Yii::app()->clientScript->scriptMap = array(
				'jquery.js' => Yii::app()->baseUrl.'/js/blueprint/jquery-1.10.2.js',
			);
		}
		else
			Yii::app()->theme = 'classic';

		// Kill uncompressed assets
		if(!isset($_GET['r']))
			Yii::app()->clientScript->scriptMap = array(
				'styles.css' => false,
				'pager.css' => false,

				'jquery.js' => false,
				'jquery.min.js' => false,
				'jquery.ba-bbq.js' => false,
				'jquery.yiilistview.js' => false,
			);

		// Work out the body ID
		$this->bodyId = $this->action->controller->id.'-'.$this->action->id;

		// Set up mobile detect
		$mobileDetect = new CMobile;
		if($mobileDetect->isAndroidOS())
			$this->bodyClass = 'android';

		// Set the language
		if(isset($_GET['language']))
			Yii::app()->language=$_GET['language'];

		// Set the page title
		$this->pageTitle = Yii::app()->name;

		// Get the app regions
		$this->regions = Region::model()->findAll();

		// Get the app settings
		$this->settings = Settings::model()->find();

		return true;
	}

	/**
	 * Helper function used in the email to move empty items to the end of an array
	 */
	public function emptySort($a, $b)
	{
		if($a == '' && $b != '') return 1;
		if($b == '' && $a != '') return -1;
		return 0; 
	}

	/**
	 * Helper function to generate a language switch URL
	 */
	public function getLanguageSwitchUrl($languageCode, $countryCode)
	{
		$params = $_GET;
		$params['language'] = $languageCode.'_'.$countryCode;

		return $this->createUrl($this->action->controller->id.'/'.$this->action->id, $params);
	}
}
