<?php

class ActiveForm extends CActiveForm
{
	public function styledButtonList($model, $attribute, $options, $value = null)
	{
		$index = 1;
		$rel = CHtml::activeId($model, $attribute);
		
		$htmlOptions = array(
			'class' => 'hiddenSelection',
		);

		if(is_array($value))
			$htmlOptions['value'] = implode(',', $value);
		elseif($value)
			$htmlOptions['value'] = $value;

		$html = $this->hiddenField($model, $attribute, $htmlOptions);
		$html .= '<div class="group selection-container option-'.count($options).'">';

		foreach($options as $key => $value)
		{
			if(is_object($value))
				$html .= '<div class="selection '.($index % 2 ? 'odd' : 'even').'" rel="'.$rel.'" data-val="'.$value->id.'" data-description="'.$value->description.'" data-unique="'.(isset($value->unique) ? $value->unique : 0).'"><span class="table"><span>'.$value->text.'</span></span></div>';
			else
				$html .= '<div class="selection '.($index % 2 ? 'odd' : 'even').'" rel="'.$rel.'" data-val="'.$key.'"><span class="table"><span>'.$value.'</span></span></div>';

			$index++;
		}

		$html .= '</div>';

		return $html;
	}
}
