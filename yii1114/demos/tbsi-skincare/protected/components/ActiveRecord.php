<?php

class ActiveRecord extends CActiveRecord
{
	public $nonTranslatables = array();
	public $translatables = array();

	/**
	 * Adapted from CDbCommandBuilder::insert()
	 */
	public function insertOnDuplicateKeyUpdate()
	{
		$table=$this->getMetaData()->tableSchema;
		$fields=array();
		$values=array();
		$placeholders=array();
		$update=array();
		$i=0;
		foreach($this->getAttributes() as $name=>$value)
		{
			if(($column=$table->getColumn($name))!==null && $value)
			{
				$fields[]=$column->rawName;
				$placeholders[]=CDbCommandBuilder::PARAM_PREFIX.$i;
				$values[CDbCommandBuilder::PARAM_PREFIX.$i]=$column->typecast($value);
				if($table->primaryKey != $name)
					$update[]="{$column->rawName}=".CDbCommandBuilder::PARAM_PREFIX.$i;
				$i++;
			}
		}
		$sql="INSERT INTO {$table->rawName} (".implode(', ',$fields).') VALUES ('.implode(', ',$placeholders).') ON DUPLICATE KEY UPDATE '.implode(', ',$update);
		$command=$this->getDbConnection()->createCommand($sql);

		foreach($values as $name=>$value)
			$command->bindValue($name,$value);

		return $command->execute();
	}

	/**
	 * PHP getter magic method.
	 * This method is overridden so that AR attributes can be accessed like properties.
	 * @param string $name property name
	 * @return mixed property value
	 * @see getAttribute
	 */
	public function __get($name)
	{
		return Yii::t('app', parent::__get($name));
	}

	/**
	 * This method is used to override the translator
	 * @param string $name property name
	 * @return mixed property value
	 * @see getAttribute
	 */
	public function getUntranslated($name)
	{
		return parent::__get($name);
	}

	/**
	 * Saves the current record.
	 *
	 * The record is inserted as a row into the database table if its {@link isNewRecord}
	 * property is true (usually the case when the record is created using the 'new'
	 * operator). Otherwise, it will be used to update the corresponding row in the table
	 * (usually the case if the record is obtained using one of those 'find' methods.)
	 *
	 * Validation will be performed before saving the record. If the validation fails,
	 * the record will not be saved. You can call {@link getErrors()} to retrieve the
	 * validation errors.
	 *
	 * If the record is saved via insertion, its {@link isNewRecord} property will be
	 * set false, and its {@link scenario} property will be set to be 'update'.
	 * And if its primary key is auto-incremental and is not set before insertion,
	 * the primary key will be populated with the automatically generated key value.
	 *
	 * @param boolean $runValidation whether to perform validation before saving the record.
	 * If the validation fails, the record will not be saved to database.
	 * @param array $attributes list of attributes that need to be saved. Defaults to null,
	 * meaning all attributes that are loaded from DB will be saved.
	 * @return boolean whether the saving succeeds
	 */
	public function save($runValidation=true,$attributes=null)
	{
		if($this->isNewRecord)
			parent::save($runValidation, $attributes);

		// If we are not working on the master language don't update the master files
		if(Yii::app()->sourceLanguage != Yii::app()->language)
		{
			$attributes = $attributes ? array_intersect($attributes, $this->nonTranslatables) : $this->nonTranslatables;

			// Find the original entry...
			$model = $this;

			// ... and instead, update the translation table
			foreach($this->translatables as $attribute)
			{
				// Find the original message
				$sourceMessage = SourceMessage::model()->findByAttributes(array('message' => $model->getUntranslated($attribute)));

				// If there is no translation yet, create the source
				if(!$sourceMessage)
				{
					$sourceMessage = new SourceMessage;
					$sourceMessage->category = 'app';
					$sourceMessage->message = $model->getUntranslated($attribute);
					$sourceMessage->save();
				}

				// Save the translation
				$message = Message::model()->findByAttributes(array('id' => $sourceMessage->id, 'language' => Yii::app()->language));

				if($message)
					$message->translation = $this->{$attribute};
				else
				{
					$message = new Message;
					$message->id = $sourceMessage->id;
					$message->language = Yii::app()->language;
					$message->translation = $this->{$attribute};
				}

				$message->save();
			}
		}

		return parent::save($runValidation, $attributes);
	}
}
