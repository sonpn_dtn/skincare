<?php

class CString
{
	/**
	 * @return string HTML-ready string that converts an array in to a translatable list
	 */
	public static function _list($data)
	{
		if(count($data) == 1)
			return array_shift($data);
		elseif(count($data) == 2)
			return array_shift($data).' '._('and').' '.array_shift($data);
		elseif(count($data) > 2)
		{
			$last = array_pop($data);

			$html = implode(', ', $data);
			$html .= ' '._('and').' '.$last;

			return $html;
		}
	}

	public static function _($key)
	{
		return nl2br(_($key));
	}

	public static function _e($key)
	{
		echo nl2br(_($key));
	}

	public static function hex2rgb($hex)
	{
		$hex = str_replace("#", "", $hex);

		if(strlen($hex) == 3)
		{
			$r = hexdec(substr($hex,0,1).substr($hex,0,1));
			$g = hexdec(substr($hex,1,1).substr($hex,1,1));
			$b = hexdec(substr($hex,2,1).substr($hex,2,1));
		}
		else
		{
			$r = hexdec(substr($hex,0,2));
			$g = hexdec(substr($hex,2,2));
			$b = hexdec(substr($hex,4,2));
		}

		$rgb = array($r, $g, $b);
		return implode(",", $rgb);
	}

	public static function rgb2hex($rgb)
	{
		$rgb = explode(",", $rgb);

		$hex = dechex($rgb[0]);
		$hex .= dechex($rgb[1]);
		$hex .= dechex($rgb[2]);

		return '#'.$hex;
	}
}