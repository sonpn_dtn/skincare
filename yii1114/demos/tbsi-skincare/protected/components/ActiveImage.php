<?php

/**
 * An extension of CActiveRecord with image specific functionality
 */
class ActiveImage extends ActiveRecord
{
	protected $directoryName = 'uploads/productrange';

	/**
	 * Extending save function to handle file uploading
	 */
	public function saveImage($runValidation=true,$attributes=null)
	{
		// Save prematurely to get an ID
		if(parent::save($runValidation,$attributes))
		{
			/* Check if the file is an image
			$finfo = finfo_open(FILEINFO_MIME_TYPE);
			if(strpos(finfo_file($finfo, $this->file->tempName), 'image/') !== false)
			{
				// Load the Image Extension
				$file=Yii::app()->imageEx->load($this->file->tempName);
		
				$this->fileName=$file->width.'_'.$file->height;
				$this->extension='.'.$file->ext;
			}
			else
			{
				$this->fileName=substr($this->file->name, 0, strrpos($this->file->name, '.'));
				$this->extension=substr($this->file->name, strrpos($this->file->name, '.'));
			}
			
			finfo_close($finfo);
			*/
			
			$this->directory=Yii::getPathOfAlias('webroot').'/'.$this->directoryName.'/'.$this->id.'/';
			$this->extension=substr($this->file->name, strrpos($this->file->name, '.'));
			
			// Check if the file is an image
			if($this->isImage())
			{
				// Load the Image Extension
				$file=Yii::app()->imageEx->load($this->file->tempName);
				$this->fileName=$file->width.'_'.$file->height;
			}
			else
				$this->fileName=substr($this->file->name, 0, strrpos($this->file->name, '.'));
	
			// Create the directory if it doesn't exist
			if(!is_dir($this->directory))
			{
				$oldumask = umask(0);
				mkdir($this->directory, 0777, true);
				umask($oldumask);
			}

			// Save and return
			if($this->file->saveAs($this->getFullPath()))
				return parent::save($runValidation,$attributes);
				
			// If the upload failed, remove the prematurely saved data
			else
				$this->delete();
		}
		
		return false;
	}

	/**
	 * Get a full image URL
	 * @return string
	 */
	public function getUrl($width = null, $height = null, $crop = true)
	{
		try
		{
			if($this->isImage())
				return Yii::app()->getBaseUrl(true).'/'.$this->directoryName.'/'.$this->id.'/'.$this->resizeImage($width, $height, $crop).$this->extension;
			else
				return Yii::app()->getBaseUrl(true).'/'.$this->directoryName.'/'.$this->id.'/'.$this->fileName.$this->extension;
		}
		catch(Exception $e)
		{
			return false;
		}
	}
	
	/**
	 * Get the original width
	 * @return int
	 */
	public function getWidth()
	{
		list($width, $height) = explode('_', $this->fileName);
		return $width;
	}

	/**
	 * Get the original height
	 * @return int
	 */
	public function getHeight()
	{
		list($width, $height) = explode('_', $this->fileName);
		return $height;
	}

	/**
	 * Get a full size string
	 * @return string
	 */
	public function getImgSizeString($width = null, $height = null, $crop = true)
	{
		$attr = $this->getFileAttr($width, $height, $crop);
		return 'width="'.$attr['width'].'" height="'.$attr['height'].'"';
	}

	/**
	 * Get a full image path
	 * @return string
	 */
	public function getFullPath()
	{
		return $this->directory.$this->fileName.$this->extension;
	}

	/**
	 * Concatenates the file details to get a full image path
	 * - 1 If both width and height are provided, return that correct size image
	 * - 2 If just the width is provided, presume it is a file name to retrieve
	 * - 3 Default to stored file name
	 * @return string
	 */
	public function getRelativePath($width = null, $height = null, $crop = true)
	{
		if(is_int($width) && is_int($height))
			return $this->directory.$this->resizeImage($width, $height, $crop).$this->extension;

		return $this->directory.($width ? $width : $this->fileName).$this->extension;
	}

	/**
	 * 'on-the-fly' resizing should the image file not exist
	 * Specific cropping is implied as we know all images on the site will be of asepct LARGER:SMALLER
	 * @return string
	 */
	protected function resizeImage($width = null, $height = null, $crop = true)
	{
		$attr = $this->getFileAttr($width, $height, $crop);
		$image_path = $this->getRelativePath($attr['fileName']);

		if(!file_exists($image_path))
		{
			$image = Yii::app()->imageEx->load($this->getRelativePath());

			$aspect = $attr['width'] / $attr['height'];

			if($attr['image_aspect'] > $aspect) // Image too wide
				if($crop)
				{
					$height_ratio = $image->height / $attr['height'];
					$image->resize($image->width / $height_ratio, $attr['height'])->crop($attr['width'], $attr['height']);
				} else
					$image->resize($attr['width'], $attr['width'] * $aspect);

			elseif($attr['image_aspect'] < $aspect) // Image too narrow
				if($crop)
				{
					$width_ratio = $image->width / $attr['width'];
					$image->resize($attr['width'], $image->height / $width_ratio)->crop($attr['width'], $attr['height']);
				} else
					$image->resize($attr['height'] * $aspect, $attr['height']);
					
			else // Image just right
				$image->resize($attr['width'], $attr['height']);

			$image->save($image_path);
		}

		return $attr['fileName'];
	}

	public function getFileAttr($width = null, $height = null, $crop = true)
	{
		list($image_width, $image_height) = explode('_', $this->fileName);
		$image_aspect = $image_width / $image_height;

		if($width && $height) // We have width and height - construct the file name
			$fileName = $width.'_'.$height.($crop ? '_crop' : null);

		elseif(!$width && !$height) // Neither width and height - use original
		{
			$fileName = $this->fileName;
			$width = $image_width;
			$height = $image_height;
		}

		else // One of the two - relative sizing
		{
			if(!$width)
				$width = floor($height * $image_aspect);
			else
				$height = floor($width / $image_aspect);

			$fileName = $width.'_'.$height.($crop ? '_crop' : null);
		}

		return array(
			'fileName' => $fileName,
			'image_width' => $image_width,
			'image_height' => $image_height,
			'image_aspect' => $image_aspect,
			'width' => $width,
			'height' => $height,
		);
	}
	
	public function isImage()
	{
		return in_array(strtolower($this->extension), array('.jpg', '.gif', '.png', '.jpeg'));
	}
}
