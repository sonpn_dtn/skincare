<!doctype html>
<html lang="en-GB">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<?php if($this->bodyId != 'site-index') echo '<meta name="robots" content="nofollow" />'; ?>
	<meta name="description" content="<?php echo _('meta-description'); ?>" />
	<meta name="keywords" content="<?php echo _('meta-keywords'); ?>" />

	<meta property="og:title" content="<?php echo _('meta-title'); ?>" />
	<meta property="og:site_name" content="<?php echo _('meta-site-name'); ?>" />
	<meta property="og:url" content="<?php echo _('meta-url'); ?>" />
	<meta property="og:description" content="<?php echo _('meta-description'); ?>" />
	<meta property="og:image" content="<?php echo Yii::app()->getBaseUrl(true); ?>/images/logo-fb.jpg" />
	<meta property="fb:app_id" content="1452283208352325" />

	<link rel="gettext" type="application/x-po" href="<?php echo Yii::app()->baseUrl; ?>/locales/<?php echo Yii::app()->language; ?>/LC_MESSAGES/the-body-shop-international.po" />
	<?php if($this->canonical) { ?>
	<link rel="canonical" href="<?php echo Yii::app()->getBaseUrl(true); ?>" />
	<?php } ?>

	<?php if(strpos($_SERVER['SERVER_NAME'], 'skincareonline.thebodyshop.') === 0) { ?>
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/all.css?2" />

	<?php if($this->settings->gaCode) { ?>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', '<?php echo $this->settings->gaCode; ?>', '<?php echo $_SERVER['SERVER_NAME']; ?>');
		ga('send', 'pageview');
	</script>
	<?php } ?>

	<script src="//assets.adobedtm.com/42c88fb73f5fd0aa15bab49ffa24c097a2516e34/satelliteLib-c38c6b3075bbe69766e1a5e7f5a5ba48a4348635.js"></script>
	<?php } else { ?>
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/html5reset.css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery-ui-1.10.4.custom.css" />

	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/responsivegridsystem/col.css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/responsivegridsystem/2cols.css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/responsivegridsystem/3cols.css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/responsivegridsystem/4cols.css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/responsivegridsystem/5cols.css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/responsivegridsystem/6cols.css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/responsivegridsystem/7cols.css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/responsivegridsystem/8cols.css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/responsivegridsystem/9cols.css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/responsivegridsystem/10cols.css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/responsivegridsystem/11cols.css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/responsivegridsystem/12cols.css" />
	<?php } ?>

	<?php if($this->settings->fontFamily) { ?>
	<link href='http://fonts.googleapis.com/css?family=<?php echo $this->settings->fontFamily; ?>:300,400,600,700,300italic' rel='stylesheet' type='text/css'>
	<?php } ?>

	<?php if($this->settings->fontFamilyHeading) { ?>
	<link href='http://fonts.googleapis.com/css?family=<?php echo $this->settings->fontFamilyHeading; ?>:300,400,600,700,300italic' rel='stylesheet' type='text/css'>
	<?php } ?>

	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/settings/css" />

	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/blog.css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/blog-mobile.css" />

	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/print.css" />

	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/810.css?1" media="only screen and (max-width: 810px) and (min-width: 561px)" />
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/568.css?1" media="only screen and (max-width: 568px)" />

	<link rel="SHORTCUT ICON" href="<?php echo Yii::app()->baseUrl; ?>/favicon.ico"/>

	<title><?php echo _('meta-title'); ?> | <?php echo _('meta-site-name'); ?></title>
</head>
<body id="<?php echo $this->bodyId; ?>" class="<?php echo $this->bodyClass; ?>">
	<div id="top-bar-span" class="full-width">
		<div class="top-bar-nav fixed-width wide">
			<div class="language-select">
				<?php
				if(count($this->regions) > 1)
				foreach($this->regions as $i => $region) {
					if($region->languageCode.'_'.$region->countryCode == Yii::app()->language)
						echo $region->name;
					else {
				?>
				<a href="<?php echo $this->getLanguageSwitchUrl($region->languageCode, $region->countryCode); ?>"><?php echo $region->name; ?></a>
				<?php
					}

					if($i + 1 != count($this->regions)) echo ' | ';
				}
				?>
			</div>

			<ul id="menu-top-bar-menu" class="menu">
				<li id="menu-item-602" class="rewards-card menu-item menu-item-type-custom menu-item-object-custom menu-item-602"><a href="http://www.thebodyshop.co.uk/loyalty/love-your-body/index.aspx?stop_mobi=yes">Rewards Card</a></li>
				<li id="menu-item-862" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-862"><a href="http://www.thebodyshop.co.uk/athome/index.aspx?stop_mobi=yes">Party at Home</a></li>
				<li id="menu-item-872" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-872"><a href="http://www.thebodyshop.co.uk/socialhub/socialhub.aspx?stop_mobi=yes">Social Hub</a></li>
				<li id="menu-item-2342" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2342"><a href="http://beautyblog.thebodyshop.com/">Blog</a></li>
				<li id="menu-item-882" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-882"><a href="http://www.thebodyshop.co.uk/content/newsletter-signup.aspx?stop_mobi=yes">Email Updates</a></li>
				<li id="menu-item-892" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-892"><a href="http://www.thebodyshop.co.uk/services/storelocator-search.aspx">Find a Store</a></li>
				<li id="menu-item-1942" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1942"><a href="http://www.thebodyshop.co.uk/services/cookies.aspx?stop_mobi=yes">Cookies</a></li>
			</ul>
		</div>

		<div class="clear"></div>
	</div>

	<div id="header-span" class="full-width">
		<div id="header" class="fixed-width wide">
			<a target="_blank" class="logo" href="http://www.thebodyshop.co.uk">The Body Shop UK | Beauty from the Heart</a>

			<div class="nav">
				<ul id="menu-main-menu" class="menu">
					<li id="menu-item-3062" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3062"><a href="#">Menu</a>
						<ul class="sub-menu">
							<li id="menu-item-3052" class="shop-link menu-item menu-item-type-custom menu-item-object-custom menu-item-3052"><a href="http://www.thebodyshop.co.uk">Shop</a></li>
							<li id="menu-item-3072" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3072"><a href="http://www.thebodyshop.co.uk/whats-hot.aspx">What’s Hot</a></li>
							<li id="menu-item-3082" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3082"><a href="http://www.thebodyshop.co.uk/gifts.aspx">Gifts</a></li>
							<li id="menu-item-3092" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3092"><a href="http://www.thebodyshop.co.uk/bath-body-care.aspx">Bath &amp; Body Care</a></li>
							<li id="menu-item-3102" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3102"><a href="http://www.thebodyshop.co.uk/hand-care.aspx">Hand Care</a></li>
							<li id="menu-item-3112" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3112"><a href="http://www.thebodyshop.co.uk/skincare.aspx">Skincare</a></li>
							<li id="menu-item-3122" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3122"><a href="http://www.thebodyshop.co.uk/make-up.aspx">Make-up</a></li>
							<li id="menu-item-3132" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3132"><a href="http://www.thebodyshop.co.uk/fragrance.aspx">Fragrance</a></li>
							<li id="menu-item-3142" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3142"><a href="http://www.thebodyshop.co.uk/mens.aspx">Mens</a></li>
							<li id="menu-item-3152" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3152"><a href="http://www.thebodyshop.co.uk/shop-by-range.aspx">Shop by Range</a></li>
						</ul>
					</li>
				</ul>
			</div>

			<div class="clear"></div>
		</div>
	</div>

	<div class="content-container">
		<?php echo $content; ?>
	</div>

	<div id="footer" class="fixed-width">
		<div id="footer-inner">
			<a class="logo" href="http://www.thebodyshop.co.uk/content/beauty-with-heart-infographic.aspx">Body Shop</a>
		
			<div id="footer-right">
				<ul id="block-links">
					<li><a href="http://www.thebodyshop.co.uk/values/AgainstAnimalTesting.aspx?stop_mobi=yes"><img src="<?php echo Yii::app()->baseUrl; ?>/images/blog/against-animal-testing.png" alt="Against Animal Testing"></a></li>
					<li><a href="http://www.thebodyshop.co.uk/values/CommunityFairTrade.aspx?stop_mobi=yes"><img src="<?php echo Yii::app()->baseUrl; ?>/images/blog/support-community-fair-trade.png" alt="Support Community Fair Trade"></a></li>
					<li><a href="http://www.thebodyshop.co.uk/values/SelfEsteem.aspx?stop_mobi=yes"><img src="<?php echo Yii::app()->baseUrl; ?>/images/blog/activate-self-esteem.png" alt="Activate Self Esteem"></a></li>
					<li><a href="http://www.thebodyshop.co.uk/values/DefendHumanRight.aspx?stop_mobi=yes"><img src="<?php echo Yii::app()->baseUrl; ?>/images/blog/defend-human-rights.png" alt="Defend Human Rights"></a></li>
					<li><a href="http://www.thebodyshop.co.uk/values/ProtectPlanet.aspx?stop_mobi=yes"><img src="<?php echo Yii::app()->baseUrl; ?>/images/blog/protect-the-planet.png" alt="Protect The Planet"></a></li>

					<div class="clear"></div>
				</ul>
		
				<div class="footer-nav">
					<ul id="menu-footer-menu" class="menu">
						<li id="menu-item-1842" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1842"><a href="http://www.thebodyshop.co.uk/services/aboutus.aspx?stop_mobi=yes">About The Body Shop</a></li>
						<li id="menu-item-2082" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2082"><a href="http://www.thebodyshop.com/content/services/aboutus_privacypolicy.aspx">Privacy Policy</a></li>
						<li id="menu-item-2372" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2372"><a href="http://www.thebodyshop.co.uk/services/termsconditions.aspx">Terms &amp; Conditions</a></li>
						<li id="menu-item-2972" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2972"><a href="http://www.thebodyshop.co.uk/services/sitemap.aspx?stop_mobi=yes">Sitemap</a></li>
					</ul>
				</div>			

				<div class="clear"></div>
			</div>

			<div class="clear"></div>
		</div>

		<p>
			<strong>©<?php echo date('Y'); ?> The Body Shop International PLC</strong><br />
			® A registered trademark of The Body Shop International plc. ™ A trademark of The Body Shop International plc. All rights reserved.
		</p>

		<div class="clear"></div>
	</div>

	<div id="fb-root"></div>
	<script type="text/javascript">var DROPS_OF_YOUTH = <?php echo Settings::model()->find()->heroProductId; ?>, POPULATE_BASKET_URL = '<?php echo Yii::app()->params['populateBasketUrl'][Yii::app()->language]; ?>';</script>

	<?php if(strpos($_SERVER['SERVER_NAME'], 'skincareonline.thebodyshop.') === 0) { ?>
	<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/all.js?2"></script>
	<script type="text/javascript">
		DTMData = {
			page: {
				pageInfo:  "<?php echo $this->siteCatalyst_pageInfo; ?>",
				category: {
					primaryCategory: "External Diagnostic",
					<?php echo $this->siteCatalyst_subCategoryKey; ?>: "<?php echo $this->siteCatalyst_subCategoryValue; ?>",
					pageType: "Diagnostic"
				},
				attributes: {
					country: "UK",
					language: "en-UK"
				}
			}
		};
	</script>
	<script type="text/javascript">_satellite.pageBottom();</script>
	<?php } else { ?>
	<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/gettext.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/retina.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery-1.10.2.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery-ui-1.10.4.custom.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.touchSwipe.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/script.js"></script>
	
	<?php } ?>
</body>
</html>
