<div class="group content">
	<div class="col span_8_of_11 content-left">
		<div class="question-container">
			<h1><?php echo $model->title; ?></h1>
			<p><?php echo $model->text; ?></p>

			<?php $form=$this->beginWidget('ActiveForm',array(
				'htmlOptions'=>array(
					'data-type'=>$model->type,
					'data-typearg'=>$model->typeArg,
				),
			)); ?>

				<?php echo $form->styledButtonList($answerUserModel,'answerId',$model->activeAnswers,$model->getAnswerIdsByUser(Yii::app()->request->getParam('userId'))); ?>

				<div class="group">
					<div class="col span_3_of_4 hint-container">
						<a class="desktop back <?php if(Yii::app()->request->getParam('summary')) echo 'inactive'; ?>" href="javascript:history.go(-1);">
							<img src="<?php echo Yii::app()->baseUrl; ?>/images/arrow-previous.png" alt="" />
							<span><?php CString::_e('back'); ?></span>
						</a>

						<div class="hint">
							<div class="hint-title"></div>
							<div class="hint-description"></div>
						</div>

						&nbsp;
					</div>

					<div class="col span_1_of_4 button">
						<a class="mobile-block back <?php if(Yii::app()->request->getParam('summary')) echo 'inactive'; ?>" href="javascript:history.go(-1);">
							<img src="<?php echo Yii::app()->baseUrl; ?>/images/arrow-previous.png" alt="" />
							<span><?php CString::_e('back'); ?></span>
						</a>

						<?php echo CHtml::submitButton(CString::_('next'),array('disabled'=>'disabled')); ?>
					</div>
				</div>

			<?php $this->endWidget(); ?>
		</div>

		<div class="breadcrumbs">
			<div class="progress-bar">
				<img class="cap-left" src="<?php echo Yii::app()->baseUrl; ?>/images/breadcrumb-active.png" alt="" />
				<img class="cap-right" src="<?php echo Yii::app()->baseUrl; ?>/images/breadcrumb-inactive.png" alt="" />

				<div class="progress"></div>
			</div>

			<div class="group">
				<div class="col fixed span_1_of_<?php echo $questionCount + 2; ?> answered">
					<a>
						<span class="desktop"><?php CString::_e('gender'); ?></span>
						<span class="mobile-inline">1</span>
					</a>
				</div>

				<?php foreach($model->quiz->breadcrumbs as $index => $question) { $answered = $question->getAnswerIdsByUser(Yii::app()->request->getParam('userId')); ?>
				<div class="col fixed span_1_of_<?php echo $questionCount + 2;
					if($question->id == $model->id) { $pastCurrent = true; echo ' current'; }
					elseif($answered) echo ' answered';
					// elseif(!$pastCurrent) { echo ' skipped'; }
				?>">
					<?php if($question->id == $model->id || $answered) { ?>
					<a href="<?php echo $this->createUrl('question/view',array('id'=>$question->id,'userId'=>Yii::app()->request->getParam('userId'))); ?>">
						<span class="desktop"><?php echo $question->breadcrumb; ?></span>
						<span class="mobile-inline"><?php echo $index + 2; ?></span>
					</a>
					<?php } else { ?>
					<a>
						<span class="desktop"><?php echo $question->breadcrumb; ?></span>
						<span class="mobile-inline"><?php echo $index + 2; ?></span>
					</a>
					<?php } ?>
				</div>
				<?php } ?>

				<div class="col fixed span_1_of_<?php echo $questionCount + 2; ?>">
					<a>
						<span class="desktop"><?php CString::_e('summary'); ?></span>
						<span class="mobile-inline"><?php echo $index + 3; ?></span>
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class="col span_3_of_11 did-you-know <?php echo strtolower($model->quiz->genderName); ?>">
		<h2><?php CString::_e('did-you-know'); ?></h2>
		<p><?php echo $model->didYouKnow; ?></p>
	</div>
</div>
