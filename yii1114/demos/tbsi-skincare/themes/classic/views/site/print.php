
<div id="printPage">
	<img id="print-logo" src="<?php echo Yii::app()->baseUrl; ?>/images/print/logo.jpg?1" alt="" />

	<div class="content">
		<div class="group result-blurb">
			<div class="col span_1_of_2 skincare-soulmates">
				<h1><?php CString::_e('your-skincare-factsheet'); ?></h1>
				<p><?php echo sprintf(CString::_('hi-name'), $user->name).' '; ?></p>


				<p class="ready"><?php CString::_e('skin-discovery'); ?></p>
			</div>

			<div class="col span_1_of_2 fruits">
				<?php if(!empty($productRanges[0])) { ?>
					<img class="aloe-image" style="DISPLAY: block"  border="0" alt="" src="<?php echo $productRanges[0]->getUrl(36, 34); ?>" width="36" height="34">
				<?php } ?>
				<?php if(!empty($productRanges[1])) { ?>
					<img class="seaweed-image" style="DISPLAY: block"  border="0" alt="" src="<?php echo $productRanges[1]->getUrl(36, 34); ?>" width="36" height="34">
				<?php } ?>
				<?php if(!empty($productRanges[2])) { ?>
					<img class="pome-image" style="DISPLAY: block"  border="0" alt="" src="<?php echo $productRanges[2]->getUrl(36, 34); ?>" width="36" height="34">
				<?php } ?>
				<!-- <img class="aloe-image" src="<?php echo Yii::app()->baseUrl; ?>/images/print/aloe.png" alt="" /> -->
			<!-- 	<img class="seaweed-image" src="<?php echo Yii::app()->baseUrl; ?>/images/print/seaweed.png" alt="" />
				<img class="pome-image" src="<?php echo Yii::app()->baseUrl; ?>/images/print/pome.png" alt="" /> -->
				
				<div class="fruitTitles">
					<?php if(!empty($productRanges[0])) { ?>
					<span class="aloeTitle fruitTitle"><?php echo $productRanges[0]->name; ?></span>
					<?php } ?>
					<?php if(!empty($productRanges[1])) { ?>
					<span class="seaweedTitle fruitTitle"><?php echo $productRanges[1]->name; ?></span>
					<?php } ?>
					<?php if(!empty($productRanges[2])) { ?>
					<span class="pomeTitle fruitTitle"><?php echo $productRanges[2]->name; ?></span>
					<?php } ?>
				</div>

				<div class="fruitDesc">
					<?php if(!empty($productRanges[0])) { ?>
					<span class="aloeDesc fruitDesc"><?php echo $productRanges[0]->description; ?></span>
					<?php } ?>
					<?php if(!empty($productRanges[1])) { ?>
					<span class="seaweedDesc fruitDesc"><?php echo $productRanges[1]->description; ?></span>
					<?php } ?>
					<?php if(!empty($productRanges[2])) { ?>
					<span class="pomeDesc fruitDesc"><?php echo $productRanges[2]->description; ?></span>
					<?php } ?>

				</div>	
				

			</div>
		</div>

		<div class="group product-tabs">
			<div class="col span_1_of_2 tab-container day-night-products">
				<img class="day-header" src="<?php echo Yii::app()->baseUrl; ?>/images/print/day.png" alt="" />
				<ul>
					<li><a style="color:#27bc2b;" class="tab-day" href="#tab-day"><?php CString::_e('day'); ?></a></li>
				</ul>
				<ul id="tab-day" class="group">
					<?php $this->renderPartial('_product', array('product' => $resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_DAY])); ?>
					<?php $this->renderPartial('_product', array('product' => $resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_DAY])); ?>
					<?php $this->renderPartial('_product', array('product' => $resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_DAY])); ?>
				</ul>
				<img class="day-line" src="<?php echo Yii::app()->baseUrl; ?>/images/print/dayline2.png" alt="" />
			</div>
			<div class="col span_1_of_2 tab-container night-container">
				<img class="night-header" src="<?php echo Yii::app()->baseUrl; ?>/images/print/night2.png" alt="" />
				<ul>	
					<?php if($resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_NIGHT] || $resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_NIGHT] || $resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_NIGHT]) { ?>
					<li><a class="tab-night" style="color:#007532;"href="#tab-night"><?php CString::_e('night'); ?></a></li><?php } ?>
				</ul>
				<?php if($resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_NIGHT] || $resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_NIGHT] || $resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_NIGHT]) { ?>
				<ul id="tab-night" class="group">
					<?php if($resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_NIGHT]) $this->renderPartial('_product', array('product' => $resultSheet[Tag::model()->ID_CLEANSE][Product::DAY_NIGHT_NIGHT])); ?>
					<?php if($resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_NIGHT]) $this->renderPartial('_product', array('product' => $resultSheet[Tag::model()->ID_TONE][Product::DAY_NIGHT_NIGHT])); ?>
					<?php if($resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_NIGHT]) $this->renderPartial('_product', array('product' => $resultSheet[Tag::model()->ID_MOISTURISE][Product::DAY_NIGHT_NIGHT])); ?>
				</ul>
				<?php } ?>
				<img class="night-line" src="<?php echo Yii::app()->baseUrl; ?>/images/print/nightline2.png" alt="" />
			</div>
		</div>

		<div class="group product-tabs second-row">
			<img class="shadow-box shadow-box2" src="<?php echo Yii::app()->baseUrl; ?>/images/print/shadowbox2.png" alt="" />
			<div class="col span_1_of_2 tab-container other-products all-day-box">
				<img class="all-day" src="<?php echo Yii::app()->baseUrl; ?>/images/print/allday.png" alt="" />
				<ul>
					<li><a class="tab-other" href="#tab-other"><?php CString::_e('all-day-care'); ?></a></li>
				</ul>

				<ul id="tab-other" class="group">
				<?php foreach($results as $product) $this->renderPartial('_product', array('product' => $product)); ?>
				</ul>
				<img class="all-day-line" src="<?php echo Yii::app()->baseUrl; ?>/images/print/alldayline2.png" alt="" />
			</div>
			<img class="shadow-box shadow-box1" src="<?php echo Yii::app()->baseUrl; ?>/images/print/shadowboxbig.png" alt="" />
			<div class="col span_1_of_2 did-you-know-print">
				<div class="table-cell">
					<h2><?php CString::_e('did-you-know'); ?></h2>

					<p><?php echo $didYouKnow[0]; ?></p>
				</div>
			</div>

			
		</div>

		<div class="group all-day-did-you-know">
			<img class="shadow-box shadow-box3" src="<?php echo Yii::app()->baseUrl; ?>/images/print/shadowbox2.png" alt="" />
			<div class="col span_1_of_2 did-you-know-print did-you-know-three">
				<div class="table-cell">
					<h2><?php CString::_e('did-you-know'); ?></h2>

					<p><?php echo $didYouKnow[1]; ?></p>
				</div>
			</div>
			<div class="col span_1_of_2">
				<div class="group drops-of-youth">
					<div class="col span_1_of_3 fixed">
						<img src="<?php echo $dropsOfYouth->getImageUrl(Product::IMAGE_SIZE_MEDIUM_LARGE); ?>" alt="" />
					</div>

					<div class="col span_2_of_3 fixed">
						<div class="product-name"><?php echo $dropsOfYouth->nameWithoutRange;?></div>
						<p><?php echo $dropsOfYouth->details; ?></p>

						<div class="product-price">
							<input type="checkbox" value="<?php echo trim($dropsOfYouth->activeProductVariants[0]->code); ?>" />
							<?php echo money_format("%.2n", $dropsOfYouth->activeProductVariants[0]->price); ?>
							-
							<span class="product-variant"><?php echo $dropsOfYouth->activeProductVariants[0]->name; ?></span>
						</div>
					</div>
				</div>
			</div>
		</div>	
		<img class="bottom-green" src="<?php echo Yii::app()->baseUrl; ?>/images/print/bottomgreen.jpg" alt="" />
	</div>
</div>	
<script>
window.print();
</script>