<div class="group content">
	<div class="col span_8_of_11 content-left">
		<div class="question-container">
			<h1><?php CString::_e('your-summary'); ?></h1>
			
			<p class="summary-blurb"><?php CString::_e('want-to-make-sure-you-have-everything-covered'); ?></p>

			<p class="user-summary">
				<?php $this->renderPartial('_summary',array('user'=>$user,'link'=>true)); ?>
				<?php CString::_e('ready-to-see-your-new-skincare-regime'); ?>
			</p>

			<div class="ready">
				<a href="<?php echo $this->createUrl('result',array('userId'=>Yii::app()->request->getParam('userId'))); ?>">
					<span><?php CString::_e('im-ready'); ?></span>
					<img src="<?php echo Yii::app()->baseUrl; ?>/images/arrow-next.png" alt="" />
				</a>
			</div>
		</div>

		<div class="breadcrumbs">
			<div class="progress-bar">
				<img class="cap-left" src="<?php echo Yii::app()->baseUrl; ?>/images/breadcrumb-active.png" alt="" />
				<img class="cap-right" src="<?php echo Yii::app()->baseUrl; ?>/images/breadcrumb-inactive.png" alt="" />

				<div class="progress"></div>
			</div>

			<div class="group">
				<div class="col fixed span_1_of_<?php echo $questionCount + 2; ?> answered">
					<a>
						<span class="desktop"><?php CString::_e('gender'); ?></span>
						<span class="mobile-inline">1</span>
					</a>
				</div>

				<?php foreach($user->quiz->breadcrumbs as $index => $question) { ?>
				<div class="col fixed span_1_of_<?php echo $questionCount + 2; ?> answered">
					<a href="<?php echo $this->createUrl('question/view',array('id'=>$question->id,'userId'=>Yii::app()->request->getParam('userId'),'summary'=>'true')); ?>">
						<span class="desktop"><?php echo $question->breadcrumb; ?></span>
						<span class="mobile-inline"><?php echo $index + 2; ?></span>
					</a>
				</div>
				<?php } ?>

				<div class="col fixed span_1_of_<?php echo $questionCount + 2; ?> current">
					<a>
						<span class="desktop"><?php CString::_e('summary'); ?></span>
						<span class="mobile-inline"><?php echo $index + 3; ?></span>
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class="col span_3_of_11 did-you-know <?php echo strtolower($user->quiz->genderName); ?>">
		<h2><?php CString::_e('did-you-know'); ?></h2>
		<p><?php CString::_e('summary-did-you-know'); ?></p>
	</div>
</div>
