/**
 * DOM ready
 */
$(function() {
	// Question form
	$('#Question_type').change(function() {
		var $typeArgLabel = $('[data-type=' + $(this).val() + ']');

		$('[data-type]').css('display', 'none');
		$('[data-type]').parent().css('display', 'none');
		$('#Question_typeArg').val('');

		if($typeArgLabel.length) {
			$typeArgLabel.css('display', '');
			$typeArgLabel.parent().css('display', '');
		}
	});

	// Quiz view: question/answer sorting
	$('#question-grid tbody, #answer-grid tbody').sortable({
		axis: 'y',
		update: function(event, ui) {
			var items = $(this).find('tr'),
				parents = $(this).parents('.grid-view'),
				order = {};

			for(var i = 0, item; item = items[i++];)
				order[i] = $(item).attr('data-id');

			$.ajax({
				url: $(parents[0]).attr('data-updateorderurl'),
				type: 'POST',
				data: order
			});
		}
	});

	// Answer view: product autocomplete
	$('#product-search').autocomplete({
		source: BASE_URL + '/product/search',
		select: function(event, ui) {
			$('#' + $('#product-search').attr('data-rel')).val(ui.item.id);
		}
	});
});